<?php
/**
 * Template Name: view
 */
?>
<div id="main_top">
</div>

<link rel="stylesheet" media="all"  type="text/css" href="http://www.e-himawari.co.jp/wordpress/js011/css/js011.css" />
<script type="text/javascript" src="http://www.e-himawari.co.jp/wordpress/js011/js/jquery.js"></script>
<script type="text/javascript" src="http://www.e-himawari.co.jp/wordpress/js011/js/js011.js"></script>
<script type="text/javascript" src="http://www.e-himawari.co.jp/wordpress/js011/js/minmax.js"></script>

<?php
	$ID = $_GET["ID"];
	if ($ID != ""){
?>
<div id="viewer" class="clearfix" align="center">
<table style="background-color:#FAFAFA;border:1px solid #DCDCDC;" width="790">
<tr>
<td>
	<div id="imagearea" style="background-color:#FAFAFA;">
	</div>
</td>
</tr>
</table>
<table style="background-color:#FFFFFF;" height="130"  width="790">
</tr>
<tr>
<td>
	<img src="http://www.e-himawari.co.jp/wordpress/js011/image/prevbutton.jpg" id="prevbutton" width="25" height="35" />
</td>
<td>
	<div id="imagelist">
		<ul>
<?
  $args = array(
    'showposts' => $count, 
    'post_parent' => $ID,
    'post_type' => 'attachment',
    'post_mime_type' => 'image',
    'numberposts' => '10000',
    'orderby' => 'title',
  );

  $images = get_posts($args);
  $cnt=0;
  foreach($images as $image) {
    $b_arr = wp_get_attachment_image_src_complete($image->ID, $size);
    if ($b_arr[3] == "") {
        echo '<li><a href="'.$b_arr[0] .'"><img src="'.$b_arr[0] .'" width="100" height="100"/></a>';
        $cnt++;
    }
  }
?>
		</ul>
	</div>
</td>
<td>
	<?php if ($cnt > 6) { ?>
	<img src="http://www.e-himawari.co.jp/wordpress/js011/image/nextbutton.jpg" id="nextbutton" width="25" height="35" />
	<?php } ?>
</td>
</tr>
</table>
</div>
<?php
	} else {
		echo '';
	}
?>
