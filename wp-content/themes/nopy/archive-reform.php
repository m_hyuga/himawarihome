<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
   <div id="contents">
 <article id="contents_left">

      <section class="bnr_box height">

            <div id="content">
             <div class="bg_f">



            <h1 class="page-title"><img src="/wp-content/themes/nopy/images/custom/title_reform.jpg" alt="リフォーム"></h1>


<ul class="taxonomy">
<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post();
    /* ループ開始 */ ?>
    <li><a href="<?php the_permalink(); ?>">

    <span><?php
		$imagefield = get_imagefield('ichiran-photo01');
		$attachment = get_attachment_object($imagefield['id']);
		$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
		echo '<image src="' . $imgattr[0] . '" width="' . $imgattr[1] . '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" />';
	?></span>
		<p><?php the_title(); ?></p>
		</a>
	</li>

	<?php endwhile; ?>
<?php else : ?>


<?php endif; ?>

</ul>

					<!-- 出口誘導 -->
					<div class="exit">
					<img src="<?php bloginfo('template_url'); ?>/images/deguti/reform_img_01.png">
						<img src="<?php bloginfo('template_url'); ?>/images/deguti/reform_img_02.png">
						<div class="btn_mail"><a href="/?page_id=34048"><img src="<?php bloginfo('template_url'); ?>/images/deguti/btn_mail.png"></a></div>
						<img src="<?php bloginfo('template_url'); ?>/images/deguti/reform_img_04.png">
						<img src="<?php bloginfo('template_url'); ?>/images/deguti/reform_img_05.png">
					<br>
</div>


<!-- 誘導バナー -->
<a href="http://www.e-himawari.co.jp/?page_id=34048"><img src="/wp-content/themes/nopy/images/bnrShuffle/bnr_l_02.png" alt="" style="margin-top:25px"></a>
<!-- end -->


            <!-- #content -->

		</section>
  </article>
<?php include TEMPLATEPATH . '/sidebar-sekou.php'; ?>

</div>



<?php get_footer(); ?>