<?php
/**
* The template for displaying Category Archive pages.
*
* @package WordPress
* @subpackage Twenty_Ten
* @since Twenty Ten 1.0
*/
get_header(); ?>

<div id="contents">
	<article id="contents_left">
		<section class="bnr_box height">
			<div id="content" class="
                <?php if (has_term('cat01','sekou_cat')) { ?>bg_sekou<?php } ?>
                <?php if (has_term('cat04','sekou_cat')) { ?>bg_tenji<?php } ?>">
                
	<div class="bg_dots">
						<?php if (has_term('cat04','sekou_cat')) { ?>
					<div class="sekou">
						<?php include TEMPLATEPATH . '/sekou_tenji.php'; ?>


				
	<!-- 出口誘導 -->
	<div class="exit">
		<div class="img_kengakupoint">
			<a href="/?page_id=34048"><img src="<?php bloginfo('template_url'); ?>/images/deguti/btn_reserve2.png" alt="来場予約はこちら"></a>
		</div>
		<div class="oh m_t25 width_big">
			<a href="/?page_id=971"><img src="<?php bloginfo('template_url'); ?>/images/deguti/btn_kodawari.png" alt="４つの全棟にこだわります"></a>
			<a href="/?page_id=1091"><img src="<?php bloginfo('template_url'); ?>/images/deguti/btn_staff.png" alt="いい家づくりはいいパートナーを見つけること！" class="m_l20"></a>
			<a href="/?sekou_cat=cat04"><img src="<?php bloginfo('template_url'); ?>/images/deguti/btn_idia.png" alt="お近くの展示場も参考にしたいアイデアがいっぱい" class="m_l20"></a>
			<a href="http://www.e-himawari.co.jp/?page_id=35631"><img src="<?php bloginfo('template_url'); ?>/images/deguti/btn_teian.png" alt="重要な6項目で必ず３つ以上の提案をお約束します。" class="m_l20"></a>
		</div><!-- /oh -->
	</div><!-- /exit -->
	<br>

						<?php } elseif (has_term('cat01','sekou_cat')) { ?>
					<div class="sekou_new">
						<div class="the_contentarea">
							<img src="/wp-content/themes/nopy/images/sekou/sekou/title_koshin.png" alt="更新情報">
							<div class="news_contentarea">
							<?php
							$page_id = 37855;    //xxxに 固定ページIDを入力
							$content = get_page($page_id);
							echo $content->post_content;
							?>
							</div>
						</div>
						<h2><img src="/wp-content/themes/nopy/images/sekou/h2_head_titie.png" alt="施工実績"></h2>
						<ul class="taxonomy_new">
							<?php if (have_posts()) : ?>
							<?php while (have_posts()) : the_post();
							/* ループ開始 */
							?>
							<li>
								<a href="<?php the_permalink(); ?>">
									<h3><?php the_title(); ?></h3>
									<span>
										<?php
											$imagefield = get_imagefield('ichiran-photo01');
											$attachment = get_attachment_object($imagefield['id']);
											$imgattr = wp_get_attachment_image_src($imagefield['id'], 'full'); // thumbnail, large, medium, full を指定
											echo '<image src="' . $imgattr[0] . '" width="343px' . $imgattr[1] . '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" />';
										?>
									</span>
									<p><?php echo mb_substr(strip_tags($post-> post_content),0,80).'...'; ?></p>
								</a>
							</li>
							<?php endwhile; ?>
							<?php else : ?>
							<?php endif; ?>
						</ul>
						<?php wp_pagenavi(); ?>

<!-- 誘導バナー -->
<ul id="bnr_large"></ul>
<!-- end -->

<!-- 誘導バナー -->
<ul id="bnr_twin"></ul>
<!-- end -->

            
		<!-- 施工実績出口誘導 -->
		<br>
	<div class="exit">
		<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_design2.png" alt="ひまわりほーむは３つ以上の設計・デザインを提案します。">
		<a href="/?sekou_cat=cat04"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_maker.png" alt="ひまわりほーむは３種類以上の住宅設備メーカーを提案します。"></a>
		<div class="oh cent_3 m_b55">
			<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_kotira.png" alt="リフォームをお考えの方で部分的な事例や施工方法を見たい方はこちら" class="img_kotira">
			<a href="/?page_id=16049"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_part.png" alt="部分別事例集"></a>
			<a href="/?page_id=30495"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_rehome.png" alt="リフォーム施工事例集" class="flr"></a>
		</div>
		<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_exam.png" alt="">
		<a href="http://www.e-himawari.co.jp/?page_id=34048"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_answer.png" alt="設計・デザインご相談はこちら" class="m_t35"></a>
		<a href="http://www.e-himawari.co.jp/?page_id=34048"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_step.png" alt="ご相談お申込みはお電話一本" class="m_t40"></a>
	</div><!-- /exit -->
	<br>
						<?php } ?>
					</div>
					</div>
				</div>
			<!-- #content -->
		</section>
	</article>
	<?php include TEMPLATEPATH . '/sidebar-sekou.php'; ?>
</div>
<?php get_footer(); ?>