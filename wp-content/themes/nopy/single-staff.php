<?php include (TEMPLATEPATH . '/header-staff.php'); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<?php
if ($terms = get_the_terms($post->ID, 'staff_category')) {
    foreach ( $terms as $term ) {
        $staffslug = esc_html($term->slug);
    }
}
?>

<div id="contents">
  <article id="contents_left">

  <section class="bnr_box height">
    <div id="content">
      <div class="bg_f">
<?php if ( $staffslug == 'shokunin' ) { ?>
 <h2><a href="http://www.e-himawari.co.jp/?page_id=36060"><img src="http://www.e-himawari.co.jp/wp-content/themes/nopy/images/shokunin/img_title_page.jpg" width="752" height="138" alt="熟練された職人紹介" class="hover" /></a></h2>
<?php }else { ?>
        <h2><a href="http://www.e-himawari.co.jp/?page_id=1091"><img src="http://www.e-himawari.co.jp/wp-content/themes/nopy/images/staff/img_title.jpg" width="754" height="148" alt="明るく元気で前向き スタッフ紹介" class="hover" /></a></h2>
			<table align="center" cellpadding="0" cellspacing="0" class="staff_menu" >
			<tr><td align="center" valign="middle"><p>｜ <a href="./?page_id=1093">素朴系</a> ｜ <a href="./?page_id=1098">紳士系</a> ｜ <a href="./?page_id=1119">熱血系</a> ｜ <span style="font-size:14px; line-height:1.2;"><a href="./?page_id=1095">癒し系</a></span> ｜ <a href="./?page_id=1102">イケメン系</a> ｜ <a href="./?page_id=1100">カワイイ系</a> ｜ <a href="./?page_id=1123">舌好調系</a> ｜ <a href="./?page_id=1106">マニア系</a> ｜<br>
			｜ <a href="./?page_id=1420">野球派</a> ｜ <a href="./?page_id=1414">サッカー派</a> ｜ <a href="./?page_id=1110">お笑い系</a> ｜ <a href="./?page_id=1112">不思議・天然系</a> ｜ <a href="./?page_id=1114">グルメ系</a> ｜ <a href="./?page_id=1121">酒豪系</a> ｜ <a href="./?page_id=1117">こぼんのう系</a> ｜</p></td>
			</tr>
			</table>
<?php } ?>
        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <?php the_content(); ?>
<?php echo get_post_meta(get_the_ID(), 'script', 1); ?>
          <?php endwhile; ?>
        </div>
			<table align="center" cellpadding="0" cellspacing="0" class="staff_menu" >
			<tr><td align="center" valign="middle"><p>｜ <a href="./?page_id=1093">素朴系</a> ｜ <a href="./?page_id=1098">紳士系</a> ｜ <a href="./?page_id=1119">熱血系</a> ｜ <span style="font-size:14px; line-height:1.2;"><a href="./?page_id=1095">癒し系</a></span> ｜ <a href="./?page_id=1102">イケメン系</a> ｜ <a href="./?page_id=1100">カワイイ系</a> ｜ <a href="./?page_id=1123">舌好調系</a> ｜ <a href="./?page_id=1106">マニア系</a> ｜<br>
			｜ <a href="./?page_id=1420">野球派</a> ｜ <a href="./?page_id=1414">サッカー派</a> ｜ <a href="./?page_id=1110">お笑い系</a> ｜ <a href="./?page_id=1112">不思議・天然系</a> ｜ <a href="./?page_id=1114">グルメ系</a> ｜ <a href="./?page_id=1121">酒豪系</a> ｜ <a href="./?page_id=1117">こぼんのう系</a> ｜</p></td>
			</tr>
			</table>

<!-- 誘導バナー -->
<ul id="bnr_twin"></ul>
      
      
							<!-- 出口誘導 -->
								<br>
							<div class="exit">
								<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_talk.png">
								<a href="http://www.e-himawari.co.jp/?post_type=voice"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_customer.png" class="m_b18 m_t97 hover"></a>
								<div class="oh">
									<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_top3.png" usemap="#Map">
                                    <map name="Map">
                                      <area shape="rect" coords="12,83,409,195" href="http://www.e-himawari.co.jp/?page_id=30287" alt="会社が明るい！なにより社長が元気で前向き！">
                                      <area shape="rect" coords="12,206,409,318" href="http://www.e-himawari.co.jp/?page_id=10" alt="スタッフ全員が守る家訓がある！">
                                      <area shape="rect" coords="12,329,409,441" href="http://www.e-himawari.co.jp/?page_id=25809" alt="お客様はもちろん、社員同士でも感謝の気持ちを忘れない！">
                                    </map>
									<a href="http://www.e-himawari.co.jp/?page_id=34048"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_reserve.png" class="flr p_t10 hover"></a>
							</div><!-- /exit -->
      </div>
    </div>
    <!-- #content -->
  </section>

  </article>
  <?php include TEMPLATEPATH . '/sidebar-staff.php'; ?>
</div>

<?php get_footer(); ?>