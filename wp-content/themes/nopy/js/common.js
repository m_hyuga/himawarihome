jQuery(document).ready(function ($) {

	//実行処理
	function ObjStart(){
		OverAction(); //マウスオーバー
		bnrShuffle(); // ランダムに画像を表示する
		$(window).load(function() {
			tiles(); //要素の高さを揃える
		});
	}
	ObjStart();

	//要素の高さを揃える
	function tiles(){
	  	$(".height").tile(2);
		$(".height02 li").tile(3);
		$(".height03 li").tile(3);
		$(".staff_height").tile(2);
		$(".height04").tile(2);
		$("ul.taxonomy li").tile(3);
		$("ul.taxonomy_new li").tile(2);
	}

	//マウスオーバー
  	function OverAction(){
		$(".over").hover(
			function(){$(this).stop().fadeTo(200,0);},
			function(){$(this).stop().fadeTo(200,1.0);}
		);
		$(".hover") .hover(
			function(){$(this).stop().fadeTo(240,0.7);},
			function(){$(this).stop().fadeTo(200,1.0);}
		);
  	}

	// ランダムに画像を表示する
	function bnrShuffle(){
		if ( $('ul#bnr_large').length ||  $('ul#bnr_twin').length) {
			$(function(){
				var target = $("#bnr_large");
				target.append(''+
					'<li><a href="http://www.e-himawari.co.jp/?page_id=34048"><img src="/wp-content/themes/nopy/images/bnrShuffle/bnr_l_01.png" alt=""></a></li>'+
					'<li><a href="http://www.e-himawari.co.jp/?page_id=34048"><img src="/wp-content/themes/nopy/images/bnrShuffle/bnr_l_02.png" alt=""></a></li>'+
				'').children().css('display', 'none');

				var elem = target.children().length;
				var rnd1 = Math.floor(Math.random() * elem);
				target.children().eq(rnd1).css({'display': 'block'}).find("a img").each(function(){
					$(this).on('mouseenter', function() {
						$(this).stop(true,true).fadeTo(200, 0.7);
					}),
					$(this).on('mouseleave', function() {
						$(this).stop(true,true).fadeTo(200, 1);
					});
				});
			});
			$(function(){
				var target = $("#bnr_twin");
				target.append(''+
					'<li><a href="http://www.e-himawari.co.jp/?page_id=34048"><img src="/wp-content/themes/nopy/images/bnrShuffle/bnr_m_01.png" alt=""></a><a href="http://www.e-himawari.co.jp/?page_id=34048"><img src="/wp-content/themes/nopy/images/bnrShuffle/bnr_m_02.png" alt=""></a></li>'+
					'<li><a href="http://www.e-himawari.co.jp/?page_id=34048"><img src="/wp-content/themes/nopy/images/bnrShuffle/bnr_m_03.png" alt=""></a><a href="http://www.e-himawari.co.jp/?page_id=34048"><img src="/wp-content/themes/nopy/images/bnrShuffle/bnr_m_04.png" alt=""></a></li>'+
					'<li><a href="http://www.e-himawari.co.jp/?page_id=34048"><img src="/wp-content/themes/nopy/images/bnrShuffle/bnr_m_05.png" alt=""></a><a href="http://www.e-himawari.co.jp/?page_id=34048"><img src="/wp-content/themes/nopy/images/bnrShuffle/bnr_m_06.png" alt=""></a></li>'+
				'').children().css('display', 'none');

				var elem = target.children().length;
				var rnd1 = Math.floor(Math.random() * elem);
				target.children().eq(rnd1).css({'display': 'block'}).find("a img").each(function(){
					$(this).on('mouseenter', function() {
						$(this).stop(true,true).fadeTo(200, 0.7);
					}),
					$(this).on('mouseleave', function() {
						$(this).stop(true,true).fadeTo(200, 1);
					});
				});
			});

		} else {
			return false;
		}
	}


	// リフォームページ　サムネイルマウスオーバー処理
	$('.lineup_deta_list li a,#reform_other_list li a').hover(function(){
		$(this).children('img').css({'opacity':'0.75'});
		$(this).children('p').css({'text-decoration':'underline','color':'#04AAE8'});
	}, function(){
		$(this).children('img').css({'opacity':'1'});
		$(this).children('p').css({'text-decoration':'none','color':'#000'});
	});

});