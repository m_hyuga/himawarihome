 </div>
	<footer>
		<div id="footer_top">
			<div class="footer">
				<div class="footer_left">
					<h1>株式会社ひまわりほーむ・金沢兼六の家ひまわりほーむ（<a href="/?page_id=34">会社概要</a>）</h1>
					<p class="taisho_area"><strong>施工エリア：</strong>石川県、富山県、東京都（奥多摩方面以外）、千葉県（南房総、外房以外）、埼玉県、神奈川県（小田原・箱根方面以外）、茨城（県南地域のみ）</p>
					<strong>●本社</strong> 石川県金沢市新保本4-66-6（<a href="/?page_id=53">地図・アクセス</a>）  TEL（076）269-8100 / FAX（076）269-8101 <br>
					
					│<a href="/?page_id=3745">プライバシーポリシー</a>│<a href="/?page_id=2119">サイトマップ</a>│<a href="/?page_id=32729">リンク</a>│<br>
					Copyright © Himawari Home Corporation. All Rights Reserved.
					</div>
			</div>
		</div>
	</footer>
</div>

<?php wp_footer(); ?>
</body>
</html>
