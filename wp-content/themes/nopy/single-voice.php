<?php
	/**
	* The Template for displaying all single posts.
	*
	* @package WordPress
	* @subpackage Twenty_Ten
	* @since Twenty Ten 1.0
	*/
get_header(); ?>
<link href="http://www.e-himawari.co.jp/wordpress/img/style.css" rel="stylesheet" type="text/css" media="all">
<div id="contents">
	<article id="contents_left">
		<section class="bnr_box height">
			<div id="content" class="bg_voice">
				<div class="bg_dots">
					<div class="user_voice">
						<h2><img src="/wp-content/themes/nopy/images/voice/h2_head_titie.png" alt="お客様の声"></h2>
						<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
						<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<div id="voice_con">
							<h3 class="voice_new"><?php the_title(); ?></h3>
							<div class="entry-content">
								<?php the_content(); ?>
								<?php if (has_term('voice01','voice_cat')) { ?>
								<?php include TEMPLATEPATH . '/voice01.php'; ?>
								<?php } elseif (has_term('voice02','voice_cat')) { ?>
								<?php include TEMPLATEPATH . '/voice02.php'; ?>
								<?php } elseif (has_term('voice03','voice_cat')) { ?>
								<?php include TEMPLATEPATH . '/voice03.php'; ?>
								<?php } elseif (has_term('voice04','voice_cat')) { ?>
								<?php include TEMPLATEPATH . '/voice04.php'; ?>
								<?php } ?>

								 <?php
									$imagefield = get_imagefield('sekou-photo01');
									$attachment = get_attachment_object($imagefield['id']);
									$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
									if(empty($imgattr)){
										echo '';
									}else{
										echo '<h5>aaaa</h5><div><a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
									}
								?>
								<?php echo c2c_get_custom('textarea-url','<p>','</p></div>'); ?>
								<?php echo c2c_get_custom('textarea-url','<a href="','"><img.....></a>'); ?>

								<!-- お客様から手紙を頂きました ここから -->
								 <?php
									$imagefield = get_imagefield('letter-photo');
									$attachment = get_attachment_object($imagefield['id']);
									$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
									if(empty($imgattr)){
										echo '';
									}else{
										echo '
											<h4 class="h4_letter"><img src="/wp-content/themes/nopy/images/voice/h4_title.jpg" alt="お客様から手紙を頂きましたお手紙をいただきました！"></h4>
											<div id="letter_area">
												<div class="letter-detail">
													<div class="letter_left">
														<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>
													</div>';
									}
								?>
                
                

								<?php echo c2c_get_custom('letter-txt','
													<div class="letter_right">
														<p>','</p>
													</div>
												</div>
											</div>
										<img src="/wp-content/themes/nopy/images/voice/letter_btm.jpg" alt="" class="letter_btm">
									');
								 ?>
								<!-- お客様から手紙を頂きました ここまで -->
                
                
<!-- 誘導バナー -->
<ul id="bnr_large"></ul>
<!-- end -->     


								<div id="voice_q">
									<!-- Q&Aここから -->
									<?php echo c2c_get_custom('textarea-voice01-01','<h4 class="voice"><img src="/wp-content/themes/nopy/images/custom/q_a_title.png" alt="お客様からお声をいただきました。"></h4><div class="q01_box"><h5>','</h5>'); ?>
									<?php echo c2c_get_custom('textarea-voice01','<div class="q02_bg"><p>','</p></div><span><img src="/wp-content/themes/nopy/images/custom/bottom02.jpg" alt=""></span></div>'); ?>

									<?php echo c2c_get_custom('textarea-voice02-02','<div class="q01_box"><h5>','</h5>'); ?>
									<?php echo c2c_get_custom('textarea-voice02','<div class="q02_bg"><p>','</p></div><span><img src="/wp-content/themes/nopy/images/custom/bottom02.jpg" alt=""></span></div>'); ?>

									<?php echo c2c_get_custom('textarea-voice03-03','<div class="q01_box"><h5>','</h5>'); ?>
									<?php echo c2c_get_custom('textarea-voice03','<div class="q02_bg"><p>','</p></div><span><img src="/wp-content/themes/nopy/images/custom/bottom02.jpg" alt=""></span></div>'); ?>

									<?php echo c2c_get_custom('textarea-voice04-04','<div class="q01_box"><h5>','</h5>'); ?>
									<?php echo c2c_get_custom('textarea-voice04','<div class="q02_bg"><p>','</p></div><span><img src="/wp-content/themes/nopy/images/custom/bottom02.jpg" alt=""></span></div>'); ?>

									<?php echo c2c_get_custom('textarea-voice05-05','<div class="q01_box"><h5>','</h5>'); ?>
									<?php echo c2c_get_custom('textarea-voice05','<div class="q02_bg"><p>','</p></div><span><img src="/wp-content/themes/nopy/images/custom/bottom02.jpg" alt=""></span></div>'); ?>
									<!-- Q&Aここまで -->

<!-- 誘導バナー -->
<ul id="bnr_twin"></ul>
<!-- end -->

									<!-- このお客妻が建てた家を見る ここから -->
									<?php echo c2c_get_custom('home-url','<div class="home-url"><a href="','"><img src="/wp-content/themes/nopy/images/voice/btn-homeurl.jpg" alt="このお客妻が建てた家を見る" /></a></div>'); ?>
									<!-- このお客妻が建てた家を見る ここまで -->






						<!-- ナビゲーション -->
						<div class="single_page_navi clearfix">
						<ul>
							<li class="btn_prev"><?php next_post_link_plus( array('format' => '%link', 'link' => '<img src="http://www.e-himawari.co.jp/wp-content/themes/nopy/images/voice/btn_prev.png" alt="前へ" class="hover">', 'in_same_tax' => false) ); ?>　</li>
							<li class="btn_list"><a href="/?post_type=voice"><img src="<?php bloginfo('template_url'); ?>/images/voice/btn_list.png" alt="一覧へ" class="hover"></a></li>
							<li class="btn_next">　<?php previous_post_link_plus( array('format' => '%link', 'link' => '<img src="http://www.e-himawari.co.jp/wp-content/themes/nopy/images/voice/btn_next.png" alt="次へ" class="hover">', 'in_same_tax' => false) ); ?></li>
						</ul>
						
						</div><!-- /single_page_navi -->




						<!-- 出口誘導 -->
						<div class="exit">
							<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_anxiety.png" alt="住宅会社に対してこんな心配ありませんか？">
							<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_relief.png" alt="ご安心下さい！ひまわりほーむは" class="p_t10">
							<div class="oh m_t25 m_b18">
								<a href="/?page_id=25194"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_media.png" alt="メディア・書籍等でもひまわりほーむが紹介されています"></a>
								<a href="/?page_id=121"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_festival.png" alt="感謝祭夏まつり・雪まつり" class="m_l20"></a>
									<a href="/?page_id=69"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_family.png" alt="お客様とは家族という関係でありたいと願っています" class="m_b10 flr hover"></a>
									<a href="/?post_type=voice"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_senior.png" alt="実はさらに先輩の生の声が聞けるんです" class="flr"></a>
							</div>
							<a href="/?page_id=34048"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_conf.png" alt="まじは匿名で相談してみませんか？"></a>
                         
</div><!-- .entry-content -->
										<div class="entry-utility">
											<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
										</div>
										<?php if(function_exists('echo_ald_wherego')) echo_ald_wherego(); ?> <!-- .これが関連記事 --><!-- .entry-utility -->
									</div>
								</div><!-- #post-## -->
								<?php endwhile; // end of the loop. ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- #content -->
		</section>
	</article>
<?php include TEMPLATEPATH . '/sidebar-sekou.php'; ?>
</div>
<?php get_footer(); ?>
