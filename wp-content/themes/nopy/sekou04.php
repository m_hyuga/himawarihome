<!-- sekou04 -->
<div class="content_tenjijyo">
            <h1 class="page-title">
            <span class="tenji_main_title"><img src="/wp-content/themes/nopy/images/sekou/tenji/h2_head_titie.png" alt="展示場情報"></span></h1>  
                            <ul class="tenjijyo_btn cf">
                                <li>
	                                <a href="/?sekou_cat=ishikawa">
                                    <img src="<?php bloginfo('template_url'); ?>/images/sekou/btn_ishikawa.png" alt="石川県" width="122" height="38">
	                                </a>
                                </li>
                                <li>
	                                <a href="/?sekou_cat=toyama">
                                    <img src="<?php bloginfo('template_url'); ?>/images/sekou/btn_toyama.png" alt="富山県" width="122" height="38">
	                                </a>
                                </li>
                                <li>
	                                <a href="/?sekou_cat=syutoken">
                                    <img src="<?php bloginfo('template_url'); ?>/images/sekou/btn_shutoken.png" alt="首都圏" width="122" height="38">
	                                </a>
                                </li>
                            </ul>
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div id="sekou_con">
		<h3 class="h3_sekou"><?php the_title(); ?></h3>
		<div class="entry-content">
			<?php the_content(); ?>
			<div class="sekou_con">
				<div class="sekou_con_l01">
					<?php
						$imagefield = get_imagefield('sekou-photo01');
						$attachment = get_attachment_object($imagefield['id']);
						$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
						if(empty($imgattr)){
							echo '';
						}else{
							echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
						}
					?>
					<?php echo c2c_get_custom('textfield-sekou01','<p>','</p>'); ?>
				</div>

				<div class="sekou_con_r01">
					<?php
						$imagefield = get_imagefield('sekou-photo02');
						$attachment = get_attachment_object($imagefield['id']);
						$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
						if(empty($imgattr)){
							echo '';
						}else{
							echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
						}
					?>
					<?php echo c2c_get_custom('textfield-sekou02','<p>','</p>'); ?>

					<?php
						$imagefield = get_imagefield('sekou-photo03');
						$attachment = get_attachment_object($imagefield['id']);
						$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
						if(empty($imgattr)){
							echo '';
						}else{
							echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
						}
					?>
					<?php echo c2c_get_custom('textfield-sekou03','<p>','</p>'); ?>
				</div>
			</div>
      
<!-- 誘導バナー -->
<!-- <ul id="bnr_large"></ul> -->
<!-- end -->

			<div class="sekou_con">
				<div class="sekou_con_l01">
					<?php
						$imagefield = get_imagefield('sekou-photo04');
						$attachment = get_attachment_object($imagefield['id']);
						$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
						if(empty($imgattr)){
							echo '';
						}else{
							echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
						}
					?>
					<?php echo c2c_get_custom('textfield-sekou04','<p>','</p>'); ?>
				</div>

				<div class="sekou_con_r01">
					<?php
						$imagefield = get_imagefield('sekou-photo05');
						$attachment = get_attachment_object($imagefield['id']);
						$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
						if(empty($imgattr)){
							echo '';
						}else{
							echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
						}
					?>
					<?php echo c2c_get_custom('textfield-sekou05','<p>','</p>'); ?>
				</div>
			</div>

			<div class="sekou_con">
				<div class="sekou_con_l02">
					<?php
						$imagefield = get_imagefield('sekou-photo06');
						$attachment = get_attachment_object($imagefield['id']);
						$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
						if(empty($imgattr)){
							echo '';
						}else{
							echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
						}
					?>
					<?php echo c2c_get_custom('textfield-sekou06','<p>','</p>'); ?>
				</div>

				<div class="sekou_con_r02">
					<?php
						$imagefield = get_imagefield('sekou-photo07');
						$attachment = get_attachment_object($imagefield['id']);
						$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
						if(empty($imgattr)){
							echo '';
						}else{
							echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
						}
					?>
					<?php echo c2c_get_custom('textfield-sekou07','<p>','</p>'); ?>
				</div>
			</div>



			<div class="sekou_con">
				<div class="sekou_con_l03">
					<?php
						$imagefield = get_imagefield('sekou-photo08');
						$attachment = get_attachment_object($imagefield['id']);
						$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
						if(empty($imgattr)){
							echo '';
						}else{
							echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
						}
					?>
					<?php echo c2c_get_custom('textfield-sekou08','<p>','</p>'); ?>
				</div>
				<div class="sekou_con_r03">
					<?php
						$imagefield = get_imagefield('sekou-photo09');
						$attachment = get_attachment_object($imagefield['id']);
						$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
						if(empty($imgattr)){
							echo '';
						}else{
							echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
						}
					?>
					<?php echo c2c_get_custom('textfield-sekou09','<p>','</p>'); ?>
					<?php
						$imagefield = get_imagefield('sekou-photo10');
						$attachment = get_attachment_object($imagefield['id']);
						$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
						if(empty($imgattr)){
							echo '';
						}else{
							echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
						}
					?>
					<?php echo c2c_get_custom('textfield-sekou10','<p>','</p>'); ?>
				</div>
			</div>


			<div class="sekou_con">
				<div class="madori">
					<?php
						$imagefield = get_imagefield('sekou-photo13');
						$attachment = get_attachment_object($imagefield['id']);
						$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
						if(empty($imgattr)){
							echo '';
						}else{
							echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
						}
					?>
				</div>

				<div class="tenji_j">
					<?php echo c2c_get_custom('textfield-sekou13','<p>','</p>'); ?>
				</div>
			</div>
		<!-- ナビゲーション -->
						<div class="single_page_navi clearfix">
						<ul>
							<li class="btn_prev"><?php next_post_link_plus( array('format' => '%link', 'link' => '<img src="http://www.e-himawari.co.jp/wp-content/themes/nopy/images/sekou/tenji/btn_prev.png" alt="前へ" class="hover">', 'in_same_tax' => true) ); ?></li>
							<li class="btn_list"><a href="/?sekou_cat=cat04"><img src="<?php bloginfo('template_url'); ?>/images/sekou/tenji/btn_list.png" alt="一覧へ" class="hover"></a></li>
							<li class="btn_next"><?php previous_post_link_plus( array('format' => '%link', 'link' => '<img src="http://www.e-himawari.co.jp/wp-content/themes/nopy/images/sekou/tenji/btn_next.png" alt="次へ" class="hover">', 'in_same_tax' => true) ); ?></li>
						</ul>
						
						</div><!-- /single_page_navi -->

<!-- 誘導バナー -->
<!-- <ul id="bnr_large"></ul> -->
<!-- end -->

<!-- 誘導バナー -->
<!-- <ul id="bnr_twin"></ul> -->
<!-- end -->     
            
	<!-- 出口誘導 -->
	<div class="exit">
		<div class="img_kengakupoint">
			<a href="/?page_id=34048"><img src="<?php bloginfo('template_url'); ?>/images/deguti/btn_reserve2.png" alt="来場予約はこちら"></a>
		</div>
		<div class="oh m_t25 width_big">
			<a href="/?page_id=971"><img src="<?php bloginfo('template_url'); ?>/images/deguti/btn_kodawari.png" alt="４つの全棟にこだわります"></a>
			<a href="/?page_id=1091"><img src="<?php bloginfo('template_url'); ?>/images/deguti/btn_staff.png" alt="いい家づくりはいいパートナーを見つけること！" class="m_l20"></a>
			<a href="/?sekou_cat=cat04"><img src="<?php bloginfo('template_url'); ?>/images/deguti/btn_idia.png" alt="お近くの展示場も参考にしたいアイデアがいっぱい" class="m_l20"></a>
			<a href="http://www.e-himawari.co.jp/?page_id=35631"><img src="<?php bloginfo('template_url'); ?>/images/deguti/btn_teian.png" alt="重要な6項目で必ず３つ以上の提案をお約束します。" class="m_l20"></a>
		</div><!-- /oh -->
	</div><!-- /exit -->
                         
</div>