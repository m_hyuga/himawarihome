<?php
/**
 * Template Name: staffblog
 */
?>
<?php

get_header(); ?>
<link href="http://www.e-himawari.co.jp/wordpress/img/style.css" rel="stylesheet" type="text/css" media="all">
<link href="http://www.e-himawari.co.jp/wordpress/img/style2.css" rel="stylesheet" type="text/css" media="all">
<div id="main_top">
</div>
<div id="main">
<div id="container">
<div id="content" role="main">
<div id="content1">
<?php if ($post->ID == "13017") { ?>
<h2 class="prsi_ttl">社長加葉田のブログ</h2>
<?php } else { ?>
<h2 class="topix_title3">社員ブログ</h2>
<?php } ?>
<div id="topix_area">
<table border="0" cellpadding="0" cellspacing="0" style="padding-top:20px;">
<?php if ($post->ID == "6607") { ?>
<tr>
<td width="150" align="center"><img src="http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/area_blog_ishikawa.jpg"  width="210" height="34"></td>
<td width="150" align="center"><a href="./?page_id=6609" onMouseOver="MM_swapImage('Image1','','http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/area_blog_toyama.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/area_blog_toyama_black.jpg" name="Image1" width="210" height="34" border="0" id="Image1" style="margin-left:5px"></a></td>
<td width="150" align="center"><a href="./?page_id=6612" onMouseOver="MM_swapImage('Image2','','http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/area_blog_tokyo.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/area_blog_tokyo_black.jpg" name="Image2" width="210" height="34" border="0" id="Image2" style="margin-left:5px"></a></td>
</tr>
</table>
<table border="0" cellpadding="15" cellspacing="0" style="border-top:solid;border-top-width:2px;border-top-color:#DC6B00">
  <tr bgcolor="#FFFFFF" valign="top">
    <td>
      <div class="area4">
          <dl>
            <?php
              // ブログ取得(最終更新日)
              $posts1_1 = get_posts('numberposts=20&category=131&orderby=modified');
              $arr_posts1_1 = array();
              $cnt=0;
              $tmp_id ='';
              if($posts1_1){
                foreach($posts1_1 as $post){
                  $tmp_id .= $post->ID.',';
                  if ($post->post_modified >= $post->post_date){
                    $arr_posts1_1[$cnt] = $post->post_modified;
                  } else {
                    $arr_posts1_1[$cnt] = $post->post_date;
                  }
                  $cnt++;
                }
              }
              // ブログ取得(公開日)
              $posts1_2 = get_posts('numberposts=20&category=131&orderby=date&exclude='.$tmp_id);
              $arr_posts1_2 = array();
              $cnt=0;
              if($posts1_2){
                foreach($posts1_2 as $post){
                  $arr_posts1_2[$cnt] = $post->post_date;
                  $cnt++;
                }
              }
              // 配列のマージ
              $arr_posts1_3 = array_merge($arr_posts1_1,$arr_posts1_2);
              $posts1_3 = array_merge($posts1_1,$posts1_2);
              // ソート処理
              array_multisort($arr_posts1_3, SORT_DESC, $posts1_3, SORT_DESC);
            ?>
            <?php
              $cnt=0;
              if($posts1_3): foreach($posts1_3 as $post): setup_postdata($post); $cnt++;
            ?>
              <dd>
                <table width="100%">
                  <tr>
                    <td><a href="?p=<?php the_id(); ?>"><?php $title = the_title( '' , '' , false ); if($title!=''){ echo $title; } else { echo 'タイトルなし'; } ?></a></td>
                    <td style="text-align:right;font-size:12px;font-weight: normal;">
                      <?php if ($post->post_modified >= $post->post_date){ 
                        the_modified_date('Y年m月d日');
                      } else { 
                        echo get_the_time('Y').'年'.get_the_time('n').'月'.get_the_time('j').'日';
                      } ?>
                    </td>
                  </tr>
                </table>
              <br><?php the_excerpt(); ?>
              </dd>
            <?php
              if ($cnt >= 25){
                break;
              }
              endforeach; endif;
            ?>
          </dl>
      </div>
    </td>
  </tr>
  <tr>
    <td style="text-align:right;font-size:13px;font-weight: bold;">
      <a href="?cat=131">ブログを全部見る</a>
    </td>
  </tr>
<?php } else if ($post->ID == "6609") { ?>
<tr>
<td width="150" align="center"><a href="./?page_id=6607" onMouseOver="MM_swapImage('Image3','','http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/area_blog_ishikawa.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/area_blog_ishikawa_black.jpg" name="Image3" width="210" height="34" border="0" id="Image3" ></a></td>
<td width="150" align="center"><img src="http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/area_blog_toyama.jpg"  width="210" height="34" style="margin-left:5px"></td>
<td width="150" align="center"><a href="./?page_id=6612" onMouseOver="MM_swapImage('Image2','','http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/area_blog_tokyo.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/area_blog_tokyo_black.jpg" name="Image2" width="210" height="34" border="0" id="Image2" style="margin-left:5px"></a></td>
</tr>
</table>
<table border="0" cellpadding="15" cellspacing="0" style="border-top:solid;border-top-width:2px;border-top-color:#5B9400;">
  <tr bgcolor="#FFFFFF" valign="top">
    <td>
      <div class="area5">
          <dl>
            <?php
              // ブログ取得(最終更新日)
              $posts1_1 = get_posts('numberposts=20&category=196&orderby=modified');
              $arr_posts1_1 = array();
              $cnt=0;
              $tmp_id ='';
              if($posts1_1){
                foreach($posts1_1 as $post){
                  $tmp_id .= $post->ID.',';
                  if ($post->post_modified >= $post->post_date){
                    $arr_posts1_1[$cnt] = $post->post_modified;
                  } else {
                    $arr_posts1_1[$cnt] = $post->post_date;
                  }
                  $cnt++;
                }
              }
              // ブログ取得(公開日)
              $posts1_2 = get_posts('numberposts=20&category=196&orderby=date&exclude='.$tmp_id);
              $arr_posts1_2 = array();
              $cnt=0;
              if($posts1_2){
                foreach($posts1_2 as $post){
                  $arr_posts1_2[$cnt] = $post->post_date;
                  $cnt++;
                }
              }
              // 配列のマージ
              $arr_posts1_3 = array_merge($arr_posts1_1,$arr_posts1_2);
              $posts1_3 = array_merge($posts1_1,$posts1_2);
              // ソート処理
              array_multisort($arr_posts1_3, SORT_DESC, $posts1_3, SORT_DESC);
            ?>
            <?php
              $cnt=0;
              if($posts1_3): foreach($posts1_3 as $post): setup_postdata($post); $cnt++;
            ?>
              <dd>
                <table width="100%">
                  <tr>
                    <td><a href="?p=<?php the_id(); ?>"><?php $title = the_title( '' , '' , false ); if($title!=''){ echo $title; } else { echo 'タイトルなし'; } ?></a></td>
                    <td style="text-align:right;font-size:12px;font-weight: normal;">
                      <?php if ($post->post_modified >= $post->post_date){ 
                        the_modified_date('Y年m月d日');
                      } else { 
                        echo get_the_time('Y').'年'.get_the_time('n').'月'.get_the_time('j').'日';
                      } ?>
                    </td>
                  </tr>
                </table>
              <br><?php the_excerpt(); ?>
              </dd>
            <?php
              if ($cnt >= 25){
                break;
              }
              endforeach; endif;
            ?>
          </dl>
      </div>
    </td>
  </tr>
  <tr>
    <td style="text-align:right;font-size:13px;font-weight: bold;">
      <a href="?cat=196">ブログを全部見る</a>
    </td>
  </tr>
<?php } else if ($post->ID == "6612") { ?>
<tr>
<td width="150" align="center"><a href="./?page_id=6607" onMouseOver="MM_swapImage('Image3','','http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/area_blog_ishikawa.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/area_blog_ishikawa_black.jpg" name="Image3" width="210" height="34" border="0" id="Image3"></a></td>
<td width="150" align="center"><a href="./?page_id=6609" onMouseOver="MM_swapImage('Image1','','http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/area_blog_toyama.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/area_blog_toyama_black.jpg" name="Image1" width="210" height="34" border="0" id="Image1" style="margin-left:5px"></a></td>
<td width="150" align="center"><img src="http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/area_blog_tokyo.jpg"  width="210" height="34" style="margin-left:5px"></td>
</tr>
</table>
<table border="0" cellpadding="15" cellspacing="0" style="border-top:solid;border-top-width:2px;border-top-color:#1E568F">
  <tr bgcolor="#FFFFFF" valign="top">
    <td>
      <div class="area6">
          <dl>
            <?php
              // ブログ取得(最終更新日)
              $posts1_1 = get_posts('numberposts=20&category=216&orderby=modified');
              $arr_posts1_1 = array();
              $cnt=0;
              $tmp_id ='';
              if($posts1_1){
                foreach($posts1_1 as $post){
                  $tmp_id .= $post->ID.',';
                  if ($post->post_modified >= $post->post_date){
                    $arr_posts1_1[$cnt] = $post->post_modified;
                  } else {
                    $arr_posts1_1[$cnt] = $post->post_date;
                  }
                  $cnt++;
                }
              }
              // ブログ取得(公開日)
              $posts1_2 = get_posts('numberposts=20&category=216&orderby=date&exclude='.$tmp_id);
              $arr_posts1_2 = array();
              $cnt=0;
              if($posts1_2){
                foreach($posts1_2 as $post){
                  $arr_posts1_2[$cnt] = $post->post_date;
                  $cnt++;
                }
              }
              // 配列のマージ
              $arr_posts1_3 = array_merge($arr_posts1_1,$arr_posts1_2);
              $posts1_3 = array_merge($posts1_1,$posts1_2);
              // ソート処理
              array_multisort($arr_posts1_3, SORT_DESC, $posts1_3, SORT_DESC);
            ?>
            <?php
              $cnt=0;
              if($posts1_3): foreach($posts1_3 as $post): setup_postdata($post); $cnt++;
            ?>
              <dd>
                <table width="100%">
                  <tr>
                    <td><a href="?p=<?php the_id(); ?>"><?php $title = the_title( '' , '' , false ); if($title!=''){ echo $title; } else { echo 'タイトルなし'; } ?></a></td>
                    <td style="text-align:right;font-size:12px;font-weight: normal;">
                      <?php if ($post->post_modified >= $post->post_date){ 
                        the_modified_date('Y年m月d日');
                      } else { 
                        echo get_the_time('Y').'年'.get_the_time('n').'月'.get_the_time('j').'日';
                      } ?>
                    </td>
                  </tr>
                </table>
              <br><?php the_excerpt(); ?>
              </dd>
            <?php
              if ($cnt >= 25){
                break;
              }
              endforeach; endif;
            ?>
          </dl>
      </div>
    </td>
  </tr>
  <tr>
    <td style="text-align:right;font-size:13px;font-weight: bold;">
      <a href="?cat=216">ブログを全部見る</a>
    </td>
  </tr>
<?php } ?>
</table>
<?php if ($post->ID == "13017") { ?>
<table border="0" cellpadding="15" cellspacing="0">
  <tr bgcolor="#FFFFFF" valign="top">
    <td>
      <div class="area4">
          <dl>
            <?php
              // ブログ取得(最終更新日)
              $posts1_1 = get_posts('numberposts=20&category=125&orderby=modified');
              $arr_posts1_1 = array();
              $cnt=0;
              $tmp_id ='';
              if($posts1_1){
                foreach($posts1_1 as $post){
                  $tmp_id .= $post->ID.',';
                  if ($post->post_modified >= $post->post_date){
                    $arr_posts1_1[$cnt] = $post->post_modified;
                  } else {
                    $arr_posts1_1[$cnt] = $post->post_date;
                  }
                  $cnt++;
                }
              }
              // ブログ取得(公開日)
              $posts1_2 = get_posts('numberposts=20&category=125&orderby=date&exclude='.$tmp_id);
              $arr_posts1_2 = array();
              $cnt=0;
              if($posts1_2){
                foreach($posts1_2 as $post){
                  $arr_posts1_2[$cnt] = $post->post_date;
                  $cnt++;
                }
              }
              // 配列のマージ
              $arr_posts1_3 = array_merge($arr_posts1_1,$arr_posts1_2);
              $posts1_3 = array_merge($posts1_1,$posts1_2);
              // ソート処理
              array_multisort($arr_posts1_3, SORT_DESC, $posts1_3, SORT_DESC);
            ?>
            <?php
              $cnt=0;
              if($posts1_3): foreach($posts1_3 as $post): setup_postdata($post); $cnt++;
            ?>
              <dd>
                <table width="100%">
                  <tr>
                    <td><a href="?p=<?php the_id(); ?>"><?php $title = the_title( '' , '' , false ); if($title!=''){ echo $title; } else { echo 'タイトルなし'; } ?></a></td>
                    <td style="text-align:right;font-size:12px;font-weight: normal;">
                      <?php if ($post->post_modified >= $post->post_date){ 
                        the_modified_date('Y年m月d日');
                      } else { 
                        echo get_the_time('Y').'年'.get_the_time('n').'月'.get_the_time('j').'日';
                      } ?>
                    </td>
                  </tr>
                </table>
              <br><?php the_excerpt(); ?>
              </dd>
            <?php
              if ($cnt >= 25){
                break;
              }
              endforeach; endif;
            ?>
          </dl>
      </div>
    </td>
  </tr>
  <tr>
    <td style="text-align:right;font-size:13px;font-weight: bold;">
      <a href="?cat=125">ブログを全部見る</a>
    </td>
  </tr>
</table>
<?php } ?>
</div>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
									

					<script language="JavaScript" type="text/JavaScript" src="http://www.e-himawari.co.jp/wordpress/main.js"></script>

					<div class="entry-content">
							<?php the_content(); ?>
							<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
							<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-content -->
				</div><!-- #post-## -->

<?php endwhile; ?>
</div>

			</div><!-- #content -->
            <?php get_sidebar(); ?>
</div><!-- #container -->


<?php get_footer(); ?>
