<div class="voice_con">
	<div class="no01-box01">
		<?php
			$imagefield = get_imagefield('voice-photo01');
			$attachment = get_attachment_object($imagefield['id']);
			$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
			if(empty($imgattr)){
				echo '';
			}else{
				echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
			}
		?>
		<?php echo c2c_get_custom('textfield-voice01'); ?>
	</div>

	<div class="no01-box02">
		<?php
			$imagefield = get_imagefield('voice-photo02');
			$attachment = get_attachment_object($imagefield['id']);
			$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
			if(empty($imgattr)){
				echo '';
			}else{
				echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
			}
		?>
		<?php echo c2c_get_custom('textfield-voice02'); ?>
	</div>

	<div class="no01-box03">
		<?php
			$imagefield = get_imagefield('voice-photo03');
			$attachment = get_attachment_object($imagefield['id']);
			$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
			if(empty($imgattr)){
				echo '';
			}else{
				echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
			}
		?>
		<?php echo c2c_get_custom('textfield-voice03'); ?>
	</div>
</div>

<div class="voice_con">
	<div class="no01-box04">
		<?php
			$imagefield = get_imagefield('voice-photo04');
			$attachment = get_attachment_object($imagefield['id']);
			$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
			if(empty($imgattr)){
				echo '';
			}else{
				echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
			}
		?>
		<?php echo c2c_get_custom('textfield-voice04'); ?>
	</div>

	<div class="no01-box05">
		<?php
			$imagefield = get_imagefield('voice-photo05');
			$attachment = get_attachment_object($imagefield['id']);
			$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
			if(empty($imgattr)){
				echo '';
			}else{
				echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
			}
		?>
		<?php echo c2c_get_custom('textfield-voice05'); ?>
	</div>
</div>

<div class="voice_con">

	<div class="no01-box06">
		<?php
			$imagefield = get_imagefield('voice-photo06');
			$attachment = get_attachment_object($imagefield['id']);
			$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
			if(empty($imgattr)){
				echo '';
			}else{
				echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
			}
		?>
		<?php echo c2c_get_custom('textfield-voice06'); ?>
	</div>

	<div class="no01-box07">
		<?php
			$imagefield = get_imagefield('voice-photo07');
			$attachment = get_attachment_object($imagefield['id']);
			$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
			if(empty($imgattr)){
				echo '';
			}else{
				echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
			}
		?>
		<?php echo c2c_get_custom('textfield-voice07'); ?>
	</div>

	<div class="no01-box08">
		<?php
			$imagefield = get_imagefield('voice-photo08');
			$attachment = get_attachment_object($imagefield['id']);
			$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
			if(empty($imgattr)){
				echo '';
			}else{
				echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
			}
		?>
		<?php echo c2c_get_custom('textfield-voice08'); ?>
	</div>
</div>