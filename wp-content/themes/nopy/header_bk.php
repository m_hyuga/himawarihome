<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">

 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />

<meta name="description"content="無垢材・自然素材・性能表示・耐震・省エネにこだわり、新築する全ての住宅を長期優良住宅とし、お客様が安心・納得・満足される健康住宅を提供しています。世界的な建築家である西沢立衛氏との共同プロジェクトも進行中です。"/>
<meta name="Keywords"content="長期優良住宅，無垢材，自然素材，性能表示，石川県，富山県，東京都，西沢立衛，ひまわりほーむ"/>

<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 * We filter the output of wp_title() a bit -- see
	 * twentyten_filter_wp_title() in functions.php.
	 */
	wp_title( '|', true, 'right' );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style.css" type="text/css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/common.css" type="text/css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/top.css" type="text/css">
       
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-21257881-1']);
  _gaq.push(['_trackPageview']);
 
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
 </script>
 
 <?php wp_head(); ?>
 <!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
  <style type="text/css" media="screen">
		html { margin-top: 0px !important; }
	* html body { margin-top: 0px !important; }
</style>



</head>

<body>


<div id="wrapper">
 <header>
    <div id="header">
       <div class="header_inner">
         <div class="header_right">
            <ul>
            <li><a href="/?page_id=32729">リンク</a></li>
              <li><a href="/?page_id=38">採用情報</a></li>
              <li><a href="/?page_id=25194">メディア掲載実績・CSR活動</a></li>
              <li><a href="/?page_id=3745">プライバシーポリシー</a></li>
              <li><a href="/?page_id=2119">サイトマップ</a></li>
              
            </ul>
            <a href="/?page_id=13666"><img src="<?php bloginfo('template_url'); ?>/images/common/header/tel.png" alt="お問い合わせ・資料請求" width="445" height="55"></a>
         </div>
         <h1><a href="/"><img src="<?php bloginfo('template_url'); ?>/images/common/header/logo.png"></a></h1>
       </div>
    </div>
  </header>


<nav id="grobal">
    <ul id="gnav" class="clearfix">
      <li><a href="/?page_id=971"><img src="<?php bloginfo('template_url'); ?>/images/common/header/nav01_off.jpg" alt="ひまわりほーむの人気の秘密"></a></li>
      <li><a href="/?page_id=1091"><img src="<?php bloginfo('template_url'); ?>/images/common/header/nav02_off.jpg" alt="スタッフ紹介"></a></li>
      <li><a href="/?sekou_cat=cat01"><img src="<?php bloginfo('template_url'); ?>/images/common/header/nav03_off.jpg" alt="施工実績"></a></li>
      <li><a href="/?post_type=voice"><img src="<?php bloginfo('template_url'); ?>/images/common/header/nav04_off.jpg" alt="お客様の声"></a></li>
      <li><a href="/?sekou_cat=cat04"><img src="<?php bloginfo('template_url'); ?>/images/common/header/nav05_off.jpg" alt="展示場"></a></li>
      <li><a href="/?page_id=33463"><img src="<?php bloginfo('template_url'); ?>/images/common/header/nav06_off.jpg" alt="インフォメーショント"></a></li>
      <li><a href="/?page_id=582"><img src="<?php bloginfo('template_url'); ?>/images/common/header/nav07_off.jpg" alt="リフォーム"></a></li>
      <li><a href="/?page_id=30564"><img src="<?php bloginfo('template_url'); ?>/images/common/header/nav08_off.jpg" alt="新しい二世帯住宅"></a></li>
      <li><a href="http://e-himawarifudousan.com/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/common/header/nav09_off.jpg" alt="不動産情報"></a></li>
    </ul>
  </nav>
  
  <div id="contents_bg">
  
  
 