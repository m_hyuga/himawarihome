<?php
/* Template Name: [お問い合わせ] */
?>
 <?php include (TEMPLATEPATH . '/header-form.php'); ?>
<div id="contents_form">
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php the_content(); ?>
</div>
<?php endwhile; ?>
</div>
<?php get_footer(); ?>