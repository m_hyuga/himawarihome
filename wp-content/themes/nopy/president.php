<?php
/**
* Template Name: president
*/
get_header(); ?>
<div id="contents_president" class="cf">
<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/nopy/css/president.css">
<article id="contents_left">
	<section class="height">
		<div id="main" class="president">
			<div id="container">
				<div id="content" class="president" role="main">
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<div class="entry-content">
							<?php the_content(); ?>
							<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
							<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
						</div><!-- .entry-content -->
					</div><!-- #post-## -->
				</div>

				<?php endwhile; ?>
			</div>
		</div>
	</section>
</article>
<?php include TEMPLATEPATH . '/sidebar-page.php'; ?>
</div>
</div>
	<?php get_footer(); ?>
