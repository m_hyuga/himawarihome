<div id="reform_lineup">
<h4><img src="<?php bloginfo('template_url'); ?>/images/reform/lineup.jpg" alt=""></h4>
<a href="/?page_id=38191"><img src="<?php bloginfo('template_url'); ?>/images/reform/index_04.jpg" width="736" height="134" alt="500万からのリフォーム"></a>
<ul class="lineup_deta_list">
<li><a href="/?page_id=38205">
<img src="<?php bloginfo('template_url'); ?>/images/reform/index_list01.jpg" width="160" height="120" alt="">
<p>台所と居間を合わせてリフォーム</p>
</a></li>
<li><a href="/?page_id=38213">
<img src="<?php bloginfo('template_url'); ?>/images/reform/index_list02.jpg" width="160" height="120" alt="">
<p>収納場所を決めてリフォーム</p>
</a></li>
<li><a href="/?page_id=38215">
<img src="<?php bloginfo('template_url'); ?>/images/reform/index_list03.jpg" width="160" height="120" alt="">
<p>家の水廻りをリフォーム</p>
</a></li>
</ul>
<a href="/?page_id=38195"><img src="<?php bloginfo('template_url'); ?>/images/reform/index_05.jpg" width="736" height="134" alt="1000万からのリフォーム"></a>
<ul class="lineup_deta_list">
<li><a href="/?page_id=38217">
<img src="<?php bloginfo('template_url'); ?>/images/reform/index_list04.jpg" width="160" height="120" alt="">
<p>2世帯6人家族の住まいを増築</p>
</a></li>
<li><a href="/?page_id=38219">
<img src="<?php bloginfo('template_url'); ?>/images/reform/index_list05.jpg" width="160" height="120" alt="">
<p>2人住まいの1階を全面改装</p>
</a></li>
</ul>
<a href="/?page_id=38197"><img src="<?php bloginfo('template_url'); ?>/images/reform/index_06.jpg" width="736" height="134" alt="200〜300万からのリフォーム"></a>
<ul class="lineup_deta_list">
<li><a href="/?page_id=38221">
<img src="<?php bloginfo('template_url'); ?>/images/reform/index_list06.jpg" width="160" height="120" alt="">
<p>アイランドキッチンにリフォーム</p>
</a></li>
<li><a href="/?page_id=38223">
<img src="<?php bloginfo('template_url'); ?>/images/reform/index_list07.jpg" width="160" height="120" alt="">
<p>キッチンの収納力を増やす</p>
</a></li>
<li><a href="/?page_id=38225">
<img src="<?php bloginfo('template_url'); ?>/images/reform/index_list08.jpg" width="160" height="120" alt="">
<p>2トーンのサイディングに</p>
</a></li>
</ul>
</div>
<ul id="reform_3col">
<li><a href="/?page_id=38199"><img src="<?php bloginfo('template_url'); ?>/images/reform/index_07.jpg" width="243" height="55" alt="部分的リフォーム"></a></li>
<li><a href="/?page_id=38201"><img src="<?php bloginfo('template_url'); ?>/images/reform/index_08.jpg" width="243" height="55" alt="店舗リフォーム"></a></li>
<li><a href="/?page_id=38203"><img src="<?php bloginfo('template_url'); ?>/images/reform/index_09.jpg" width="243" height="55" alt="こんなこともやってます工事"></a></li>
</ul>
<ul id="reform_other_list">
<li><a href="/?page_id=38227">
<img src="<?php bloginfo('template_url'); ?>/images/reform/index_list09.jpg" width="160" height="120" alt="">
<p>強度を増して綺麗に張り直し</p>
</a></li>
<li><a href="/?page_id=38229">
<img src="<?php bloginfo('template_url'); ?>/images/reform/index_list10.jpg" width="160" height="120" alt="">
<p>居酒屋を喫茶店にリフォーム</p>
</a></li>
<li><a href="/?page_id=38231">
<img src="<?php bloginfo('template_url'); ?>/images/reform/index_list11.jpg" width="160" height="120" alt="">
<p>印象の悪い塀をリフォーム</p>
</a></li>
</ul>
