<!-- sekou03 -->
<div class="sekou_new">
	<h2><img src="/wp-content/themes/nopy/images/sekou/h2_head_titie.png" alt="施工実績"></h2>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div id="sekou_con">
		<h3 class="h3_sekou"><?php the_title(); ?></h3>
		<div class="entry-content">
			<?php the_content(); ?>
			<div class="sekou_con">
				<div class="sekou_con_l01_03">
					<?php
						$imagefield = get_imagefield('sekou-photo01');
						$attachment = get_attachment_object($imagefield['id']);
						$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
						if(empty($imgattr)){
							echo '';
						}else{
							echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
						}
					?>
					<?php echo c2c_get_custom('textfield-sekou01','<p>','</p>'); ?>

					<?php
						$imagefield = get_imagefield('sekou-photo02');
						$attachment = get_attachment_object($imagefield['id']);
						$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
						if(empty($imgattr)){
							echo '';
						}else{
							echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
						}
					?>
					<?php echo c2c_get_custom('textfield-sekou02','<p>','</p>'); ?>
				</div>

				<div class="sekou_con_r01_03">
					<?php
						$imagefield = get_imagefield('sekou-photo03');
						$attachment = get_attachment_object($imagefield['id']);
						$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
						if(empty($imgattr)){
							echo '';
						}else{
							echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
						}
					?>
					<?php echo c2c_get_custom('textfield-sekou03','<p>','</p>'); ?>
				</div>
			</div>

<!-- 誘導バナー -->
<!-- <ul id="bnr_large"></ul> -->
<!-- end -->


			<div class="sekou_con">
				<div class="sekou_con_l01_03">
					<?php
						$imagefield = get_imagefield('sekou-photo04');
						$attachment = get_attachment_object($imagefield['id']);
						$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
						if(empty($imgattr)){
							echo '';
						}else{
							echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
						}
					?>
					<?php echo c2c_get_custom('textfield-sekou04','<p>','</p>'); ?>
				</div>

				<div class="sekou_con_r01_03">
					<?php
						$imagefield = get_imagefield('sekou-photo05');
						$attachment = get_attachment_object($imagefield['id']);
						$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
						if(empty($imgattr)){
							echo '';
						}else{
							echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
						}
					?>
					<?php echo c2c_get_custom('textfield-sekou05','<p>','</p>'); ?>
				</div>
			</div>

			<div class="sekou_con">
				<div class="sekou_con_l02_03">
					<?php
						$imagefield = get_imagefield('sekou-photo06');
						$attachment = get_attachment_object($imagefield['id']);
						$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
						if(empty($imgattr)){
							echo '';
						}else{
							echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
						}
					?>
					<?php echo c2c_get_custom('textfield-sekou06','<p>','</p>'); ?>
				</div>

				<div class="sekou_con_r02_03">
					<?php
						$imagefield = get_imagefield('sekou-photo07');
						$attachment = get_attachment_object($imagefield['id']);
						$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
						if(empty($imgattr)){
							echo '';
						}else{
							echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
						}
					?>
					<?php echo c2c_get_custom('textfield-sekou07','<p>','</p>'); ?>
				</div>
			</div>

<!-- 誘導バナー -->
<!-- <ul id="bnr_twin"></ul> -->
<!-- end -->

			<div class="sekou_con">
				<div class="sekou_con_l03_03">
					<?php
						$imagefield = get_imagefield('sekou-photo08');
						$attachment = get_attachment_object($imagefield['id']);
						$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
						if(empty($imgattr)){
							echo '';
						}else{
							echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
						}
					?>
					<?php echo c2c_get_custom('textfield-sekou08','<p>','</p>'); ?>

					<?php
						$imagefield = get_imagefield('sekou-photo09');
						$attachment = get_attachment_object($imagefield['id']);
						$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
						if(empty($imgattr)){
							echo '';
						}else{
							echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
						}
					?>
					<?php echo c2c_get_custom('textfield-sekou09','<p>','</p>'); ?>
				</div>
				<div class="sekou_con_r03_03">
					<?php
						$imagefield = get_imagefield('sekou-photo10');
						$attachment = get_attachment_object($imagefield['id']);
						$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
						if(empty($imgattr)){
							echo '';
						}else{
							echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
						}
					?>
					<?php echo c2c_get_custom('textfield-sekou10','<p>','</p>'); ?>
				</div>
			</div>
		<!-- ナビゲーション -->
						<div class="single_page_navi clearfix">
						<ul>
							<li class="btn_prev"><?php next_post_link_plus( array('format' => '%link', 'link' => '<img src="http://www.e-himawari.co.jp/wp-content/themes/nopy/images/voice/btn_prev.png" alt="前へ" class="hover">', 'in_same_tax' => true) ); ?></li>
							<li class="btn_list"><a href="/?sekou_cat=cat01"><img src="<?php bloginfo('template_url'); ?>/images/voice/btn_list.png" alt="一覧へ" class="hover"></a></li>
							<li class="btn_next"><?php previous_post_link_plus( array('format' => '%link', 'link' => '<img src="http://www.e-himawari.co.jp/wp-content/themes/nopy/images/voice/btn_next.png" alt="次へ" class="hover">', 'in_same_tax' => true) ); ?></li>
						</ul>
						
						</div><!-- /single_page_navi -->
            

            
		<!-- 出口誘導 -->
	<div class="exit">
		<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_design2.png" alt="ひまわりほーむは３つ以上の設計・デザインを提案します。">
		<a href="/?sekou_cat=cat04"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_maker.png" alt="ひまわりほーむは３種類以上の住宅設備メーカーを提案します。"></a>
		<div class="oh cent_3 m_b55">
			<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_kotira.png" alt="リフォームをお考えの方で部分的な事例や施工方法を見たい方はこちら" class="img_kotira">
			<a href="/?page_id=16049"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_part.png" alt="部分別事例集"></a>
			<a href="/?page_id=30495"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_rehome.png" alt="リフォーム施工事例集" class="flr"></a>
		</div>
		<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_exam.png" alt="">
		<a href="http://www.e-himawari.co.jp/?page_id=34048"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_answer.png" alt="設計・デザインご相談はこちら" class="m_t35"></a>
		<a href="http://www.e-himawari.co.jp/?page_id=34048"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_step.png" alt="ご相談お申込みはお電話一本" class="m_t40"></a>
	</div><!-- /exit -->
			<!-- このお客様の声を見る ここから -->
			<?php echo c2c_get_custom('home-voice','<div class="home-voice"><a href="','"><img src="/wp-content/themes/nopy/images/sekou/btn_user-voice.jpg" alt="このお客様の声を見る" /></a></div>'); ?>
			<!-- このお客様の声を見る ここまで -->
                         
