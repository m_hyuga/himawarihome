<?php include (TEMPLATEPATH . '/header-staff2.php'); ?>


<div id="contents">
  <article id="contents_left">

  <section class="bnr_box height">
    <div id="content">
      <div class="bg_f">
        <h2><a href="http://www.e-himawari.co.jp/?page_id=1091"><img src="http://www.e-himawari.co.jp/wp-content/themes/nopy/images/shokunin/img_title.jpg" width="754" height="148" alt="明るく元気で前向き スタッフ紹介" class="hover" /></a></h2>
        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <?php the_content(); ?>
<?php echo get_post_meta(get_the_ID(), 'script', 1); ?>
          <?php endwhile; ?>
        </div>

<!-- 誘導バナー -->
<ul id="bnr_twin"></ul>
      
      
							<!-- 出口誘導 -->
								<br>
							<div class="exit">
								<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_talk.png">
								<a href="http://www.e-himawari.co.jp/?post_type=voice"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_customer.png" class="m_b18 m_t97 hover"></a>
								<div class="oh">
									<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_top3.png" usemap="#Map">
                                    <map name="Map">
                                      <area shape="rect" coords="12,83,409,195" href="http://www.e-himawari.co.jp/?page_id=30287" alt="会社が明るい！なにより社長が元気で前向き！">
                                      <area shape="rect" coords="12,206,409,318" href="http://www.e-himawari.co.jp/?page_id=10" alt="スタッフ全員が守る家訓がある！">
                                      <area shape="rect" coords="12,329,409,441" href="http://www.e-himawari.co.jp/?page_id=25809" alt="お客様はもちろん、社員同士でも感謝の気持ちを忘れない！">
                                    </map>
									<a href="http://www.e-himawari.co.jp/?page_id=34048"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_reserve.png" class="flr p_t10 hover"></a>
							</div><!-- /exit -->
      </div>
    </div>
    <!-- #content -->
  </section>

  </article>
  <?php include TEMPLATEPATH . '/sidebar-staff.php'; ?>
</div>

<?php get_footer(); ?>