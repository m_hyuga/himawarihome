<?php
/**
 * Template Name: kodawari_page
 */
?>
<?php

get_header(); ?>
<link href="http://www.e-himawari.co.jp/wordpress/img/style.css" rel="stylesheet" type="text/css" media="all">
<link href="http://www.e-himawari.co.jp/wordpress/img/style2.css" rel="stylesheet" type="text/css" media="all">
<div id="main_top">
</div>
<div id="main">
<div id="container">
<div id="content" role="main">

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
									

					<script language="JavaScript" type="text/JavaScript" src="http://www.e-himawari.co.jp/wordpress/main.js"></script>

					<div class="entry-content">
						<div id="content1">
							<table width="100%">
								<tr>
									<td width="50%" align="left"><h1 class="entry-title"><?php the_title(); ?></h1></td>
								</tr>
								<tr>
									<td width="50%" align="right"><b style="font-size : 10pt;"><a href="./?page_id=602">戻る</a></b></td>
								</tr>
							</table>
							<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
							<iframe src="./?page_id=4365&ID=<?php echo $post->ID ?>" name="under_iframe" width="790" height="700" frameborder="0" scrolling="NO"></iframe>
							<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
						</div>
					</div><!-- .entry-content -->
				</div><!-- #post-## -->

<?php endwhile; ?>
</div><!-- #content -->
            <?php get_sidebar(); ?>
</div><!-- #container -->


<?php get_footer(); ?>
