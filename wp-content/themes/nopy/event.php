<?php
/**
 * Template Name: event
 */
?>
<?php

get_header(); ?>
<link href="/img/style.css" rel="stylesheet" type="text/css" media="all">
<link href="/img/style2.css" rel="stylesheet" type="text/css" media="all">

           <div id="contents">
 <article id="contents_left">
   
      <section class="bnr_box height">
      
            <div id="content">
             <div class="bg_f">
<table width="790" border="0" cellpadding="0" cellspacing="0" style="margin-bottom:15px">
  <tr valign="top">
    <td align="right" valign="top">&nbsp;</td>
  </tr>
  <tr valign="top">
    <td align="right" valign="top">
      <a href="./?page_id=872"><img src="/wp-content/themes/nopy/images/tenji_banner.jpg"  width="229" height="59"></a>
    </td>
    
  </tr>
   <tr valign="top">
     <td align="right" valign="top">&nbsp;</td>
   </tr>
   <tr valign="top">
    <td align="right" valign="top"><a href="./?page_id=13666"><img src="/wp-content/uploads/2010/07/banner01.png" alt="ちょっと教えてコーナー" /></a></td>
  </tr>
</table>
<h2 class="topix_title2">イベント情報</h2>
<div id="topix_area">
<table border="0" cellpadding="0" cellspacing="0" style="padding-top:20px;">
<?php if ($post->ID == "1439") { ?>
<tr>
<td width="150" align="center"><img src="/wp-content/themes/nopy/images/area1_title.jpg"  width="210" height="34"></td>
<td width="150" align="center"><a href="./?page_id=4007" onMouseOver="MM_swapImage('Image1','','/wp-content/themes/nopy/images/area2_title.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/wp-content/themes/nopy/images/area2_title_white.jpg" name="Image1" width="210" height="34" border="0" id="Image1" style="margin-left:5px"></a></td>
<td width="150" align="center"><a href="./?page_id=4009" onMouseOver="MM_swapImage('Image2','','/wp-content/themes/nopy/images/area3_title.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/wp-content/themes/nopy/images/area3_title_white.jpg" name="Image2" width="210" height="34" border="0" id="Image2" style="margin-left:5px"></a></td>
</tr>
</table>
<table border="0" cellpadding="15" cellspacing="0" style="border-top:solid;border-top-width:2px;border-top-color:#DC6B00">
  <tr bgcolor="#FFFFFF" valign="top">
    <td>
      <div class="area4">
          <dl>
            <?php
              // ブログ取得(最終更新日)
              $posts1_1 = get_posts('numberposts=20&category=4&orderby=modified');
              $arr_posts1_1 = array();
              $cnt=0;
              $tmp_id ='';
              if($posts1_1){
                foreach($posts1_1 as $post){
                  $tmp_id .= $post->ID.',';
                  if ($post->post_modified >= $post->post_date){
                    $arr_posts1_1[$cnt] = $post->post_modified;
                  } else {
                    $arr_posts1_1[$cnt] = $post->post_date;
                  }
                  $cnt++;
                }
              }
              // ブログ取得(公開日)
              $posts1_2 = get_posts('numberposts=20&category=4&orderby=date&exclude='.$tmp_id);
              $arr_posts1_2 = array();
              $cnt=0;
              if($posts1_2){
                foreach($posts1_2 as $post){
                  $arr_posts1_2[$cnt] = $post->post_date;
                  $cnt++;
                }
              }
              // 配列のマージ
              $arr_posts1_3 = array_merge($arr_posts1_1,$arr_posts1_2);
              $posts1_3 = array_merge($posts1_1,$posts1_2);
              // ソート処理
              array_multisort($arr_posts1_3, SORT_DESC, $posts1_3, SORT_DESC);
            ?>
            <?php
              $cnt=0;
              if($posts1_3): foreach($posts1_3 as $post): setup_postdata($post); $cnt++;
            ?>
              <dd><a href="?p=<?php the_id(); ?>"><?php $title = the_title( '' , '' , false ); if($title!=''){ echo $title; } else { echo 'タイトルなし'; } ?></a><br><br><?php the_excerpt(); ?></dd>
            <?php
              if ($cnt >= 20){
                break;
              }
              endforeach; endif;
            ?>
          </dl>
      </div>
    </td>
  </tr>
<?php } else if ($post->ID == "4007") { ?>
<tr>
<td width="150" align="center"><a href="./?page_id=1439" onMouseOver="MM_swapImage('Image3','','/wp-content/themes/nopy/images/area1_title.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/wp-content/themes/nopy/images/area1_title_white.jpg" name="Image3" width="210" height="34" border="0" id="Image3" ></a></td>
<td width="150" align="center"><img src="/wp-content/themes/nopy/images/area2_title.jpg"  width="210" height="34" style="margin-left:5px"></td>
<td width="150" align="center"><a href="./?page_id=4009" onMouseOver="MM_swapImage('Image2','','/wp-content/themes/nopy/images/area3_title.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/wp-content/themes/nopy/images/area3_title_white.jpg" name="Image2" width="210" height="34" border="0" id="Image2" style="margin-left:5px"></a></td>
</tr>
</table>
<table border="0" cellpadding="15" cellspacing="0" style="border-top:solid;border-top-width:2px;border-top-color:#5B9400;">
  <tr bgcolor="#FFFFFF" valign="top">
    <td>
      <div class="area5">
          <dl>
            <?php
              // ブログ取得(最終更新日)
              $posts1_1 = get_posts('numberposts=20&category=5&orderby=modified');
              $arr_posts1_1 = array();
              $cnt=0;
              $tmp_id ='';
              if($posts1_1){
                foreach($posts1_1 as $post){
                  $tmp_id .= $post->ID.',';
                  if ($post->post_modified >= $post->post_date){
                    $arr_posts1_1[$cnt] = $post->post_modified;
                  } else {
                    $arr_posts1_1[$cnt] = $post->post_date;
                  }
                  $cnt++;
                }
              }
              // ブログ取得(公開日)
              $posts1_2 = get_posts('numberposts=20&category=5&orderby=date&exclude='.$tmp_id);
              $arr_posts1_2 = array();
              $cnt=0;
              if($posts1_2){
                foreach($posts1_2 as $post){
                  $arr_posts1_2[$cnt] = $post->post_date;
                  $cnt++;
                }
              }
              // 配列のマージ
              $arr_posts1_3 = array_merge($arr_posts1_1,$arr_posts1_2);
              $posts1_3 = array_merge($posts1_1,$posts1_2);
              // ソート処理
              array_multisort($arr_posts1_3, SORT_DESC, $posts1_3, SORT_DESC);
            ?>
            <?php
              $cnt=0;
              if($posts1_3): foreach($posts1_3 as $post): setup_postdata($post); $cnt++;
            ?>
              <dd><a href="?p=<?php the_id(); ?>"><?php $title = the_title( '' , '' , false ); if($title!=''){ echo $title; } else { echo 'タイトルなし'; } ?></a><br><br><?php the_excerpt(); ?></dd>
            <?php
              if ($cnt >= 20){
                break;
              }
              endforeach; endif;
            ?>
          </dl>
      </div>
    </td>
  </tr>
<?php } else if ($post->ID == "4009") { ?>
<tr>
<td width="150" align="center"><a href="./?page_id=1439" onMouseOver="MM_swapImage('Image3','','/wp-content/themes/nopy/images/area1_title.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/wp-content/themes/nopy/images/area1_title_white.jpg" name="Image3" width="210" height="34" border="0" id="Image3"></a></td>
<td width="150" align="center"><a href="./?page_id=4007" onMouseOver="MM_swapImage('Image1','','/wp-content/themes/nopy/images/area2_title.jpg',1)" onMouseOut="MM_swapImgRestore()"><img src="/wp-content/themes/nopy/images/area2_title_white.jpg" name="Image1" width="210" height="34" border="0" id="Image1" style="margin-left:5px"></a></td>
<td width="150" align="center"><img src="/wp-content/themes/nopy/images/area3_title.jpg"  width="210" height="34" style="margin-left:5px"></td>
</tr>
</table>
<table border="0" cellpadding="15" cellspacing="0" style="border-top:solid;border-top-width:2px;border-top-color:#1E568F">
  <tr bgcolor="#FFFFFF" valign="top">
    <td>
      <div class="area6">
          <dl>
            <?php
              // ブログ取得(最終更新日)
              $posts1_1 = get_posts('numberposts=20&category=6&orderby=modified');
              $arr_posts1_1 = array();
              $cnt=0;
              $tmp_id ='';
              if($posts1_1){
                foreach($posts1_1 as $post){
                  $tmp_id .= $post->ID.',';
                  if ($post->post_modified >= $post->post_date){
                    $arr_posts1_1[$cnt] = $post->post_modified;
                  } else {
                    $arr_posts1_1[$cnt] = $post->post_date;
                  }
                  $cnt++;
                }
              }
              // ブログ取得(公開日)
              $posts1_2 = get_posts('numberposts=20&category=6&orderby=date&exclude='.$tmp_id);
              $arr_posts1_2 = array();
              $cnt=0;
              if($posts1_2){
                foreach($posts1_2 as $post){
                  $arr_posts1_2[$cnt] = $post->post_date;
                  $cnt++;
                }
              }
              // 配列のマージ
              $arr_posts1_3 = array_merge($arr_posts1_1,$arr_posts1_2);
              $posts1_3 = array_merge($posts1_1,$posts1_2);
              // ソート処理
              array_multisort($arr_posts1_3, SORT_DESC, $posts1_3, SORT_DESC);
            ?>
            <?php
              $cnt=0;
              if($posts1_3): foreach($posts1_3 as $post): setup_postdata($post); $cnt++;
            ?>
              <dd><a href="?p=<?php the_id(); ?>"><?php $title = the_title( '' , '' , false ); if($title!=''){ echo $title; } else { echo 'タイトルなし'; } ?></a><br><br><?php the_excerpt(); ?></dd>
            <?php
              if ($cnt >= 20){
                break;
              }
              endforeach; endif;
            ?>
          </dl>
      </div>
    </td>
  </tr>
<?php } ?>
</table>
</div>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
									

					<script language="JavaScript" type="text/JavaScript" src="/main.js"></script>

					<div class="entry-content">
							<?php the_content(); ?>
							<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
							<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-content -->
				</div><!-- #post-## -->

<?php endwhile; ?>
  </div></div>
            <!-- #content -->
         
		</section>
  </article>
<?php include TEMPLATEPATH . '/sidebar-page.php'; ?>

</div>



<?php get_footer(); ?>
