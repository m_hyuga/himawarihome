<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
   <div id="contents">
 <article id="contents_left">
   
      <section class="bnr_box height">
      
            <div id="content">
             <div class="bg_f">

 
<div id="staf_index">
<h2><img src="/wp-content/themes/nopy/images/staff/top/title01.jpg" alt="スタッフにあいに行こう！！" width="749" height="129" /></h2>

<div class="search_box">

<form id="frmReqStaffs">
<!-- 役職 -->
<h3><img src="/wp-content/themes/nopy/images/staff/top/title02.jpg" alt="役職・部署" width="749" height="65" /></h3>

<div class="search_contents">
	<div class="search_check01">
		<UL>
			<LI class="m01l"><img width="79" height="119" src="/wp-content/themes/nopy/images/staff/top/cat-02-01.png"><input type="checkbox" name="role[]" value="manager"></LI>
			<LI><img width="79" height="119" src="/wp-content/themes/nopy/images/staff/top/cat-02-02.png"><input type="checkbox" name="role[]" value="cis_sales"></LI>
			<LI><img width="79" height="119" src="/wp-content/themes/nopy/images/staff/top/cat-02-03.png"><input type="checkbox" name="role[]" value="cis_manifacture"></LI>
			<LI><img width="79" height="119" src="/wp-content/themes/nopy/images/staff/top/cat-02-04.png"><input type="checkbox" name="role[]" value="development"></LI>
			<LI><img width="79" height="119" src="/wp-content/themes/nopy/images/staff/top/cat-02-05.png"><input type="checkbox" name="role[]" value="cis_manage"></LI>
			<LI><img width="79" height="119" src="/wp-content/themes/nopy/images/staff/top/cat-02-06.png"><input type="checkbox" name="role[]" value="reform"></LI>
			<LI><img width="79" height="119" src="/wp-content/themes/nopy/images/staff/top/cat-02-07.png"><input type="checkbox" name="role[]" value="officework"></LI>
		</UL>
	</div>
</div>

<!-- イメージ -->
<h3><img src="/wp-content/themes/nopy/images/staff/top/title03.jpg" alt="スタッフイメージ" width="749" height="96" /></h3>
<div class="search_contents">
<div class="search_check02">
<ul>
			<LI class="m02l"><img width="79" height="108" src="/wp-content/themes/nopy/images/staff/top/cat-01-01.png"><input type="checkbox" name="charactor[]" value="stylish"></LI>
			<LI><img width="79" height="108" src="/wp-content/themes/nopy/images/staff/top/cat-01-02.png"><input type="checkbox" name="charactor[]" value="physical"></LI>
			<LI><img width="79" height="108" src="/wp-content/themes/nopy/images/staff/top/cat-01-03.png"><input type="checkbox" name="charactor[]" value="geek"></LI>
			<LI><img width="79" height="108" src="/wp-content/themes/nopy/images/staff/top/cat-01-04.png"><input type="checkbox" name="charactor[]" value="cute"></LI>
			<LI><img width="79" height="108" src="/wp-content/themes/nopy/images/staff/top/cat-01-05.png"><input type="checkbox" name="charactor[]" value="handsome"></LI>
			<LI><img width="79" height="108" src="/wp-content/themes/nopy/images/staff/top/cat-01-06.png"><input type="checkbox" name="charactor[]" value="pretty"></LI>
			<LI><img width="79" height="108" src="/wp-content/themes/nopy/images/staff/top/cat-01-07.png"><input type="checkbox" name="charactor[]" value="familish"></LI>
			<LI><img width="79" height="108" src="/wp-content/themes/nopy/images/staff/top/cat-01-08.png"><input type="checkbox" name="charactor[]" value="gourmet"></LI>
			<LI><img width="79" height="108" src="/wp-content/themes/nopy/images/staff/top/cat-01-09.png"><input type="checkbox" name="charactor[]" value="funny"></LI>
</ul>
</div>
</div>

<img src="/wp-content/themes/nopy/images/staff/top/img_bottom.jpg" alt="" width="749" height="24" />
<div class="search_bt">
<img id="btnRequest" src="/wp-content/themes/nopy/images/staff/top/bt01.jpg" />
</div>
</form>

<!-- 結果表示 -->
<h3><img width="749" height="39" src="himawari.files/title04.jpg"></h3>
<div class="search_check03">
	<ul>
		<LI>
			<img width="68" height="114" src="himawari.files/img.png">
			<span>CIS推進施工部<br>辻 菜央子</span><a href="http://www.e-himawari.co.jp/?page_id=30743#">詳細をみる</a>
		</LI> 
	</ul>
</div>

</div>
</div>

		 </div></div>
            <!-- #content -->
         
		</section>
  </article>
<?php include TEMPLATEPATH . '/sidebar-staff.php'; ?>

</div>



<?php get_footer(); ?>