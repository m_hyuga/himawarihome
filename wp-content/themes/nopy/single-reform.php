<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<link href="http://www.e-himawari.co.jp/wordpress/img/style.css" rel="stylesheet" type="text/css" media="all">
<div id="contents">
 <article id="contents_left">
   
      <section class="bnr_box height">
      
            <div id="content">
             <div class="bg_f">




<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
               
                 <div id="reform_con">
				 
                	<h3 class="reform"><?php the_title(); ?></h3>

					
					<div class="entry-content">
						<?php the_content(); ?>
                        
                        
                        <?php if (has_term('reform01','reform_cat')) { ?>
                       <?php include TEMPLATEPATH . '/reform01.php'; ?>
                       
                        <?php } elseif (has_term('reform02','reform_cat')) { ?>
                        <?php include TEMPLATEPATH . '/reform02.php'; ?>
                        
                        <?php } elseif (has_term('reform03','reform_cat')) { ?>
                        <?php include TEMPLATEPATH . '/reform03.php'; ?>
                        
						<?php } ?>
                        
                        
          <br>
          <br>
          <br>
					
          

          
          
					<!-- 出口誘導 -->
					<div class="exit">
						<div class="control">
						<?php next_post_link_plus( array('format' => '%link', 'link' => '<img src="http://www.e-himawari.co.jp/wp-content/themes/nopy/images/deguti/btn_pre.png" alt="前へ" class="hover">', 'in_same_tax' => true) ); ?>
						<a href="/?post_type=reform"><img src="<?php bloginfo('template_url'); ?>/images/deguti/btn_table.png" alt="一覧へ"></a>
						<?php previous_post_link_plus( array('format' => '%link', 'link' => '<img src="http://www.e-himawari.co.jp/wp-content/themes/nopy/images/deguti/btn_next.png" alt="次へ" class="hover">', 'in_same_tax' => true) ); ?>
					</div>
					<img src="<?php bloginfo('template_url'); ?>/images/deguti/reform_img_01.png">
						<img src="<?php bloginfo('template_url'); ?>/images/deguti/reform_img_02.png">
						<div class="btn_mail"><a href="/?page_id=34048"><img src="<?php bloginfo('template_url'); ?>/images/deguti/btn_mail.png"></a></div>
						<img src="<?php bloginfo('template_url'); ?>/images/deguti/reform_img_04.png">
						<img src="<?php bloginfo('template_url'); ?>/images/deguti/reform_img_05.png">
					
<!-- 誘導バナー -->
<a href="http://www.e-himawari.co.jp/?page_id=34048"><img src="/wp-content/themes/nopy/images/bnrShuffle/bnr_l_02.png" alt="" style="margin-top:25px"></a>
<!-- end -->

					<div class="entry-utility">
						
					<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
					</div><?php if(function_exists('echo_ald_wherego')) echo_ald_wherego(); ?> <!-- .これが関連記事 --><!-- .entry-utility -->
                    
                    
                    
               
                 </div>    
				</div><!-- #post-## -->

				
        

			
<?php endwhile; // end of the loop. ?>



		 </div></div></div></div>
            <!-- #content -->
         
		</section>
  </article>
<?php include TEMPLATEPATH . '/sidebar-sekou.php'; ?>
</div>


<?php get_footer(); ?>
