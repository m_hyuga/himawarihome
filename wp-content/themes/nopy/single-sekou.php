<?php
/**
* The Template for displaying all single posts.
*
* @package WordPress
* @subpackage Twenty_Ten
* @since Twenty Ten 1.0
*/
get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<link href="http://www.e-himawari.co.jp/wordpress/img/style.css" rel="stylesheet" type="text/css" media="all">
<div id="contents">
<article id="contents_left">
	<section class="bnr_box height">
                <?php if (has_term('cat01','sekou_cat')) { ?>
                <div id="content" class="bg_sekou detail_page">
                    <div class="bg_dots"><?php } ?>
                <?php if (has_term('cat04','sekou_cat')) { ?>
                <div id="content" class="bg_tenji detail_page">
                    <div class="bg_dots">
					<div class="sekou"><?php } ?>
					<!-- 以下、↓の各テンプレートに記載されています。
					＜div id="sekou_con"＞
						＜h3 class="sekou">タイトルが入ります。＜/h3＞
						＜div class="entry-content"＞
							＜？php the_content(); ？＞
					-->
							<!-- 施工実績 詳細ページ ここから -->
							<?php if (has_term('cat01-01','sekou_cat')) { ?>
							<?php include TEMPLATEPATH . '/sekou01.php'; ?>
							<?php } elseif (has_term('cat01-02','sekou_cat')) { ?>
							<?php include TEMPLATEPATH . '/sekou02.php'; ?>
							<?php } elseif (has_term('cat01-03','sekou_cat')) { ?>
							<?php include TEMPLATEPATH . '/sekou03.php'; ?>
							<!-- 施工実績 詳細ページここまで -->

							<!-- 展示場 詳細ページ ここから -->
							<?php } elseif (has_term('cat02-01','sekou_cat')) { ?>
							<?php include TEMPLATEPATH . '/sekou04.php'; ?>
							<?php } elseif (has_term('cat02-02','sekou_cat')) { ?>
							<?php include TEMPLATEPATH . '/sekou05.php'; ?>
							<?php } elseif (has_term('cat02-03','sekou_cat')) { ?>
							<?php include TEMPLATEPATH . '/sekou06.php'; ?>
							<!-- 展示場 詳細ページ ここまで -->

							<?php } ?>
						</div><!-- .entry-content -->
						<div class="entry-utility">
							<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
						</div>
						<?php if(function_exists('echo_ald_wherego')) echo_ald_wherego(); ?> <!-- .これが関連記事 --><!-- .entry-utility -->
					</div>
				</div><!-- #post-## -->
				<?php endwhile; // end of the loop. ?>
			</div>
			</div>
			
		</div><!-- #content -->
	</section>
</article>
<?php include TEMPLATEPATH . '/sidebar-sekou.php'; ?>
</div>
<?php get_footer(); ?>