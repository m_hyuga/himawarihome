<?php
	/**
	* The template for displaying Category Archive pages.
	*
	* @package WordPress
	* @subpackage Twenty_Ten
	* @since Twenty Ten 1.0
	*/
get_header(); ?>
<div id="contents">
	<article id="contents_left">
		<section class="bnr_box height">
			<div id="content" class="bg_voice">
				<div class="bg_dots">
					<div class="user_voice">
						<h2><img src="/wp-content/themes/nopy/images/voice/h2_head_titie.png" alt="お客様の声"></h2>
						<ul class="taxonomy_new">
							<?php if (have_posts()) : ?>
							<?php is_main_query($query_string.'&posts_per_page=6'); ?>
							<?php while (have_posts()) : the_post();
							/* ループ開始 */
							?>
							<li>
								<a href="<?php the_permalink(); ?>">
									<h3><?php the_title(); ?></h3>
									<span>
										<?php
											$imagefield = get_imagefield('ichiran-photo01');
											$attachment = get_attachment_object($imagefield['id']);
											$imgattr = wp_get_attachment_image_src($imagefield['id'], 'full'); // thumbnail, large, medium, full を指定
											echo '<image src="' . $imgattr[0] . '" width="343px' . $imgattr[1] . '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" />';
										?>
									</span>
									<p><?php echo mb_substr(strip_tags($post-> post_content),0,80).'...'; ?></p>
								</a>
							</li>
							<?php endwhile; ?>
							<?php else : ?>
							<?php endif; ?>
						</ul>
						<?php wp_pagenavi(); ?>

<!-- 誘導バナー -->
<ul id="bnr_large"></ul>
<!-- end -->

<!-- 誘導バナー -->
<ul id="bnr_twin"></ul>
<!-- end -->
		
						
						<!-- 出口誘導 -->
						<div class="exit">
							<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_anxiety.png" alt="住宅会社に対してこんな心配ありませんか？">
							<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_relief.png" alt="ご安心下さい！ひまわりほーむは" class="p_t10">
							<div class="oh m_t25 m_b18">
								<a href="/?page_id=25194"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_media.png" alt="メディア・書籍等でもひまわりほーむが紹介されています"></a>
								<a href="/?page_id=121"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_festival.png" alt="感謝祭夏まつり・雪まつり" class="m_l20"></a>
									<a href="/?page_id=69"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_family.png" alt="お客様とは家族という関係でありたいと願っています" class="m_b10 flr hover"></a>
									<a href="/?post_type=voice"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_senior.png" alt="実はさらに先輩の生の声が聞けるんです" class="flr"></a>
							</div>
							<a href="/?page_id=34048"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_conf.png" alt="まじは匿名で相談してみませんか？"></a>
						<br><br><!-- /exit -->
                         
</div>
					</div>
				</div>
			</div>
			<!-- #content -->
		</section>
	</article>
	<?php include TEMPLATEPATH . '/sidebar-sekou.php'; ?>
</div>
<?php get_footer(); ?>