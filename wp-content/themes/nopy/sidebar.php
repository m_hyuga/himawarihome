<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

	<article id="contents_right" class="height">
	<div id="side">
     <div class="contents_inner">
     
    
     
      
       <section class="ranking_area">
         <h2><img src="<?php bloginfo('template_url'); ?>/images/common/side/recommend.png" width="231" height="115"></h2>
         <div class="recommend">
           <ul>
             <li><a><img src="<?php bloginfo('template_url'); ?>/images/common/side/dammy.jpg"><p>ひまわりほーむの人気…</p></a></li>
             <li><a><img src="<?php bloginfo('template_url'); ?>/images/common/side/dammy.jpg"><p>ひまわりほーむの人気…</p></a></li>
             <li><a><img src="<?php bloginfo('template_url'); ?>/images/common/side/dammy.jpg"><p>ひまわりほーむの人気…</p></a></li>
             <li><a><img src="<?php bloginfo('template_url'); ?>/images/common/side/dammy.jpg"><p>ひまわりほーむの人気…</p></a></li>
             <li><a><img src="<?php bloginfo('template_url'); ?>/images/common/side/dammy.jpg"><p>ひまわりほーむの人気…</p></a></li>
             <li><a><img src="<?php bloginfo('template_url'); ?>/images/common/side/dammy.jpg"><p>ひまわりほーむの人気…</p></a></li>
             <li><a><img src="<?php bloginfo('template_url'); ?>/images/common/side/dammy.jpg"><p>ひまわりほーむの人気…</p></a></li>
             <li><a><img src="<?php bloginfo('template_url'); ?>/images/common/side/dammy.jpg"><p>ひまわりほーむの人気…</p></a></li>
             <li><a><img src="<?php bloginfo('template_url'); ?>/images/common/side/dammy.jpg"><p>ひまわりほーむの人気…</p></a></li>
           </ul>
         </div>
       </section>
      
      <section class="concern_area">
        <h2><img src="<?php bloginfo('template_url'); ?>/images/common/side/contents_top.png" width="231" height="103"></h2>
         <div class="recommend height02">
           <ul>
             <li><a href="/?page_id=971"><img src="<?php bloginfo('template_url'); ?>/images/common/side/sam/dammy01.jpg"><p>ひまわり家づくり人気…</p></a></li>
             <li><a href="/?page_id=87"><img src="<?php bloginfo('template_url'); ?>/images/common/side/sam/dammy02.jpg"><p>全棟無垢材</p></a></li>
             <li><a href="/?page_id=582"><img src="<?php bloginfo('template_url'); ?>/images/common/side/sam/dammy03.jpg"><p>素敵にリフォーム</p></a></li>
             <li><a href="/?page_id=19456"><img src="<?php bloginfo('template_url'); ?>/images/common/side/sam/dammy04.jpg"><p>全棟省エネ</p></a></li>
             <li><a href="/?page_id=22998"><img src="<?php bloginfo('template_url'); ?>/images/common/side/sam/dammy05.jpg"><p>富山まるごと実験＆…</p></a></li>
             <li><a href="/?page_id=30287"><img src="<?php bloginfo('template_url'); ?>/images/common/side/sam/dammy06.jpg"><p>社長かばたついて</p></a></li>
             <li><a href="/?page_id=30277"><img src="<?php bloginfo('template_url'); ?>/images/common/side/sam/dammy07.jpg"><p>イラストレーター紹介</p></a></li>
             <li><a href="/?page_id=17500"><img src="<?php bloginfo('template_url'); ?>/images/common/side/sam/dammy08.jpg"><p>全棟耐震</p></a></li>
             <li><a href="/?page_id=13666"><img src="<?php bloginfo('template_url'); ?>/images/common/side/sam/dammy09.jpg"><p>お問い合わせ</p></a></li>
             <li><a href="/?page_id=30346"><img src="<?php bloginfo('template_url'); ?>/images/common/side/sam/dammy10.jpg"><p>八日市出町モデル</p></a></li>
             <li><a href="http://himawarikizuna.shop-pro.jp/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/common/side/sam/dammy11.jpg"><p>ひまわりきづな…</p></a></li>
             <li><a href="/?page_id=25031"><img src="<?php bloginfo('template_url'); ?>/images/common/side/sam/dammy12.jpg"><p>能登ヒバツアー</p></a></li>
           </ul>
         </div>
       </section>
     </div>
       <section id="blog_right">
        <h2><img src="<?php bloginfo('template_url'); ?>/images/common/blog_title.png" width="231" height="75"></h2>
       <a href="/?page_id=30287"><img src="<?php bloginfo('template_url'); ?>/images/common/blog_syatyou.jpg" alt=""></a>
        
        
        <ul>
        <li><a href="http://himawarihomeblog.blog.fc2.com/entry-11582373653.html" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/common/blog01.png" alt=""></a></li>
        <li><a href="http://himawarihomeblog.seesaa.net/entry-11576455187.html" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/common/blog02.png" alt=""></a></li>
        <li><a href="http://himawarihomeblog.blog.jp/entry-11571801512.html" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/common/blog03.png" alt=""></a></li>
        <li><a href="https://www.facebook.com/pages/%E3%81%B2%E3%81%BE%E3%82%8F%E3%82%8A%E3%81%BB%E3%83%BC%E3%82%80/416995395004441" target="_blank"><img src="http://www.e-himawari.co.jp/wp-content/themes/nopy/images/common/facebook.png" alt=""></a></li>
       
        
        </ul>
        

        
       </section>
	</div><!-- #side -->
	</article>





