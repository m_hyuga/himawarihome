<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">

 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />

<meta name="description"content="無垢材・自然素材・性能表示・耐震・省エネにこだわり、新築する全ての住宅を長期優良住宅とし、お客様が安心・納得・満足される健康住宅を提供しています。世界的な建築家である西沢立衛氏との共同プロジェクトも進行中です。"/>
<meta name="Keywords"content="長期優良住宅，無垢材，自然素材，性能表示，石川県，富山県，東京都，西沢立衛，ひまわりほーむ"/>
<meta name="viewport" content="target-densitydpi=device-dpi, width=1150px, maximum-scale=1.0, user-scalable=yes">

<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 * We filter the output of wp_title() a bit -- see
	 * twentyten_filter_wp_title() in functions.php.
	 */
	wp_title( '|', true, 'right' );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style.css" type="text/css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/common.css" type="text/css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/top.css" type="text/css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/deguti.css" type="text/css">
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-21257881-1']);
  _gaq.push(['_trackPageview']);
 
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
 </script>
 
<?php wp_head(); ?>
<script type="text/JavaScript" src="<?php bloginfo('template_url'); ?>/js/sidescroll.js"></script>
 
 <script type="text/javascript">
$(function () {
  $('#gnav li a').hover(function() {
    $(this).next('span').show();
  }, function(){
    $(this).next('span').hide();
  });
});

</script>
<script type="text/javascript">
//要素の高さを揃える
$(function(){
	$(".height").tile(2); 
	$(".height04").tile(2); 	
});
</script>
 <!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
 
 <style type="text/css" media="screen">
		html { margin-top: 0px !important; }
	* html body { margin-top: 0px !important; }
</style>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>  
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/smp.js"></script>

</head>

<body>
<div data-shutto-display="match" style="display: none;" id="sp"><a href="javascript:__shutto.view('mobile')">スマホ向け表示</a></div>
<div id="wrapper">
	<header>
		<div id="header">
			<div class="header_inner">
				<div class="header_right">
					<ul class="outline clearfix">
						<li><img src="<?php bloginfo('template_url'); ?>/images/common_new/img_phone.png" alt=""></li>
						<li><a href="/?page_id=34048"><img src="<?php bloginfo('template_url'); ?>/images/common_new/btn_contact.png" alt="お問い合わせ・資料請求" class="hover"></a></li>
					</ul>
					<ul class="head_nav">
						<li><a href="/">トップ</a></li>
						<li><a href="/?page_id=34">会社案内</a></li>
						<li><a href="http://e-himawarifudousan.com/" target="_blank">不動産情報</a></li>
						<li><a href="/?page_id=38">採用情報</a></li>
						<li><a href="/?page_id=2119">サイトマップ</a></li>
					</ul>
					<div class="facebook">
					<iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.e-himawari.co.jp%2F&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=true&amp;share=false&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowtransparency="true"></iframe>
				</div>
				</div>
				<h1><a href="/"><img src="<?php bloginfo('template_url'); ?>/images/common/header/logo.png" alt="ひまわりほーむ"></a></h1>
			</div>
		</div>
	</header>
	<nav id="grobal">
		<ul id="gnav">
			<li class="boder_nav"><a href="/?page_id=971"><img src="<?php bloginfo('template_url'); ?>/images/common_new/nav/nav01.jpg" width="116" height="103" alt="「全棟」にこだわる家づくり"/></a>
			<span class="arrow_box01"><img src="<?php bloginfo('template_url'); ?>/images/common_new/nav/nav01.png" alt="「全棟」にこだわる家づくり"/></span>
			</li>
			<li class="boder_nav"><a href="/?page_id=1091"><img src="<?php bloginfo('template_url'); ?>/images/common_new/nav/nav02.jpg" width="116" height="103" alt="明るく元気で前向き スタッフ紹介"/></a>
			<span class="arrow_box01"><img src="<?php bloginfo('template_url'); ?>/images/common_new/nav/nav02.png" alt="明るく元気で前向き スタッフ紹介"/></span>
			</li>
			<li class="boder_nav"><a href="/?sekou_cat=cat01"><img src="<?php bloginfo('template_url'); ?>/images/common_new/nav/nav03.jpg" width="116" height="103" alt="提案がいっぱい 施工事例集"/></a>
			<span class="arrow_box01"><img src="<?php bloginfo('template_url'); ?>/images/common_new/nav/nav03.png" alt="「全棟」にこだわる家づくり"/></span>
			</li>
			<li class="boder_nav"><a href="/?post_type=voice"><img src="<?php bloginfo('template_url'); ?>/images/common_new/nav/nav04.jpg" width="116" height="103" alt="お客様の声です 感謝"/></a>
			<span class="arrow_box01"><img src="<?php bloginfo('template_url'); ?>/images/common_new/nav/nav04.png" alt="お客様の声です 感謝"/></span>
			</li>
			<li class="boder_nav"><a href="/?sekou_cat=cat04"><img src="<?php bloginfo('template_url'); ?>/images/common_new/nav/nav05.jpg" width="116" height="103" alt="今週のイベントと展示場情報"/></a>
			<span class="arrow_box01"><img src="<?php bloginfo('template_url'); ?>/images/common_new/nav/nav05.png" alt="今週のイベントと展示場情報"/></span>
			</li>
			<li class="boder_nav"><a href="/?page_id=582"><img src="<?php bloginfo('template_url'); ?>/images/common_new/nav/nav06.jpg" width="116" height="103" alt="これからのリフォーム"/></a>
			<span class="arrow_box01"><img src="<?php bloginfo('template_url'); ?>/images/common_new/nav/nav06.png" alt="これからのリフォーム"/></span>
			</li>
			<li class="boder_nav"><a href="/?page_id=30564"><img src="<?php bloginfo('template_url'); ?>/images/common_new/nav/nav07.jpg" width="116" height="103" alt="イマドキの2世帯住宅"/></a>
			<span class="arrow_box01 arrow_boxlast02"><img src="<?php bloginfo('template_url'); ?>/images/common_new/nav/nav07.png" alt="イマドキの2世帯住宅"/></span>
			</li>
			<li><a href="/?page_id=25194"><img src="<?php bloginfo('template_url'); ?>/images/common_new/nav/nav08.jpg" width="116" height="103" alt="メディア・書籍等に紹介されました"/></a>
			<span class="arrow_box01 arrow_boxlast"><img src="<?php bloginfo('template_url'); ?>/images/common_new/nav/nav08.png" alt="メディア・書籍等に紹介されました"/></span>
			</li>
		</ul>
	</nav>
	<div id="contents_bg_index">
  
  
  
 