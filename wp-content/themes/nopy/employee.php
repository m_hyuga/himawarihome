<?php
/**
 * Template Name: employee
 * 
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 * We filter the output of wp_title() a bit -- see
	 * twentyten_filter_wp_title() in functions.php.
	 */
	wp_title( '|', true, 'right' );

	?></title>

<script language="JavaScript" type="text/JavaScript" src="./main.js"></script>
<link href="./img/style.css" rel="stylesheet" type="text/css" media="all">
<div id="content1">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<?php the_content('<p class="serif">' . __('Read the rest of this page &raquo;', 'kubrick') . '</p>'); ?>
<?php endwhile; endif; ?>
</div>
