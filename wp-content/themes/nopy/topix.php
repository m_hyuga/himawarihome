<?php
/**
 * Template Name: topix
 */
?>

<?php get_header(); ?>
<link href="http://www.e-himawari.co.jp/wordpress/img/style.css" rel="stylesheet" type="text/css" media="all">
<link href="http://www.e-himawari.co.jp/wordpress/img/style2.css" rel="stylesheet" type="text/css" media="all">
<div id="main_top">
</div>
<div id="main">
  <div id="container">
    <div id="content" role="main">
      <div id="topix_area3">
          <h4 class="h_topix_title1">ひまわりほーむトピックス</h4>
            <dl>
              <?php
              // ページ取得(最終更新日)
              $posts1_1 = get_posts('numberposts=0&post_type=page&post_parent=1076&orderby=modified');
              $arr_posts1_1 = array();
              $cnt=0;
              $tmp_id ='';
              if($posts1_1){
                foreach($posts1_1 as $post){
                  $tmp_id .= $post->ID.',';
                  if ($post->post_modified >= $post->post_date){
                      $arr_posts1_1[$cnt] = $post->post_modified;
                  } else {
                      $arr_posts1_1[$cnt] = $post->post_date;
                  }
                  $cnt++;
                }
              }

              // ページ取得(公開日)
              $posts1_2 = get_posts('numberposts=0&post_type=page&post_parent=1076&orderby=date&exclude='.$tmp_id);
              $arr_posts1_2 = array();
              $cnt=0;
              if($posts1_2){
                foreach($posts1_2 as $post){
                  $arr_posts1_2[$cnt] = $post->post_date;
                  $cnt++;
                }
              }

              // ブログ取得(最終更新日)
              $posts2_1 = get_posts('numberposts=20&category=130&orderby=modified');
              $arr_posts2_1 = array();
              $cnt=0;
              $tmp_id ='';
              if($posts2_1){
                foreach($posts2_1 as $post){
                  $tmp_id .= $post->ID.',';
                  if ($post->post_modified >= $post->post_date){
                      $arr_posts2_1[$cnt] = $post->post_modified;
                  } else {
                      $arr_posts2_1[$cnt] = $post->post_date;
                  }
                  $cnt++;
                }
              }

              // ブログ取得(公開日)
              $posts2_2 = get_posts('numberposts=20&category=130&orderby=date&exclude='.$tmp_id);
              $arr_posts2_2 = array();
              $cnt=0;
              if($posts2_2){
                foreach($posts2_2 as $post){
                  $arr_posts2_2[$cnt] = $post->post_date;
                  $cnt++;
                }
              }

              // 配列のマージ
              $arr_posts3 = array_merge($arr_posts1_1,$arr_posts1_2,$arr_posts2_1,$arr_posts2_2);
              $posts3 = array_merge($posts1_1,$posts1_2,$posts2_1,$posts2_2);

              // ソート処理
              array_multisort($arr_posts3, SORT_DESC, $posts3, SORT_DESC)

              ?>
              <?php
              $cnt = 0;
              if($posts3): foreach($posts3 as $post): setup_postdata($post);
                if ($post->post_type == 'page'){ ?>
                  <dd><div id="content1">
                    <table width="100%">
                      <tr>
                        <td><a href="./?page_id=<?php the_id(); ?>"><?php $title = the_title( '' , '' , false ); if($title!=''){ echo $title; } else { echo 'タイトルなし'; } ?></a><?php the_excerpt(); ?></td>
                        <td style="text-align:right;font-size:12px;font-weight: normal;">
                          <?php if ($post->post_modified >= $post->post_date){ 
                            the_modified_date('Y年m月d日');
                          } else { 
                            echo get_the_time('Y').'年'.get_the_time('n').'月'.get_the_time('j').'日';
                          } ?>
                        </td>
                      </tr>
                    </table>
                  </div></dd>
                <?php } else { $cnt++; ?>
                  <dd><div id="content1">
                    <table width="100%">
                      <tr>
                        <td><a href="./?p=<?php the_id(); ?>"><?php $title = the_title( '' , '' , false ); if($title!=''){ echo $title; } else { echo 'タイトルなし'; } ?></a><?php the_excerpt(); ?></td>
                        <td style="text-align:right;font-size:12px;font-weight: normal;">
                          <?php if ($post->post_modified >= $post->post_date){ 
                            the_modified_date('Y年m月d日');
                          } else { 
                            echo get_the_time('Y').'年'.get_the_time('n').'月'.get_the_time('j').'日';
                          } ?>
                        </td>
                      </tr>
                    </table>
                  </div></dd>
                <?php }
               if ($cnt >= 25){
                   break;
               }
              endforeach; endif;
              ?>
            </dl>
            <div id="content1" align="right">
            <table>
              <tr>
                <td style="text-align:right;font-size:13px;font-weight: bold;">
                  <a href="?cat=130">ブログを全部見る</a>
                </td>
              </tr>
            </table>
            </div>
          </div>
        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
          <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <script language="JavaScript" type="text/JavaScript" src="http://www.e-himawari.co.jp/wordpress/main.js"></script>
            <div class="entry-content">
              <div id="content1">
                <?php the_content(); ?>
                <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
                <?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
              </div>
            </div><!-- .entry-content -->
          </div><!-- #post-## -->
        <?php endwhile; ?>
      </div><!-- #content -->
      <?php get_sidebar(); ?>
    </div><!-- #container -->
<?php get_footer(); ?>
