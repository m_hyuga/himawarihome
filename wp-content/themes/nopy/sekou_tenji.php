<div class="content_tenjijyo">
	<div class="the_contentarea">
		<img src="/wp-content/themes/nopy/images/sekou/tenji/title_koshin.png" alt="更新情報">
		<div class="news_contentarea">
		<?php
		$page_id = 37861;    //xxxに 固定ページIDを入力
		$content = get_page($page_id);
		echo $content->post_content;
		?>
		</div>
	</div>
<h1 class="page-title">
<?php if(is_tax('sekou_cat', 'ishikawa')): ?>
            <span class="tenji_main_title"><img src="<?php bloginfo('template_url'); ?>/images/sekou/tenji/h2_head_titie_ishikawa.png" alt="石川県の展示場情報"></span></h1>  
						<ul class="tenjijyo_btn cf">
								<li><img src="<?php bloginfo('template_url'); ?>/images/sekou/btn_ishikawa_now.png" alt="石川県" width="122" height="38">
								</li>
								<li>
									<a href="/?sekou_cat=toyama"><img src="<?php bloginfo('template_url'); ?>/images/sekou/btn_toyama.png" alt="富山県" width="122" height="38"></a>
								</li>
								<li>
									<a href="/?sekou_cat=syutoken"><img src="<?php bloginfo('template_url'); ?>/images/sekou/btn_shutoken.png" alt="首都圏" width="122" height="38"></a>
								</li>
						</ul>
						
	<div class="tenjijyo_box_page tenjijyo_box_page_i">
		<div id="event_ticker01"></div>
	</div>
					
<?php  elseif(is_tax('sekou_cat', 'toyama')): ?>
            <span class="tenji_main_title"><img src="<?php bloginfo('template_url'); ?>/images/sekou/tenji/h2_head_titie_toyama.png" alt="富山県の展示場情報"></span></h1>  
						<ul class="tenjijyo_btn cf">
								<li><a href="/?sekou_cat=ishikawa"><img src="<?php bloginfo('template_url'); ?>/images/sekou/btn_ishikawa.png" alt="石川県" width="122" height="38"></a>
								</li>
								<li>
									<img src="<?php bloginfo('template_url'); ?>/images/sekou/btn_toyama_now.png" alt="富山県" width="122" height="38">
								</li>
								<li>
									<a href="/?sekou_cat=syutoken"><img src="<?php bloginfo('template_url'); ?>/images/sekou/btn_shutoken.png" alt="首都圏" width="122" height="38"></a>
								</li>
						</ul>
						
	<div class="tenjijyo_box_page tenjijyo_box_page_t">
		<div id="event_ticker02"></div>
	</div>
					
<?php  elseif(is_tax('sekou_cat', 'syutoken')): ?>
            <span class="tenji_main_title"><img src="<?php bloginfo('template_url'); ?>/images/sekou/tenji/h2_head_titie_shutoken.png" alt="首都圏の展示場情報"></span></h1>  
						<ul class="tenjijyo_btn cf">
								<li><a href="/?sekou_cat=ishikawa"><img src="<?php bloginfo('template_url'); ?>/images/sekou/btn_ishikawa.png" alt="石川県" width="122" height="38"></a>
								</li>
								<li>
									<a href="/?sekou_cat=toyama"><img src="<?php bloginfo('template_url'); ?>/images/sekou/btn_toyama.png" alt="富山県" width="122" height="38"></a>
								</li>
								<li>
									<img src="<?php bloginfo('template_url'); ?>/images/sekou/btn_shutoken_now.png" alt="首都圏" width="122" height="38">
								</li>
						</ul>
						
	<div class="tenjijyo_box_page tenjijyo_box_page_s">
		<div id="event_ticker03"></div>
	</div>
					
<?php  else: ?>
<span class="tenji_main_title"><img src="<?php bloginfo('template_url'); ?>/images/sekou/tenji/h2_head_titie.png" alt="展示場情報"></span></h1>  
	<ul class="tenjijyo_btn cf">
			<li>
				<a href="/?sekou_cat=ishikawa">
					<img src="<?php bloginfo('template_url'); ?>/images/sekou/btn_ishikawa.png" alt="石川県" width="122" height="38" class="hover" >
				</a>
			</li>
			<li>
				<a href="/?sekou_cat=toyama">
					<img src="<?php bloginfo('template_url'); ?>/images/sekou/btn_toyama.png" alt="富山県" width="122" height="38" class="hover" >
				</a>
			</li>
			<li>
				<a href="/?sekou_cat=syutoken">
					<img src="<?php bloginfo('template_url'); ?>/images/sekou/btn_shutoken.png" alt="首都圏" width="122" height="38" class="hover" >
				</a>
			</li>
	</ul>
	<strong><div class="tenjijyo_box">
	<div class="event_index" > <a href="http://www.e-himawari.co.jp/?sekou_cat=ishikawa"><img src="<?php bloginfo('template_url'); ?>/images/sekou/tenji/event01.png" width="227" height="135" alt="石川県の今週のイベント・展示会情報" class="hover"></a>
		<span class="maru"><a href="/?page_id=670" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/sekou/tenji/ishikawa_maru.png" width="212" height="29" alt="石川まるごと実験＆体験件" class="hover"></a></span>
	
	<div id="event_ticker01"></div>
	<p><a href="http://ameblo.jp/himawarihomeishikawa/" target="_blank">>>一覧を見る</a></p>
	
	</div>
	
	<div class="event_index" > <a href="http://www.e-himawari.co.jp/?sekou_cat=toyama"><img src="<?php bloginfo('template_url'); ?>/images/sekou/tenji/event02.png" width="227" height="135" alt="富山県の今週のイベント・展示会情報" class="hover"></a>
				<span class="maru"><a href="/?page_id=22998" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/sekou/tenji/tiyama_maru.png" width="212" height="29" alt="富山まるごと実験＆体験件" class="hover"></a></span>
	<div id="event_ticker02"></div>
	<p><a href="http://ameblo.jp/himawarihometoyama/" target="_blank">>>一覧を見る</a></p>
	
	</div>
	
	<div class="event_index" > <a href="http://www.e-himawari.co.jp/?sekou_cat=syutoken"><img src="<?php bloginfo('template_url'); ?>/images/sekou/tenji/event03.png" width="227" height="135"  alt="富山県の今週のイベント・展示会情報" class="hover"></a>
	<div id="event_ticker03"></div>
	<p><a href="http://ameblo.jp/himawarihometokyo/" target="_blank">>>一覧を見る</a></p>
	
	</div>
	<div class="clear"></div>
	</div>
	</strong>
	

<?php endif; ?>

	<h2 class="tenjijyo_list_title">
	<?php if(is_tax('sekou_cat', 'ishikawa')): ?>
	<span><img src="<?php bloginfo('template_url'); ?>/images/sekou/tenji/head_titie_ishikawa.png" width="92" height="22"  alt="石川県の"></span>
	<?php elseif(is_tax('sekou_cat', 'toyama')): ?>
	<span><img src="<?php bloginfo('template_url'); ?>/images/sekou/tenji/head_titie_toyama.png" width="92" height="22"  alt="富山県の"></span>
	<?php elseif(is_tax('sekou_cat', 'syutoken')): ?>
	<span><img src="<?php bloginfo('template_url'); ?>/images/sekou/tenji/head_titie_shutoken.png" width="92" height="22"  alt="首都圏の"></span>
	<?php endif; ?>
	<img src="<?php bloginfo('template_url'); ?>/images/sekou/tenji/ttl_tenji.png" width="202" height="36"  alt="展示場情報">
	</h2>
	<ul class="tenjijyo_list">
	<?php if (have_posts()) : query_posts($query_string.'&posts_per_page=6'); ?>
			<?php while (have_posts()) : the_post(); 
			/* ループ開始 */ ?>
	<li>
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<dl>
					<dt>
							<a href="<?php the_permalink(); ?>"><span>
<?php
											$imagefield = get_imagefield('ichiran-photo01');
											$attachment = get_attachment_object($imagefield['id']);
											$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
											echo '<image src="' . $imgattr[0] . '" width="' . $imgattr[1] . '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" class="hover" />';
									?>
							</span></a>
							<a href="<?php the_permalink(); ?>"><span>
<?php
											$imagefield = get_imagefield('ichiran-photo02');
											$attachment = get_attachment_object($imagefield['id']);
											$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
											echo '<image src="' . $imgattr[0] . '" width="' . $imgattr[1] . '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" class="hover" />';
									?>
							</span></a>
					</dt>
					<dd>
							<p>
							<?php echo mb_substr(get_the_excerpt(), 0, 100). '…'; ?>
							</p>
							<p class="btn_area">
									<a href="<?php the_permalink(); ?>">
											<img src="<?php bloginfo('template_url'); ?>/images/custom/btn_temji.png" alt="詳細を見る" width="144" height="38" class="hover" >
									</a>
							</p>
					</dd>
			</dl>
	</li>	
<?php endwhile; ?>
<?php else : ?>
<?php endif; ?>
	</ul>
	
<?php wp_pagenavi(); ?>
<br>

	<div class="tenjijyo_marugoto">
	<div class="left">
		<h2><img src="<?php bloginfo('template_url'); ?>/images/sekou/tenji/ttl_taikenkan.png" width="482" height="142"  alt="まるごと実験&体験館"></h2>
		<div>
			<p><a href="/?page_id=670"><img src="<?php bloginfo('template_url'); ?>/images/sekou/tenji/btn_marugoto_kanazawa.png" width="272" height="252"  alt="金沢本社はコチラ" class="hover"></a></p>
			<p><a href="/?page_id=22998"><img src="<?php bloginfo('template_url'); ?>/images/sekou/tenji/btn_marugoto_toyama.png" width="228" height="258"  alt="富山本社はコチラ" class="hover"></a></p>
		</div>
	</div>
	<div class="right">
		<p><img src="<?php bloginfo('template_url'); ?>/images/sekou/tenji/img_marugoto.png" width="230" height="388"  alt=""></p>
	</div>
		<br class="clear">
	</div>
	

	</div>
  
<!-- 誘導バナー -->
<ul id="bnr_large"></ul>
<!-- end -->

<!-- 誘導バナー -->
<ul id="bnr_twin"></ul>
<!-- end -->

