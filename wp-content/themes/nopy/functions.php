<?php
/**
 * TwentyTen functions and definitions
 *
 * Sets up the theme and provides some helper functions. Some helper functions
 * are used in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *
 * The first function, twentyten_setup(), sets up the theme by registering support
 * for various features in WordPress, such as post thumbnails, navigation menus, and the like.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are instead attached
 * to a filter or action hook. The hook can be removed by using remove_action() or
 * remove_filter() and you can attach your own function to the hook.
 *
 * We can remove the parent theme's hook only after it is attached, which means we need to
 * wait until setting up the child theme:
 *
 * <code>
 * add_action( 'after_setup_theme', 'my_child_theme_setup' );
 * function my_child_theme_setup() {
 *     // We are providing our own filter for excerpt_length (or using the unfiltered value)
 *     remove_filter( 'excerpt_length', 'twentyten_excerpt_length' );
 *     ...
 * }
 * </code>
 *
 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * Used to set the width of images and content. Should be equal to the width the theme
 * is designed for, generally via the style.css stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 640;

/** Tell WordPress to run twentyten_setup() when the 'after_setup_theme' hook is run. */
add_action( 'after_setup_theme', 'twentyten_setup' );

if ( ! function_exists( 'twentyten_setup' ) ):
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 *
 * To override twentyten_setup() in a child theme, add your own twentyten_setup to your child theme's
 * functions.php file.
 *
 * @uses add_theme_support() To add support for post thumbnails and automatic feed links.
 * @uses register_nav_menus() To add support for navigation menus.
 * @uses add_custom_background() To add support for a custom background.
 * @uses add_editor_style() To style the visual editor.
 * @uses load_theme_textdomain() For translation/localization support.
 * @uses add_custom_image_header() To add support for a custom header.
 * @uses register_default_headers() To register the default custom header images provided with the theme.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 * @since Twenty Ten 1.0
 */
function twentyten_setup() {

	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();

	// This theme uses post thumbnails
	add_theme_support( 'post-thumbnails' );

	// Add default posts and comments RSS feed links to head
	add_theme_support( 'automatic-feed-links' );

	// Make theme available for translation
	// Translations can be filed in the /languages/ directory
	load_theme_textdomain( 'twentyten', TEMPLATEPATH . '/languages' );

	$locale = get_locale();
	$locale_file = TEMPLATEPATH . "/languages/$locale.php";
	if ( is_readable( $locale_file ) )
		require_once( $locale_file );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Navigation', 'twentyten' ),
	) );

	// This theme allows users to set a custom background
	add_custom_background();

	// Your changeable header business starts here
	define( 'HEADER_TEXTCOLOR', '' );
	// No CSS, just IMG call. The %s is a placeholder for the theme template directory URI.
	define( 'HEADER_IMAGE', '%s/images/headers/path.jpg' );

	// The height and width of your custom header. You can hook into the theme's own filters to change these values.
	// Add a filter to twentyten_header_image_width and twentyten_header_image_height to change these values.
	define( 'HEADER_IMAGE_WIDTH', apply_filters( 'twentyten_header_image_width', 940 ) );
	define( 'HEADER_IMAGE_HEIGHT', apply_filters( 'twentyten_header_image_height', 198 ) );

	// We'll be using post thumbnails for custom header images on posts and pages.
	// We want them to be 940 pixels wide by 198 pixels tall.
	// Larger images will be auto-cropped to fit, smaller ones will be ignored. See header.php.
	set_post_thumbnail_size( HEADER_IMAGE_WIDTH, HEADER_IMAGE_HEIGHT, true );

	// Don't support text inside the header image.
	define( 'NO_HEADER_TEXT', true );

	// Add a way for the custom header to be styled in the admin panel that controls
	// custom headers. See twentyten_admin_header_style(), below.
	add_custom_image_header( '', 'twentyten_admin_header_style' );

	// ... and thus ends the changeable header business.

	// Default custom headers packaged with the theme. %s is a placeholder for the theme template directory URI.
	register_default_headers( array(
		'berries' => array(
			'url' => '%s/images/headers/berries.jpg',
			'thumbnail_url' => '%s/images/headers/berries-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Berries', 'twentyten' )
		),
		'cherryblossom' => array(
			'url' => '%s/images/headers/cherryblossoms.jpg',
			'thumbnail_url' => '%s/images/headers/cherryblossoms-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Cherry Blossoms', 'twentyten' )
		),
		'concave' => array(
			'url' => '%s/images/headers/concave.jpg',
			'thumbnail_url' => '%s/images/headers/concave-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Concave', 'twentyten' )
		),
		'fern' => array(
			'url' => '%s/images/headers/fern.jpg',
			'thumbnail_url' => '%s/images/headers/fern-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Fern', 'twentyten' )
		),
		'forestfloor' => array(
			'url' => '%s/images/headers/forestfloor.jpg',
			'thumbnail_url' => '%s/images/headers/forestfloor-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Forest Floor', 'twentyten' )
		),
		'inkwell' => array(
			'url' => '%s/images/headers/inkwell.jpg',
			'thumbnail_url' => '%s/images/headers/inkwell-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Inkwell', 'twentyten' )
		),
		'path' => array(
			'url' => '%s/images/headers/path.jpg',
			'thumbnail_url' => '%s/images/headers/path-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Path', 'twentyten' )
		),
		'sunset' => array(
			'url' => '%s/images/headers/sunset.jpg',
			'thumbnail_url' => '%s/images/headers/sunset-thumbnail.jpg',
			/* translators: header image description */
			'description' => __( 'Sunset', 'twentyten' )
		)
	) );
}
endif;

if ( ! function_exists( 'twentyten_admin_header_style' ) ) :
/**
 * Styles the header image displayed on the Appearance > Header admin panel.
 *
 * Referenced via add_custom_image_header() in twentyten_setup().
 *
 * @since Twenty Ten 1.0
 */
function twentyten_admin_header_style() {
?>
<style type="text/css">
/* Shows the same border as on front end */
#headimg {
	border-bottom: 1px solid #000;
	border-top: 4px solid #000;
}
/* If NO_HEADER_TEXT is false, you would style the text with these selectors:
	#headimg #name { }
	#headimg #desc { }
*/
</style>
<?php
}
endif;

/**
 * Makes some changes to the <title> tag, by filtering the output of wp_title().
 *
 * If we have a site description and we're viewing the home page or a blog posts
 * page (when using a static front page), then we will add the site description.
 *
 * If we're viewing a search result, then we're going to recreate the title entirely.
 * We're going to add page numbers to all titles as well, to the middle of a search
 * result title and the end of all other titles.
 *
 * The site title also gets added to all titles.
 *
 * @since Twenty Ten 1.0
 *
 * @param string $title Title generated by wp_title()
 * @param string $separator The separator passed to wp_title(). Twenty Ten uses a
 * 	vertical bar, "|", as a separator in header.php.
 * @return string The new title, ready for the <title> tag.
 */
function twentyten_filter_wp_title( $title, $separator ) {
	// Don't affect wp_title() calls in feeds.
	if ( is_feed() )
		return $title;

	// The $paged global variable contains the page number of a listing of posts.
	// The $page global variable contains the page number of a single post that is paged.
	// We'll display whichever one applies, if we're not looking at the first page.
	global $paged, $page;

	if ( is_search() ) {
		// If we're a search, let's start over:
		$title = sprintf( __( 'Search results for %s', 'twentyten' ), '"' . get_search_query() . '"' );
		// Add a page number if we're on page 2 or more:
		if ( $paged >= 2 )
			$title .= " $separator " . sprintf( __( 'Page %s', 'twentyten' ), $paged );
		// Add the site name to the end:
		$title .= " $separator " . get_bloginfo( 'name', 'display' );
		// We're done. Let's send the new title back to wp_title():
		return $title;
	}

	// Otherwise, let's start by adding the site name to the end:
	$title .= get_bloginfo( 'name', 'display' );

	// If we have a site description and we're on the home/front page, add the description:
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title .= " $separator " . $site_description;

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		$title .= " $separator " . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	// Return the new title to wp_title():
	return $title;
}
add_filter( 'wp_title', 'twentyten_filter_wp_title', 10, 2 );

/**
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 *
 * To override this in a child theme, remove the filter and optionally add
 * your own function tied to the wp_page_menu_args filter hook.
 *
 * @since Twenty Ten 1.0
 */
function twentyten_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'twentyten_page_menu_args' );

/**
 * Sets the post excerpt length to 40 characters.
 *
 * To override this length in a child theme, remove the filter and add your own
 * function tied to the excerpt_length filter hook.
 *
 * @since Twenty Ten 1.0
 * @return int
 */
function twentyten_excerpt_length( $length ) {
	return 40;
}
add_filter( 'excerpt_length', 'twentyten_excerpt_length' );

/**
 * Returns a "Continue Reading" link for excerpts
 *
 * @since Twenty Ten 1.0
 * @return string "Continue Reading" link
 */
function twentyten_continue_reading_link() {
	return ' <a href="'. get_permalink() . '">' . __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentyten' ) . '</a>';
}

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and twentyten_continue_reading_link().
 *
 * To override this in a child theme, remove the filter and add your own
 * function tied to the excerpt_more filter hook.
 *
 * @since Twenty Ten 1.0
 * @return string An ellipsis
 */
function twentyten_auto_excerpt_more( $more ) {
	return ' &hellip;' . twentyten_continue_reading_link();
}
add_filter( 'excerpt_more', 'twentyten_auto_excerpt_more' );

/**
 * Adds a pretty "Continue Reading" link to custom post excerpts.
 *
 * To override this link in a child theme, remove the filter and add your own
 * function tied to the get_the_excerpt filter hook.
 *
 * @since Twenty Ten 1.0
 * @return string Excerpt with a pretty "Continue Reading" link
 */
function twentyten_custom_excerpt_more( $output ) {
	if ( has_excerpt() && ! is_attachment() ) {
		$output .= twentyten_continue_reading_link();
	}
	return $output;
}
add_filter( 'get_the_excerpt', 'twentyten_custom_excerpt_more' );

/**
 * Remove inline styles printed when the gallery shortcode is used.
 *
 * Galleries are styled by the theme in Twenty Ten's style.css.
 *
 * @since Twenty Ten 1.0
 * @return string The gallery style filter, with the styles themselves removed.
 */
function twentyten_remove_gallery_css( $css ) {
	return preg_replace( "#<style type='text/css'>(.*?)</style>#s", '', $css );
}
add_filter( 'gallery_style', 'twentyten_remove_gallery_css' );

if ( ! function_exists( 'twentyten_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * To override this walker in a child theme without modifying the comments template
 * simply create your own twentyten_comment(), and that function will be used instead.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since Twenty Ten 1.0
 */
function twentyten_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case '' :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<div id="comment-<?php comment_ID(); ?>">
		<div class="comment-author vcard">
			<?php echo get_avatar( $comment, 40 ); ?>
			<?php printf( __( '%s <span class="says">says:</span>', 'twentyten' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
		</div><!-- .comment-author .vcard -->
		<?php if ( $comment->comment_approved == '0' ) : ?>
			<em><?php _e( 'Your comment is awaiting moderation.', 'twentyten' ); ?></em>
			<br />
		<?php endif; ?>

		<div class="comment-meta commentmetadata"><a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
			<?php
				/* translators: 1: date, 2: time */
				printf( __( '%1$s at %2$s', 'twentyten' ), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)', 'twentyten' ), ' ' );
			?>
		</div><!-- .comment-meta .commentmetadata -->

		<div class="comment-body"><?php comment_text(); ?></div>

		<div class="reply">
			<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
		</div><!-- .reply -->
	</div><!-- #comment-##  -->

	<?php
			break;
		case 'pingback'  :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'twentyten' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __('(Edit)', 'twentyten'), ' ' ); ?></p>
	<?php
			break;
	endswitch;
}
endif;

/**
 * Register widgetized areas, including two sidebars and four widget-ready columns in the footer.
 *
 * To override twentyten_widgets_init() in a child theme, remove the action hook and add your own
 * function tied to the init hook.
 *
 * @since Twenty Ten 1.0
 * @uses register_sidebar
 */
function twentyten_widgets_init() {
	// Area 1, located at the top of the sidebar.
	register_sidebar( array(
		'name' => __( 'Primary Widget Area', 'twentyten' ),
		'id' => 'primary-widget-area',
		'description' => __( 'The primary widget area', 'twentyten' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 2, located below the Primary Widget Area in the sidebar. Empty by default.
	register_sidebar( array(
		'name' => __( 'Secondary Widget Area', 'twentyten' ),
		'id' => 'secondary-widget-area',
		'description' => __( 'The secondary widget area', 'twentyten' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 3, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'First Footer Widget Area', 'twentyten' ),
		'id' => 'first-footer-widget-area',
		'description' => __( 'The first footer widget area', 'twentyten' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 4, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Second Footer Widget Area', 'twentyten' ),
		'id' => 'second-footer-widget-area',
		'description' => __( 'The second footer widget area', 'twentyten' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 5, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Third Footer Widget Area', 'twentyten' ),
		'id' => 'third-footer-widget-area',
		'description' => __( 'The third footer widget area', 'twentyten' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 6, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Fourth Footer Widget Area', 'twentyten' ),
		'id' => 'fourth-footer-widget-area',
		'description' => __( 'The fourth footer widget area', 'twentyten' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}
/** Register sidebars by running twentyten_widgets_init() on the widgets_init hook. */
add_action( 'widgets_init', 'twentyten_widgets_init' );

/**
 * Removes the default styles that are packaged with the Recent Comments widget.
 *
 * To override this in a child theme, remove the filter and optionally add your own
 * function tied to the widgets_init action hook.
 *
 * @since Twenty Ten 1.0
 */
function twentyten_remove_recent_comments_style() {
	global $wp_widget_factory;
	remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );
}
add_action( 'widgets_init', 'twentyten_remove_recent_comments_style' );

if ( ! function_exists( 'twentyten_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post�date/time and author.
 *
 * @since Twenty Ten 1.0
 */
function twentyten_posted_on() {
	printf( __( '<span class="%1$s">Posted on</span> %2$s <span class="meta-sep">by</span> %3$s', 'twentyten' ),
		'meta-prep meta-prep-author',
		sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><span class="entry-date">%3$s</span></a>',
			get_permalink(),
			esc_attr( get_the_time() ),
			get_the_date()
		),
		sprintf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s">%3$s</a></span>',
			get_author_posts_url( get_the_author_meta( 'ID' ) ),
			sprintf( esc_attr__( 'View all posts by %s', 'twentyten' ), get_the_author() ),
			get_the_author()
		)
	);
}
endif;

if ( ! function_exists( 'twentyten_posted_in' ) ) :
/**
 * Prints HTML with meta information for the current post (category, tags and permalink).
 *
 * @since Twenty Ten 1.0
 */
function twentyten_posted_in() {
	// Retrieves tag list of current post, separated by commas.
	$tag_list = get_the_tag_list( '', ', ' );
	if ( $tag_list ) {
		$posted_in = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'twentyten' );
	} elseif ( is_object_in_taxonomy( get_post_type(), 'category' ) ) {
		$posted_in = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'twentyten' );
	} else {
		$posted_in = __( 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'twentyten' );
	}
	// Prints the string, replacing the placeholders.
	printf(
		$posted_in,
		get_the_category_list( ', ' ),
		$tag_list,
		get_permalink(),
		the_title_attribute( 'echo=0' )
	);
}
endif;






if ( ! function_exists( 'wp_get_attachment_image_src_complete' ) ) :
/**
 * 指定IDの添付画像を返す
 */
function wp_get_attachment_image_src_complete($attachment_id, $size='thumbnail', $icon = false) {
	
	$image = false;

	// get a thumbnail or intermediate image if there is one
	if ( $image = image_downsize($attachment_id, $size) ){
		//do nothing
	}else{
		if ( $icon && $src = wp_mime_type_icon($attachment_id) ) {
			$icon_dir = apply_filters( 'icon_dir', ABSPATH . WPINC . '/images/crystal' );
			$src_file = $icon_dir . '/' . basename($src);
			@list($width, $height) = getimagesize($src_file);
		}
		if ( $src && $width && $height ){
			$image = array( $src, $width, $height );
		}else{
			return false;
		}
	}

	if(empty($image[3]) || empty($image[4])){
		$post = get_post($attachment_id);
		$image[3] = $post->post_excerpt;
		$image[4] = $post->post_content;
	}
	
	return $image;

}

endif;

function new_excerpt_length($length) {	
     return 0;	
}	
add_filter('excerpt_length', 'new_excerpt_length');

function new_excerpt_more($more) {
	return '';
}
add_filter('excerpt_more', 'new_excerpt_more');




// カスタム投稿タイプを作成
// スタッフ紹介

function staff_custom_post_type() {
$labels = array(
'name' => 'スタッフ紹介',
'singular_name' => 'スタッフ紹介',
'add_new_item' => 'スタッフ紹介を追加',
'add_new' => '新規追加',
'new_item' => '新規スタッフ紹介',
'view_item' => 'スタッフ紹介を表示',
'not_found' => 'スタッフ紹介は見つかりませんでした',
'not_found_in_trash' => 'ゴミ箱にスタッフ紹介はありません。',
'search_items' => 'FAQを検索',
);
$args = array(
'labels' => $labels,
'public' => true,
'show_ui' => true, 
'query_var' => true,
'hierarchical' => false,
'has_archive' => true,
'menu_position' => 5,
'supports' => array('title','thumbnail','editor')
); 
register_post_type('staff', $args);
}
add_action('init', 'staff_custom_post_type');


$args = array(
'label' => 'カテゴリー',
'public' => true,
'show_ui' => true,
'hierarchical' => true,
);
register_taxonomy('staff_category', 'staff', $args);

$args = array(
'label' => 'タグ',
'public' => true,
'show_ui' => true,
'hierarchical' => false,
);
register_taxonomy('staff_tag', 'staff', $args);


// 管理画面記事一覧にカスタムタクソノミーの表示追加
function manage_posts_columns($columns) {
    $columns['fcategory1'] = "カテゴリー";
    $columns['fcategory2'] = "タグ";
    return $columns;
}
 
function add_column($column_name, $post_id){
    if( 'fcategory1' == $column_name ) {
        $fcategory = get_the_term_list($post_id, 'staff_category'); // カテゴリ
    }
    if( 'fcategory2' == $column_name ) {
        $fcategory = get_the_term_list($post_id, 'staff_tag'); // タグ
    }
    if ( isset($fcategory) && $fcategory ) {
        echo $fcategory;
    } else {
        echo __('None');
    }
}
add_filter('manage_edit-staff_columns', 'manage_posts_columns');
add_action('manage_posts_custom_column',  'add_column', 10, 2);


/* ★施工事例系ここらから★ */
register_post_type(
'sekou',
  array(
  'label' => '施工事例と展示場',
  'hierarchical' => false,
  'public' => true,
  'query_var' => false,
  'menu_position' => 7,
  'has_archive' => true,
  'supports' => array('title','editor','author')
  )
);
 
  /* カスタムタクソノミーを定義 */
  register_taxonomy(
    'sekou_cat',
    'sekou',
    array(
    'label' => 'カテゴリー',
    'hierarchical' => true,
    'rewrite' => array('slug' => 'sekou')
    )
  );
  /* カスタムタクソノミーを定義ここまで */
 
  /* 管理画面一覧にカテゴリを表示 */
  function manage_sekou_columns($columns) {
    $columns['sekou_category'] = "カテゴリー";
    return $columns;
  }
  function add_sekou_column($column_name, $post_id){
    if( $column_name == 'sekou_category' ) {
    //カテゴリー名取得
    if( 'sekou_category' == $column_name ) {
        $sekou_category = get_the_term_list($post_id, 'sekou_cat');
    }
    //該当カテゴリーがない場合「なし」を表示
    if ( isset($sekou_category) && $sekou_category ) {
        echo $sekou_category;
    } else {
        echo __('None');
    }
    }
  }
  add_filter('manage_edit-sekou_columns', 'manage_sekou_columns');
  add_action('manage_posts_custom_column',  'add_sekou_column', 10, 2);
  /* 管理画面一覧にカテゴリを表示ここまで */
 
/* ★施工事例と展示場ここまで★ */


/* ★お客様の声ここらから★ */
register_post_type(
'voice',
  array(
  'label' => 'お客様の声',
  'hierarchical' => false,
  'public' => true,
  'query_var' => false,
  'menu_position' => 7,
  'has_archive' => true,
  'supports' => array('title','editor','author')
  )
);
 
  /* カスタムタクソノミーを定義 */
  register_taxonomy(
    'voice_cat',
    'voice',
    array(
    'label' => 'カテゴリー',
    'hierarchical' => true,
    'rewrite' => array('slug' => 'voice')
    )
  );
  /* カスタムタクソノミーを定義ここまで */
 
  /* 管理画面一覧にカテゴリを表示 */
  function manage_voice_columns($columns) {
    $columns['voice_category'] = "カテゴリー";
    return $columns;
  }
  function add_voice_column($column_name, $post_id){
    if( $column_name == 'voice_category' ) {
    //カテゴリー名取得
    if( 'voice_category' == $column_name ) {
        $voice_category = get_the_term_list($post_id, 'voice_cat');
    }
    //該当カテゴリーがない場合「なし」を表示
    if ( isset($voice_category) && $voice_category ) {
        echo $voice_category;
    } else {
        echo __('None');
    }
    }
  }
  add_filter('manage_edit-voice_columns', 'manage_voice_columns');
  add_action('manage_posts_custom_column',  'add_voice_column', 10, 2);
  /* 管理画面一覧にカテゴリを表示ここまで */
 
/* ★お客様の声ここまで★ */

/* ★リフォームここらから★ */
register_post_type(
'reform',
  array(
  'label' => 'リフォーム',
  'hierarchical' => false,
  'public' => true,
  'query_var' => false,
  'menu_position' => 7,
  'has_archive' => true,
  'supports' => array('title','editor','author')
  )
);
 
  /* カスタムタクソノミーを定義 */
  register_taxonomy(
    'reform_cat',
    'reform',
    array(
    'label' => 'カテゴリー',
    'hierarchical' => true,
    'rewrite' => array('slug' => 'voice')
    )
  );
  /* カスタムタクソノミーを定義ここまで */
 
  /* 管理画面一覧にカテゴリを表示 */
  function manage_reform_columns($columns) {
    $columns['reform_category'] = "カテゴリー";
    return $columns;
  }
  function add_reform_column($column_name, $post_id){
    if( $column_name == 'reform_category' ) {
    //カテゴリー名取得
    if( 'reform_category' == $column_name ) {
        $reform_category = get_the_term_list($post_id, 'reform_cat');
    }
    //該当カテゴリーがない場合「なし」を表示
    if ( isset($reform_category) && $reform_category ) {
        echo $reform_category;
    } else {
        echo __('None');
    }
    }
  }
  add_filter('manage_edit-reform_columns', 'manage_reform_columns');
  add_action('manage_posts_custom_column',  'add_reform_column', 10, 2);
  /* 管理画面一覧にカテゴリを表示ここまで */
 
/* ★リフォームここまで★ */



/* ★更新履歴ここらから★ */
register_post_type(
'news',
  array(
  'label' => '更新履歴',
  'hierarchical' => false,
  'public' => true,
  'query_var' => false,
  'menu_position' => 7,
  'has_archive' => true,
  'supports' => array('title','editor','author')
  )
);
 
  /* カスタムタクソノミーを定義 */
  register_taxonomy(
    'news_cat',
    'news',
    array(
    'label' => 'カテゴリー',
    'hierarchical' => true,
    'rewrite' => array('slug' => 'sekou')
    )
  );
  /* カスタムタクソノミーを定義ここまで */
 
  /* 管理画面一覧にカテゴリを表示 */
  function manage_news_columns($columns) {
    $columns['news_category'] = "カテゴリー";
    return $columns;
  }
  function add_news_column($column_name, $post_id){
    if( $column_name == 'news_category' ) {
    //カテゴリー名取得
    if( 'news_category' == $column_name ) {
        $news_category = get_the_term_list($post_id, 'news_cat');
    }
    //該当カテゴリーがない場合「なし」を表示
    if ( isset($news_category) && $news_category ) {
        echo $news_category;
    } else {
        echo __('None');
    }
    }
  }
  add_filter('manage_edit-news_columns', 'manage_news_columns');
  add_action('manage_posts_custom_column',  'add_news_column', 10, 2);
  /* 管理画面一覧にカテゴリを表示ここまで */
 
/* ★施工事例と展示場ここまで★ */



 //GoogleMaps
wp_deregister_script('googlemapapi');
wp_enqueue_script('googlemapapi', 'http://maps.googleapis.com/maps/api/js?&sensor=false');

/*
javascript登録
*/
function register_script(){
	wp_register_script('jquery183', get_bloginfo('template_directory').'/js/jquery.js');
	wp_register_script('common02', get_bloginfo('template_directory').'/js/common.js');
	wp_register_script('height', get_bloginfo('template_directory').'/js/height.js');
	wp_register_script('ajaxzip3', 'http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js');
	wp_register_script('jsapi', 'http://www.google.com/jsapi');
	wp_register_script('Feedjson', get_bloginfo('template_directory').'/js/Feedjson.js');
	wp_register_script('blog01', get_bloginfo('template_directory').'/js/blog01.js');
	wp_register_script('blog02', get_bloginfo('template_directory').'/js/blog02.js');
	wp_register_script('blog03', get_bloginfo('template_directory').'/js/blog03.js');
	wp_register_script('blog04', get_bloginfo('template_directory').'/js/blog04.js');
	wp_register_script('blog04-02', get_bloginfo('template_directory').'/js/blog04-02.js');    
	wp_register_script('blog01-2', get_bloginfo('template_directory').'/js/info/blog01.js');
	wp_register_script('blog02-2', get_bloginfo('template_directory').'/js/info/blog02.js');
	wp_register_script('blog03-2', get_bloginfo('template_directory').'/js/info/blog03.js');
	wp_register_script('blog04-2', get_bloginfo('template_directory').'/js/info/blog04.js');
	wp_register_script('dotdotdot', get_bloginfo('template_directory').'/js/jquery.dotdotdot.js');
	wp_register_script('MyThumbnail', get_bloginfo('template_directory').'/js/jquery.MyThumbnail.js');
	wp_register_script('vticker', get_bloginfo('template_directory').'/js/jquery.vticker.js');



}


function add_script() {
	register_script();
	wp_enqueue_script('jquery183');
	wp_enqueue_script('common02');
	wp_enqueue_script('MyThumbnail');
        	wp_enqueue_script('height');
		wp_enqueue_script('jsapi');

	if (is_home()) {
		wp_enqueue_script('blog01');
		wp_enqueue_script('blog02');
		wp_enqueue_script('blog03');
		wp_enqueue_script('blog04');
		wp_enqueue_script('blog04-02');        
		wp_enqueue_script('vticker');
		wp_enqueue_script('dotdotdot');
	}
	elseif (is_page(array(1344))) {
		wp_enqueue_script('blog01');
		wp_enqueue_script('blog02');
		wp_enqueue_script('blog03');
		wp_enqueue_script('blog04');
		wp_enqueue_script('blog04-02');   
        wp_enqueue_script('vticker');
		wp_enqueue_script('dotdotdot');
	}
	elseif (is_page(array(25194))) {
		wp_enqueue_script('Feedjson');
	}
	elseif (has_term('cat04','sekou_cat')) {
		wp_enqueue_script('blog01');
		wp_enqueue_script('blog02');
		wp_enqueue_script('blog03');
		wp_enqueue_script('blog04');
		wp_enqueue_script('blog04-02');   
        wp_enqueue_script('vticker');
	}
	elseif (is_page(array(33463))) {
		wp_enqueue_script('blog01-2');
		wp_enqueue_script('blog02-2');
		wp_enqueue_script('blog03-2');
		wp_enqueue_script('blog04-2');
	}
}


if (!is_admin()) {
	wp_deregister_script('jquery');
	add_action('wp_print_scripts', 'add_script');
}


/* ギャラリーをulで出力 */
add_shortcode( 'gallery', 'file_my_gallery_shortcode' );
 
function file_gallery_shortcode( $atts )
{
    $atts['link'] = 'file';
	$atts['size'] = 'full';
    return my_gallery_shortcode( $atts );
}

/* デフォルトの gallery ショートコードを削除 */
remove_shortcode('gallery', 'gallery_shortcode');

/* 新しい gallery ショートコードを追加 */
add_shortcode('gallery', 'my_gallery_shortcode');



function my_gallery_shortcode($attr) {
	global $post;

	static $instance = 0;
	$instance++;

	// Allow plugins/themes to override the default gallery template.
	$output = apply_filters('post_gallery', '', $attr);
	if ( $output != '' )
		return $output;

	// We're trusting author input, so let's at least make sure it looks like a valid orderby statement
	if ( isset( $attr['orderby'] ) ) {
		$attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
		if ( !$attr['orderby'] )
			unset( $attr['orderby'] );
	}

	extract(shortcode_atts(array(
		'order'      => 'ASC',
		'orderby'    => 'menu_order ID',
		'id'         => $post->ID,
		'itemtag'    => 'li',
		'icontag'    => 'p',
		'captiontag' => 'p',
		'columns'    => 1,
		'size'       => 'thumbnail',
		'include'    => '',
		'exclude'    => ''
	), $attr));

	$id = intval($id);
	if ( 'RAND' == $order )
		$orderby = 'none';

	if ( !empty($include) ) {
		$include = preg_replace( '/[^0-9,]+/', '', $include );
		$_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

		$attachments = array();
		foreach ( $_attachments as $key => $val ) {
			$attachments[$val->ID] = $_attachments[$key];
		}
	} elseif ( !empty($exclude) ) {
		$exclude = preg_replace( '/[^0-9,]+/', '', $exclude );
		$attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	} else {
		$attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	}

	if ( empty($attachments) )
		return '';

	if ( is_feed() ) {
		$output = "\n";
		foreach ( $attachments as $att_id => $attachment )
			$output .= wp_get_attachment_link($att_id, $size, true) . "\n";
		return $output;
	}

	$itemtag = tag_escape($itemtag);
	$captiontag = tag_escape($captiontag);
	$columns = intval($columns);
	$itemwidth = $columns > 0 ? floor(100/$columns) : 100;
	$float = is_rtl() ? 'right' : 'left';

	$selector = "gallery-{$instance}";

	$gallery_style = $gallery_div = '';
	if ( apply_filters( 'use_default_gallery_style', true ) )
		$gallery_style = "
		<!-- see gallery_shortcode() in wp-includes/media.php -->";
	$size_class = sanitize_html_class( $size );
	$gallery_div = "<div id='$selector' class='galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class}'><ul class='clearfix'>";
	
	$output = apply_filters( 'gallery_style', $gallery_style . "\n\t\t" . $gallery_div );

	$i = 0;
	foreach ( $attachments as $id => $attachment ) {
		$url_full = wp_get_attachment_image_src($id, $size='full');
		$url_medium = wp_get_attachment_image_src($id, $size='thumbnail');
		$pot_date = $attachment->post_content;
		$pot_caption = $attachment->post_excerpt;
	
		$output .= "<{$itemtag}>";
		$output .= "<dl><dt style=\"margin-bottom: 5px;\"><a href=\"";
		$output .= $url_full[0];
		$output .= "\" class=\"lightbox[]\">";
		$output .= "<img src=\"";
		$output .= $url_medium[0];
		$output .= "\" class=\"hover\" width=\"124\" height=\"175\"  />";
		$output .= "</a></dt><dd>";
		$output .= $pot_date;
		$output .= "<br><a href=\"";
		$output .= $url_full[0];
		$output .= "\" class=\"lightbox[]\">";
		$output .= $pot_caption;
		$output .= "</a></dd></dl>";
		$output .= "</{$itemtag}>
					";
		}

	$output .= "
		</ul></div>\n";

	return $output;
}
/* ギャラリーをulで出力 end*/

/* 投稿内にテンプレートを読み込む */
function PHP_Include($params = array()) {
	extract(shortcode_atts(array(
	    'file' => 'default'
	), $params));
	ob_start();
	include(get_theme_root() . '/' . get_template() . "/$file.php");
	return ob_get_clean();
}
// register shortcode
add_shortcode('tp', 'PHP_Include');
