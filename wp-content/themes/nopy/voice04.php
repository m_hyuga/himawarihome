<div class="voice_con">
<?php echo c2c_get_custom('textfield-voice01'); ?>
	<div class="no04-box01">
	<?php
		$imagefield = get_imagefield('voice-photo01');
		$attachment = get_attachment_object($imagefield['id']);
		$imgattr = wp_get_attachment_image_src($imagefield['id'], 'large'); // thumbnail, large, medium, full を指定
		if(empty($imgattr)){
			echo '';
		}else{
			echo '<a href="' . $imgattr[0] .'" rel="lightbox[]"><image src="' . $imgattr[0] . '" width="' . $imgattr[1] .  '" alt="' . $attachment['title'] . '" title="' . $attachment['content'] . '" /></a>';
		}
	?>
	</div>
</div>