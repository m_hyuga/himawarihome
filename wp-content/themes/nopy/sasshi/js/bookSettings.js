flippingBook.pages = [
	"pages/page-001.jpeg",
	"pages/page-002.jpeg",
	"pages/page-003.jpeg",
	"pages/page-004.jpeg",
	"pages/page-005.jpeg",
	"pages/page-006.jpeg",
	"pages/page-007.jpeg",
	"pages/page-008.jpeg",
	"pages/page-009.jpeg",
	"pages/page-010.jpeg",
	"pages/page-011.jpeg",
	"pages/page-012.jpeg",
	"pages/page-013.jpeg",
	"pages/page-014.jpeg",
	"pages/page-015.jpeg",
	"pages/page-016.jpeg",
	"pages/page-017.jpeg",
	"pages/page-018.jpeg",
	"pages/page-019.jpeg",
	"pages/page-020.jpeg",
	"pages/page-021.jpeg",
	"pages/page-022.jpeg",
	"pages/page-023.jpeg",
	"pages/page-024.jpeg",
	"pages/page-025.jpeg",
	"pages/page-026.jpeg",
	"pages/page-027.jpeg",
	"pages/page-028.jpeg",
	"pages/page-029.jpeg",
	"pages/page-030.jpeg",
	"pages/page-031.jpeg",
	"pages/page-032.jpeg",
	"pages/page-033.jpeg",
	"pages/page-034.jpeg",
	"pages/page-035.jpeg",
	"pages/page-036.jpeg",
	"pages/page-037.jpeg",
	"pages/page-038.jpeg",
	"pages/page-039.jpeg",
	"pages/page-040.jpeg"
];


flippingBook.contents = [
	[ "Cover", 1 ],
	[ "Modern", 2 ]
];

// define custom book settings here
flippingBook.settings.bookWidth = 826;
flippingBook.settings.bookHeight = 584;
flippingBook.settings.pageBackgroundColor = 0xfa8c0b;
flippingBook.settings.backgroundColor = 0xfa8c0b;
flippingBook.settings.zoomUIColor = 0xecdab4;
flippingBook.settings.useCustomCursors = false;
flippingBook.settings.dropShadowEnabled = false;
flippingBook.settings.zoomImageWidth = 992;
flippingBook.settings.zoomImageHeight = 1403;
flippingBook.settings.downloadURL = "http://www.demdm.net/sasshi/zukan.pdf";
flippingBook.settings.flipSound = "sounds/02.mp3";
flippingBook.settings.flipCornerStyle = "first page only";
flippingBook.settings.zoomHintEnabled = true;

// default settings can be found in the flippingbook.js file
flippingBook.create();
