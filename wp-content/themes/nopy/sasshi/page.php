<?php if ($post->ID == "1344") { ?>

 <?php include (TEMPLATEPATH . '/header-index.php'); ?>
 

		
		<div id="contents">
		<div class="index_txt_area">
        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
        	<?php the_content(); ?>
            <?php endwhile; ?>
		</div>
		
		<!--1段目-->

		<section class="contents01">
		<div class="zento_index">
		<ul>
		<li><a href="/?page_id=772"><img src="<?php bloginfo('template_url'); ?>/images/index/zento_tyoki.jpg" class="hover" width="267" height="43" alt="全棟長期優良住宅"></a></li>
		<li><a href="/?page_id=87"><img src="<?php bloginfo('template_url'); ?>/images/index/zento_muku.jpg" class="hover" width="267" height="43" alt="全棟ムク材"></a></li>
		<li><a href="/?page_id=17500"><img src="<?php bloginfo('template_url'); ?>/images/index/zento_shoene.jpg" class="hover" width="267" height="44" alt="全棟耐震・省エネ"></a></li>
		<li><a href="/?page_id=57"><img src="<?php bloginfo('template_url'); ?>/images/index/zento_seino.jpg" class="hover" width="267" height="44" alt="全棟性能表示"></a></li>
		</ul>
		</div>
		
		<div class="kodwari_index">
		<ul>
		<li><a href="/?page_id=971"><img src="<?php bloginfo('template_url'); ?>/images/index/kodawaro01.jpg" class="hover" width="544" height="180" alt="「全棟」にこだわる家づくり"></a></li>
		</ul>
		</div>
		
		<ul class="mittu_index">
		<li><img src="<?php bloginfo('template_url'); ?>/images/index/mittu.jpg" width="267" height="180" alt="3つ以上の"></li>
		</ul>
		</section>
		
		<!--1段目end-->
		
		<!--2段目-->
			<section class="contents01">
	
	<div class="left01_index">
	
	<ul class="sekou_index">
		<li><a href="/?sekou_cat=cat01"><img src="<?php bloginfo('template_url'); ?>/images/index/sekou01.jpg" class="hover" width="267" height="360" alt="提案がいっぱい 施工事例集"></a></li>
		</ul>
	<ul class="muku_index">
		<li><a href="/?page_id=34747"><img src="<?php bloginfo('template_url'); ?>/images/index/mukuzainohanashi01.jpg" width="267" height="85" alt="木の家とムク材の話" class="hover"></a></li>
		</ul>
		<ul class="sekisetu_index">
		<li><a href="/?page_id=33388"><img src="<?php bloginfo('template_url'); ?>/images/index/sekisetsu01.jpg" class="hover" width="267" height="85" alt="積雪に強い家づくり"></a></li>
		</ul>
	</div>
	
	
	<div class="right01_index">
	
	<div class="right01_con">
		<ul class="imadoki_index">
		<li><a href="/?page_id=30564"><img src="<?php bloginfo('template_url'); ?>/images/index/imadoki01.jpg" class="hover" width="128" height="180" alt="イマドキの2世帯住宅"></a></li>
		</ul>
		<ul class="zishinnni_index">
		<li><a href="/?page_id=17500"><img src="<?php bloginfo('template_url'); ?>/images/index/zishin.jpg" class="hover" width="129" height="180" alt="地震に耐える家"></a></li>
		</ul>
		
		<ul class="staff_index">
		<li><a href="/?page_id=1091"><img src="<?php bloginfo('template_url'); ?>/images/index/staff01.jpg" class="hover" width="544" height="180" alt="明るく元気で前向き スタッフ紹介"></a></li>
		</ul>
	</div>
	
	<div>
	<div class="event_index" > <a href="http://ameblo.jp/himawarihomeishikawa/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/index/event01.jpg" width="267" height="138" alt="石川県の今週のイベント・展示会情報" class="hover"></a>
    <span class="maru"><a href="/?page_id=670" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/index/ishikawa_maru.png" width="250" height="29" alt="石川まるごと実験＆体験件" class="hover"></a></span>
	
    <div id="event_ticker01"></div>
	<p><a href="http://ameblo.jp/himawarihomeishikawa/" target="_blank">>>一覧を見る</a></p>
	
	</div>
	
	<div class="event_index" > <a href="http://ameblo.jp/himawarihometoyama/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/index/event02.jpg" width="267" height="138" alt="富山県の今週のイベント・展示会情報" class="hover"></a>
        <span class="maru"><a href="/?page_id=22998" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/index/tiyama_maru.png" width="250" height="29" alt="富山まるごと実験＆体験件" class="hover"></a></span>
	<div id="event_ticker02"></div>
	<p><a href="http://ameblo.jp/himawarihometoyama/" target="_blank">>>一覧を見る</a></p>
	
	</div>
	
	<div class="event_index" > <a href="http://ameblo.jp/himawarihometokyo/"><img src="<?php bloginfo('template_url'); ?>/images/index/event03.jpg" width="267" height="138"  alt="富山県の今週のイベント・展示会情報" class="hover"></a>
	<div id="event_ticker03"></div>
	<p><a href="http://ameblo.jp/himawarihometokyo/" target="_blank">>>一覧を見る</a></p>
	
	</div>
	
</div>
	</div>
	
			</section>
		
		<!--2段目end-->		
				
	<!--3段目-->
	<section class="contents01">
	
	<div id="osusume">
		
		<div><a href="/?post_type=sekou&p=31698"><img src="<?php bloginfo('template_url'); ?>/images/index/osusume02.jpg" class="hover" width="267" height="180"></a></div>
		</div>
	
	
		<div class="left02_index" >
		
		<div class="news_bg02">
			<div id="news-ticker02"></div>
		</div>
		<ul class="media_index">
		<li><a href="/?page_id=25194"><img src="<?php bloginfo('template_url'); ?>/images/index/media01.jpg" class="hover" width="267" height="85" alt="サッカー"></a></li>
		</ul>
		</div>
		<ul class="voice_index">
		<li><a href="/?post_type=voice"><img src="<?php bloginfo('template_url'); ?>/images/index/voice01.jpg" class="hover" width="544" height="180" alt="お客様の声です 感謝"></a></li>
		</ul>
		
	</section>	
	
		<!--3段目end-->		
		
		<!--4段目-->
	<section class="contents01">
		<div class="left01_index" >
		<ul class="kenzaimoku_index">
		<li><a href="/?page_id=8472"><img src="<?php bloginfo('template_url'); ?>/images/index/kenzaimoku01.jpg" class="hover" width="267" height="85" alt="石川県の県産材"></a></li>
		</ul>
		<ul class="honebuto_index clearfix">
		<li><a href="/?page_id=25031"><img src="<?php bloginfo('template_url'); ?>/images/index/honebuto01.jpg" class="hover" width="128" height="180" alt="我が家の骨太 大黒柱を選ぶツアー"></a></li>
		<li><a href="/?page_id=13068"><img src="<?php bloginfo('template_url'); ?>/images/index/kinoie01.jpg" class="hover" width="128" height="180" alt="いしかわ木の家 自然と伝統の結晶"></a></li>
		</ul>
		<ul class="tochi_index">
		<li><a href="/?page_id=34"><img src="<?php bloginfo('template_url'); ?>/images/index/kaisya01.jpg" class="hover" width="267" height="180" alt="楽しく元気な家つくります。 ひまわりほーむ会社案内"></a></li>
		</ul>
		<ul class="kuredo_index">
		<li><a href="/?page_id=30277"><img src="<?php bloginfo('template_url'); ?>/images/index/character.jpg" class="hover" width="267" height="85" alt="入江久絵さん作　イメージキャラクター"></a></li>
		</ul>
		<ul class="shiawase_index">
		<li><a href="/?page_id=34723"><img src="<?php bloginfo('template_url'); ?>/images/index/movie.jpg" width="267" height="276" alt="幸せ家族ムービー" class="hover"></a></li>
		</ul>
		</div>
		
			<div class="left02_index" >
		<ul class="ceo_index">
		<li><a href="/?page_id=30287"><img src="<?php bloginfo('template_url'); ?>/images/index/ceo01.jpg" class="hover" width="267" height="180" alt="社長・加葉田について"></a></li>
		</ul>
	
		<ul class="report_index">
		<li><a href="/?page_id=121"><img src="<?php bloginfo('template_url'); ?>/images/index/kansha.jpg" width="267" height="85" alt="感謝祭　夏まつり・雪まつり" class="hover"></a></li>
		</ul>
		<ul class="manabi_index">
		<li><a href="/?page_id=25809"><img src="<?php bloginfo('template_url'); ?>/images/index/manabi01.jpg" class="hover" width="267" height="85" alt="研修レポート＆ありがとうレター 社員のマナビ"></a></li>
		</ul>
		<ul class="tiiki_index">
		<li><a href="/?page_id=65"><img src="<?php bloginfo('template_url'); ?>/images/index/tiiki01.jpg" class="hover" width="267" height="85" alt="地域貢献活動"></a></li>
		</ul>
		<ul class="himawari_index">
		<li><a href="/?page_id=59"><img src="<?php bloginfo('template_url'); ?>/images/index/himawari01.jpg" class="hover" width="267" height="85" alt="ひまわり村 写生大会"></a></li>
		</ul>
		<ul class="soccor_index">
		<li><a href="/?page_id=9065"><img src="<?php bloginfo('template_url'); ?>/images/index/soccor.jpg" class="hover" width="267" height="85" alt="ジュニアサッカーに特別協賛しています"></a></li>
		</ul>
		<ul class="himawari_index">
		<li><img src="<?php bloginfo('template_url'); ?>/images/index/designer.jpg" width="267" height="85" alt="15人のデザイナーたちが設計・デザインを3つ以上ご提案します。"></li>
		</ul>
		<ul class="stuffblog_index">
			<li><a href="http://ameblo.jp/himawarihomeishikawa/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/index/stuffblog_ishikawa.png" width="82" height="85" alt="石川スタッフブログ" class="hover"></a></li>
			<li><a href="http://ameblo.jp/himawarihometoyama/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/index/stuffblog_toyama.png" width="83" height="85" alt="富山スタッフブログ" class="hover"></a></li>
			<li><a href="http://ameblo.jp/himawarihometokyo/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/index/stuffblog_tokyo.png" width="82" height="85" alt="東京スタッフブログ" class="hover"></a></li>
		</ul>
		</div>
		<div class="left02_index" >
		<ul class="kizuna_index">
		<li><img src="<?php bloginfo('template_url'); ?>/images/index/designer.jpg" width="267" height="85" alt="15人のデザイナーたちが設計・デザインを3つ以上ご提案します。"></li>
		</ul>
		<ul class="tochi_index">
		<li><a href="/?page_id=34721"><img src="<?php bloginfo('template_url'); ?>/images/index/kaiun.jpg" width="267" height="85" alt="開運家相セミナー" class="hover"></a></li>
		</ul>
			<ul class="tochi_index">
		<li><a href="http://e-himawarifudousan.com/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/index/tochi01.jpg" class="hover" width="267" height="180" alt="土地・建売住宅不動産情報"></a></li>
		</ul>
		<ul class="kuredo_index">
		<li><a href="/?page_id=10"><img src="<?php bloginfo('template_url'); ?>/images/index/kuredo01.jpg" class="hover" width="267" height="85" alt="ひまわりクレド"></a></li>
		</ul>
		<ul class="soccer_index">
		<li><a href="/?page_id=34725"><img src="<?php bloginfo('template_url'); ?>/images/index/shikinkekaku.jpg" width="267" height="85" alt="家づくり資金計画" class="hover"></a></li>
		</ul>
		<ul class="soccer_index">
		<li><a href="/?page_id=69"><img src="<?php bloginfo('template_url'); ?>/images/index/torikumi.jpg" width="267" height="180" alt="取り組み" class="hover"></a></li>
		</ul>
		<ul class="soccer_index">
		<li><a href="http://www.e-himawari.co.jp/?page_id=23276"><img src="<?php bloginfo('template_url'); ?>/images/index/ohgonhi.png" width="267" height="85" alt="黄金比と白銀比の美" class="hover"></a></li>
		</ul>
		</div>
		
		<div class="left02_index" >
			<ul class="kinoie_index">
		<li><a href="/?page_id=23106"><img src="<?php bloginfo('template_url'); ?>/images/index/syunou01.jpg" class="hover" width="267" height="180" alt="家づくりのヒント！ 収納提案"></a></li>
		</ul>
		<ul class="kokoro_index">
		<li><a href="/?page_id=8"><img src="<?php bloginfo('template_url'); ?>/images/index/kokoro01.jpg" class="hover" width="267" height="85" alt="子供の心を育てる家づくり"></a></li>
		</ul>
		<ul class="taiyou_index">
		<li><a href="/?page_id=858"><img src="<?php bloginfo('template_url'); ?>/images/index/taiyou01.jpg" class="hover" width="267" height="85" alt="ひまわりの太陽光発電"></a></li>
		</ul>
		<ul class="tochi_index">
		<li><a href="/?page_id=19141"><img src="<?php bloginfo('template_url'); ?>/images/index/kawara01.jpg" class="hover" width="267" height="85" alt="瓦 修理ご相談"></a></li>
		</ul>
		<ul class="tochi_index">
		<li><a href="http://himawarikizuna.shop-pro.jp/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/index/maket01.jpg" class="hover" width="267" height="85" alt="ひまわりきずなマーケット"></a></li>
		</ul>
	<ul class="reform_index">
		<li><a href="/?page_id=582"><img src="<?php bloginfo('template_url'); ?>/images/index/reform01.jpg" class="hover" width="267" height="275" alt="これからのリフォーム"></a></li>
		</ul>
		
		</div>
	</section>	
	
	<!--4段目end-->
	<!--5段目-->

	<section class="contents01 cf">
	<ul class="shokunin_index left">
	<li><img src="<?php bloginfo('template_url'); ?>/images/index/shokunin_shokai.png" width="544" height="85" alt=""></li>
	</ul>
	
	<ul class="himawari_index left">
		<li><a href="/?page_id=25194"><img src="<?php bloginfo('template_url'); ?>/images/index/media.jpg" class="hover" width="267" height="85" alt="メディア・書籍等に紹介されました"/></a>
	</ul>
	
		<div class="news_bg">
			<div id="news-ticker"></div>
		</div>
	</section>
	
		<!--5段目end-->



	<div id="kodawari_index">
		<h2><img src="<?php bloginfo('template_url'); ?>/images/index/title_kodawari.png" width="634" height="28" alt="他とは違う！ひまわりほーむは４つの「全棟」にこだわります！！"></h2>
		<ul class="clearfix">
		<li><a href="/?page_id=772"><img src="<?php bloginfo('template_url'); ?>/images/index/bnr_tyoki.jpg" class="hover" width="268" height="56" alt="全棟長期優良住宅"></a></li>
		<li><a href="/?page_id=87"><img src="<?php bloginfo('template_url'); ?>/images/index/bnr_muku.jpg" class="hover" width="268" height="56" alt="全棟ムク材"></a></li>
		<li><a href="/?page_id=17500"><img src="<?php bloginfo('template_url'); ?>/images/index/bnr_taishin.jpg" class="hover" width="268" height="56" alt="全棟耐震・省エネ"></a></li>
		<li><a href="/?page_id=57"><img src="<?php bloginfo('template_url'); ?>/images/index/bnr_seino.jpg" class="hover" width="268" height="56" alt="全棟性能表示"></a></li>
		</ul>
	</div>
	
	<div id="form_index">
		<div id="form_page">
		<div class="btn_area"><a href="/?page_id=34048"><img src="<?php bloginfo('template_url'); ?>/images/common/form_page/form_bt.png" width="1072" height="378" alt="お問い合わせ" class="hover"></a></div>
		</div>
	</div>
	
	
	
		</div>


  
 


      

<!--

<?
  $args = array(
    'showposts' => $count, 
    'post_parent' => '1347',
    'post_type' => 'attachment',
    'post_mime_type' => 'image',
    'numberposts' => '1',
    'orderby' => 'rand',
  );
  
  $images = get_posts($args);
  foreach($images as $image) {
	$b_arr = wp_get_attachment_image_src_complete($image->ID, $size);

    echo '<a href="'. $b_arr[3] .'"><img src="'. $b_arr[0] .'" width="'. $b_arr[1] .'" height="'. $b_arr[2] .'" /></a>';
  }

?>-->    
            
    
        
   <!--ここまでがトップ-->

            <?php  } else { ?>
            
            
            
 <?php include (TEMPLATEPATH . '/header.php'); ?>
           <div id="contents">
 <article id="contents_left">
   
      <section class="bnr_box height">
      
            <div id="content">
             <div class="bg_f">
    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
									

<script language="JavaScript" type="text/JavaScript" src="http://www.e-himawari.co.jp/main.js"></script>
<link href="http://www.e-himawari.co.jp/img/style.css" rel="stylesheet" type="text/css" media="all">

					<div class="entry-content">
						<div id="content1">
							<?php the_content(); ?>
									
							
							<?php //全棟にこだわる家づくり 木の家とムク材
							if(is_page( array( 971,772,57,17500,25689,19456,87 ,34747,34750,34754 ) ) ): ?>
							
							
							<!-- 出口誘導 -->
							<div class="exit">
								<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_uneasy.png" alt="">
								<div class="bg_exp cf m_t30 m_b55">
									<a href="http://www.e-himawari.co.jp/?page_id=670"><img src="<?php bloginfo('template_url'); ?>/images/deguti/kanazawa_office.png" alt="金沢本社" class="kanazawa_office"></a>
									<a href="http://www.e-himawari.co.jp/?page_id=22998"><img src="<?php bloginfo('template_url'); ?>/images/deguti/toyama_office.png" alt="富山本社" class="toyama_office"></a>
								</div>
								<div class="oh m_b18">
									<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_sales.png" alt="しつこい営業はいたしません！" class="p_t2">
									<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_design.png" alt="同じ家は建てません！" class="flr">
								</div>
								<div class="oh">
									<a href="http://www.e-himawari.co.jp/?post_type=voice"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_gratitude.png" alt="お客様の声です"></a>
									<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_propose.png" alt="３つの提案お約束します" class="flr">
							</div><!-- /exit -->
							
							
							
							<?php //スタッフ紹介
							elseif(is_page( array( 1091,1093,1098,1119,1095,1102,1100,1123,1106,1420,1414,1110,1112,1114,1121,1117 ) ) ): ?>


							<!-- 出口誘導 -->
								<br>
							<div class="exit">
								<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_talk.png">
								<a href="http://www.e-himawari.co.jp/?post_type=voice"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_customer.png" class="m_b18 m_t97 hover"></a>
								<div class="oh">
									<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_top3.png" usemap="#Map">
                                    <map name="Map">
                                      <area shape="rect" coords="12,83,409,195" href="http://www.e-himawari.co.jp/?page_id=30287" alt="会社が明るい！なにより社長が元気で前向き！">
                                      <area shape="rect" coords="12,206,409,318" href="http://www.e-himawari.co.jp/?page_id=10" alt="スタッフ全員が守る家訓がある！">
                                      <area shape="rect" coords="12,329,409,441" href="http://www.e-himawari.co.jp/?page_id=25809" alt="お客様はもちろん、社員同士でも感謝の気持ちを忘れない！">
                                    </map>
									<a href="http://www.e-himawari.co.jp/?page_id=34048"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_reserve.png" class="flr p_t10 hover"></a>
							</div><!-- /exit -->




							<?php //リフォーム
							elseif(is_page( array( 582,30495,30498,30501,30505,30508,30511,30514,30517 ) ) ): ?>



					<!-- 出口誘導 -->
					<div class="exit">
					<img src="<?php bloginfo('template_url'); ?>/images/deguti/reform_img_01.png">
						<img src="<?php bloginfo('template_url'); ?>/images/deguti/reform_img_02.png">
						<div class="btn_mail"><a href="/?page_id=34048"><img src="<?php bloginfo('template_url'); ?>/images/deguti/btn_mail.png"></a></div>
						<img src="<?php bloginfo('template_url'); ?>/images/deguti/reform_img_04.png">
						<img src="<?php bloginfo('template_url'); ?>/images/deguti/reform_img_05.png">
					<br>
          
<!-- 誘導バナー -->
<a href="http://www.e-himawari.co.jp/?page_id=34048"><img src="/wp-content/themes/nopy/images/bnrShuffle/bnr_l_02.png" alt="" style="margin-top:25px"></a>
<!-- end -->
							
							
							<?php //おうち訪問 資金計画
							elseif(is_page( array( 1543,107,109,111,113,115,6551,6566 ,34725,34729,34731,34734,34736 ) ) ): ?>
							
							            

							
						<!-- 出口誘導 -->
						<div class="exit">
							<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_anxiety.png" alt="住宅会社に対してこんな心配ありませんか？">
							<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_relief.png" alt="ご安心下さい！ひまわりほーむは" class="p_t10">
							<div class="oh m_t25 m_b18">
								<a href="/?page_id=25194"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_media.png" alt="メディア・書籍等でもひまわりほーむが紹介されています"></a>
								<a href="/?page_id=121"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_festival.png" alt="感謝祭夏まつり・雪まつり" class="m_l20"></a>
									<a href="/?page_id=69"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_family.png" alt="お客様とは家族という関係でありたいと願っています" class="m_b10 flr"></a>
									<a href="/?post_type=voice"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_senior.png" alt="実はさらに先輩の生の声が聞けるんです" class="flr"></a>
							</div>
							<a href="/?page_id=34048"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_conf.png" alt="まじは匿名で相談してみませんか？"></a>
		
							
							
							<?php //まるごと実験
							elseif(is_page( array( 670,674,677,683,679,688,681,691,97,105,99,103,101,22998 ) ) ): ?>
<br><br><br>

						<!-- 出口誘導 -->
						<div class="exit">
							<div class="img_knowledge">
								<a href="/?page_id=34048"><img src="<?php bloginfo('template_url'); ?>/images/deguti/btn_reserve3.png" alt="来場予約はこちら" class="toyama"></a>
								<a href="/?page_id=22998"><img src="<?php bloginfo('template_url'); ?>/images/deguti/btn_toyama.png" alt="富山社屋はこちら"></a>
							</div>
							<div class="oh m_t25">
								<a href="/?page_id=971"><img src="<?php bloginfo('template_url'); ?>/images/deguti/btn_kodawari.png" alt="４つの全棟にこだわります"></a>
								<a href="/?page_id=1091"><img src="<?php bloginfo('template_url'); ?>/images/deguti/btn_staff.png" alt="いい家づくりはいいパートナーを見つけること！" class="m_l20"></a>
								<a href="/?sekou_cat=cat04"><img src="<?php bloginfo('template_url'); ?>/images/deguti/btn_idia.png" alt="お近くの展示場も参考にしたいアイデアがいっぱい" class="m_l20"></a>
								<img src="<?php bloginfo('template_url'); ?>/images/deguti/btn_teian.png" alt="重要な6項目で必ず３つ以上の提案をお約束します。" class="m_l20">
						</div><!-- /exit -->




							<?php //フォトギャラリー・部分事例集
							elseif(is_page( array( 16049,25508,25588,25538,25544,25551,25554,25557,25559,25561,25564,25568,
							17726,17977,17932,17951,17935,17994,18014,18031,18058 ) ) ): ?>

<br>
						<!-- 出口誘導 -->
						<div class="exit">
							<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_design2.png" alt="ひまわりほーむは３つ以上の設計・デザインを提案します。">
							<a href="/?sekou_cat=cat04"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_maker.png" alt="ひまわりほーむは３種類以上の住宅設備メーカーを提案します。"></a>
							<div class="oh cent_3 m_b55">
								<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_itto.png" alt="一棟丸ごと事例を見たい方はこちら" class="img_kotira">
								<a href="/?sekou_cat=cat01"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_sekojire.png" alt="施工事例集"></a>
								<a href="/?page_id=30495"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_rehome.png" alt="リフォーム施工事例集" class="flr"></a>
							</div>
							<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_exam.png" alt="">
							<a href="/?page_id=34048"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_answer.png" alt="設計・デザインご相談はこちら" class="m_t35"></a>
							<a href="/?page_id=34048"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_step.png" alt="ご相談お申込みはお電話一本" class="m_t40"></a>



							<?php //会社概要
							elseif(is_page( array( 34 ) ) ): ?>



						<!-- 出口誘導 -->
						<div class="exit">
							<div class="oh m_b35">
								<a href="/?cat=240"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_emp.png" alt="一緒に働きませんか？採用情報"></a>
								<a href="/?page_id=10"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_credo.png" alt="ひまわりほーみのクレド" class="m_l15"></a>
								<a href="/?page_id=25809"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_study.png" alt="研修レポート&ありがとうレター　社員のマナビ" class="m_l15"></a>
							</div>
						<a href="/?page_id=34048"><img src="<?php bloginfo('template_url'); ?>/images/foot/btn_inquiry.png" alt="お問い合わせ" class="hover"></a>


							<?php //ひまわりのきずな&感謝祭 研修レポートありがとうレター 家相 家族ムービー
							elseif(is_page( array( 121,123,125,127,129,131,133 , 67,69,77,79,73,75,71,81, 25194, 25809,34701,34632 ,34721,34721 ) ) ): ?>
							
							<br>
							<!-- 出口誘導 -->
							<div class="exit">
								<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_anxiety.png" alt="住宅会社に対してこんな心配ありませんか？">
								<img src="<?php bloginfo('template_url'); ?>/images/deguti/img_relief2.png" alt="ご安心下さい！ひまわりほーむは" class="p_t10">
								<div class="oh m_t25 m_b18">
									<a href="/?page_id=25194"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_media2.png" alt="メディア・書籍等でもひまわりほーむが紹介されています"></a>
									<a href="/?page_id=121"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_festival2.png" alt="感謝祭夏まつり・雪まつり" class="m_l20"></a>
									<a href="/?post_type=voice"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_senior2.png" alt="実はさらに先輩の生の声が聞けるんです" class="flr"></a>
								</div>
								<a href="/?page_id=34048"><img src="<?php bloginfo('template_url'); ?>/images/deguti/img_conf.png" alt="まじは匿名で相談してみませんか？"></a>
							
							
							
						<?php else : ?>
						

							<div style="margin:10px 0;">
							<!-- footer ここから -->
							<div class="foot">
								<div class="btn_inquiry">
									<a href="/?page_id=34048"><img src="<?php bloginfo('template_url'); ?>/images/foot/btn_inquiry.png" alt="お問い合わせ" class="hover"></a>
								</div>
							</div>
							<!-- footer ここまで -->




							<?php endif; ?>
							
                         
                            
							<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
							<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
						</div>
					</div><?php if(function_exists('echo_ald_wherego')) echo_ald_wherego(); ?> <!-- .これが関連記事 --><!-- .entry-content -->
				</div><!-- #post-## -->



				
            <?php endwhile; ?>
		  </div></div></div>
            <!-- #content -->
         
		</section>
  </article>
<?php include TEMPLATEPATH . '/sidebar-page.php'; ?>

</div>
<? } ?>


<?php get_footer(); ?>