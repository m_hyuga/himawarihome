<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<link href="http://www.e-himawari.co.jp/wordpress/img/style.css" rel="stylesheet" type="text/css" media="all">
<div id="contents">
 <article id="contents_left">
   
      <section class="bnr_box height">
      
            <div id="content">
             <div class="bg_f">




<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="voice_con">
                
					<h3 class="voice"><?php the_title(); ?></h3>

					
					<div class="entry-content">
						<?php the_content(); ?>
                         <?php echo c2c_get_custom('textarea-voice01'); ?>
						
					</div><!-- .entry-content -->


					<div class="entry-utility">
						
						<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
					</div><?php if(function_exists('echo_ald_wherego')) echo_ald_wherego(); ?> <!-- .これが関連記事 --><!-- .entry-utility -->
                    
                    
                    
                </div>    
				</div><!-- #post-## -->

				

			
<?php endwhile; // end of the loop. ?>

		 </div></div>
            <!-- #content -->
         
		</section>
  </article>
<?php include TEMPLATEPATH . '/sidebar-page.php'; ?>
</div>


<?php get_footer(); ?>
