<?php
/**
 * Template Name: kodawari
 */
?>

<?php get_header(); ?>
<link href="http://www.e-himawari.co.jp/wordpress/img/style.css" rel="stylesheet" type="text/css" media="all">
<link href="http://www.e-himawari.co.jp/wordpress/img/style2.css" rel="stylesheet" type="text/css" media="all">

<div id="main_top">
</div>
<div id="main">
  <div id="container">
    <div id="content" role="main">
      <div id="content1">
        <div id="topix_title2" align="center" style="background-color: #fff2ae;">
          <table width="740" height="900" border="0" cellpadding="0" cellspacing="0" style="line-height: 1.5;">
            <tbody>
               <tr>
                 <td colspan="4" height="150" align="center"><img src="http://www.e-himawari.co.jp/wordpress/img/kodawari/title.gif" alt="" width="670" height="120" /></td>
               </tr>
               <tr>
                 <td valign="top" style="line-height:55px;">
<?
  $args = array(
    'showposts' => $count, 
    'post_parent' => '4695',
    'post_type' => 'attachment',
    'post_mime_type' => 'image',
    'numberposts' => '50',
#    'orderby' => 'rand',
  );
  
  $images = get_posts($args);
  foreach($images as $image) {
	$b_arr = wp_get_attachment_image_src_complete($image->ID, $size);
	if(!empty($b_arr[4])) {
		$b_arr[4] = $b_arr[4]."<br>";
	}

    $out_arr[] = '<a href="'. $b_arr[3] .'"><img src="'. $b_arr[0] .'" width="180" height="150" /></a>';
  }
  
	//表示バナー数
	$b_count = count($out_arr);
	//各列の表示数決定
	$b_tmp = $b_count / 4;
	$base_count = (int)$b_tmp;
	$line1_count = $base_count;
	$line2_count = $base_count;
	$line3_count = $base_count;
	$line4_count = $base_count;
	$b_tmp2 = $b_count % 4;
	if($b_tmp2 == 1) { ++$line1_count; }
	elseif($b_tmp2 == 2) { ++$line1_count; ++$line2_count; }
	elseif($b_tmp2 == 3) { ++$line1_count; ++$line2_count; ++$line3_count; }
	$line_count = 1;
	$this_line = 1;
	$start_div = "";
	$end_div = "";
	foreach($out_arr as $val) {
		if($line_count == 1) {
			$start_div = "\n";
		} else {
			$start_div = '';
		}
		if( ($this_line == 1 && $line_count == $line1_count) || ($this_line == 2 && $line_count == $line2_count) || ($this_line == 3 && $line_count == $line3_count) || ($this_line == 4 && $line_count == $line4_count) ) {
			$end_div = "\n\n";
			++$this_line;
			$line_count = 1;
		} else {
			$end_div = '';
			++$line_count;
		}
		
		echo "\n".$start_div . $val . $end_div."\n";
		
	}
?>



                 </td>
               </tr>
               <tr>
                 <td>　</td>
               </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <?php get_sidebar(); ?>
  </div>
<?php get_footer(); ?>
