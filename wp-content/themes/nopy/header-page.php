<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">

 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />

<meta name="description"content="無垢材・自然素材・性能表示・耐震・省エネにこだわり、新築する全ての住宅を長期優良住宅とし、お客様が安心・納得・満足される健康住宅を提供しています。世界的な建築家である西沢立衛氏との共同プロジェクトも進行中です。"/>
<meta name="Keywords"content="長期優良住宅,無垢材,自然素材,性能表示,石川県,富山県,東京都,西沢立衛,ひまわりほーむ"/>
<meta name="viewport" content="target-densitydpi=device-dpi, width=1150px, maximum-scale=1.0, user-scalable=yes">

<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 * We filter the output of wp_title() a bit -- see
	 * twentyten_filter_wp_title() in functions.php.
	 */
	wp_title( '|', true, 'right' );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
 <?php wp_head(); ?>

<script type="text/javascript">
 
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-21257881-1']);
  _gaq.push(['_trackPageview']);
 
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
 
</script>
<script type="text/javascript" src="http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/iepngfix.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/smp.js"></script>

</head>

<body <?php body_class(); ?>>
<div data-shutto-display="match" style="display: none;" id="sp"><a href="javascript:__shutto.view('mobile')">スマホ向け表示</a></div>
<a name="top"></a>
<div id="header">
		
			<div id="branding" role="banner">
				<?php $heading_tag = ( is_home() || is_front_page() ) ? 'h1' : 'div'; ?>
				<<?php echo $heading_tag; ?> id="site-title">
					<span>
						<a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">無垢材、自然素材、性能表示、長期優良住宅・西沢立衛プロジェクト｜ひまわりほーむ</a>					</span>
				</<?php echo $heading_tag; ?>>
			  <?php
					// Check if this is a post or page, if it has a thumbnail, and if it's a big one
					if ( is_singular() &&
							has_post_thumbnail( $post->ID ) &&
							( /* $src, $width, $height */ $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnail' ) ) &&
							$image[1] >= HEADER_IMAGE_WIDTH ) :
						// Houston, we have a new header image!
						echo get_the_post_thumbnail( $post->ID, 'post-thumbnail' );
					else : ?><?php endif; ?>
                    <div id="topmenu">
                    <ul>
                    	<li><a href="./?page_id=38" class="um1">採用情報</a></li>
                      <li><a href="./?page_id=34" class="um2">会社概要</a></li>
                      <li><a href="./?page_id=978" class="um3">資料請求</a></li>
<!--
                      <li><a href="mailto:info@e-himawari.co.jp" class="um4">お問い合わせ</a></li>
-->
                      <li><a href="./?page_id=13666" class="um4">お問い合わせ</a></li>
                      <li><a href="./?page_id=25194" class="um5">メディア実績</a></li>
                    </ul>
                    </div>
		  </div>
	   
       
          <!-- #branding -->
        <div id="mainmenu_bg">
        <div id="mainmenu">
		<ul>
           		<li><a href="./?page_id=971"><img src="http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_01.png" width="122" height="42" alt="ひまわりほーむ人気の秘密" class="iepngfix" onMouseOver="this.src='http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_01_over.png',IEPNGFIX.fix(this)" onMouseOut="this.src='http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_01.png',IEPNGFIX.fix(this)" / ></a></li>
               <li><a href="./?page_id=1091"><img src="http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_02.png" width="139" height="42" alt="ひまわりスタッフに会いに行こう！" class="iepngfix" onMouseOver="this.src='http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_02_over.png',IEPNGFIX.fix(this)" onMouseOut="this.src='http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_02.png',IEPNGFIX.fix(this)" / ></a></li>
               <li><a href="./?page_id=872"><img src="http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_03.png" width="122" height="42" alt="ひまわりほーむ展示会情報" class="iepngfix" onMouseOver="this.src='http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_03_over.png',IEPNGFIX.fix(this)" onMouseOut="this.src='http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_03.png',IEPNGFIX.fix(this)" / ></a></li>
               <li><a href="./?page_id=16049"><img src="http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_04.png" width="128" height="42" alt="ひまわりほーむフォトギャラリー" class="iepngfix" onMouseOver="this.src='http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_04_over.png',IEPNGFIX.fix(this)" onMouseOut="this.src='http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_04.png',IEPNGFIX.fix(this)" / ></a></li>
               <li><a href="./?page_id=30"><img src="http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_05.png" width="148" height="42" alt="ひまわりほーむってどんな会社？" class="iepngfix" onMouseOver="this.src='http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_05_over.png',IEPNGFIX.fix(this)" onMouseOut="this.src='http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_05.png',IEPNGFIX.fix(this)" / ></a></li>
               <li><a href=".p/?page_id=582"><img src="http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_06.png" width="134" height="42" alt="リフォーム大作戦" class="iepngfix" onMouseOver="this.src='http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_06_over.png',IEPNGFIX.fix(this)" onMouseOut="this.src='http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_06.png',IEPNGFIX.fix(this)" / ></a></li>
                <li><a href="./?page_id=670"><img src="http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_07.png" width="155" height="42" alt="まるごと実験＆体験館を見に行こう！" class="iepngfix" onMouseOver="this.src='http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_07_over.png',IEPNGFIX.fix(this)" onMouseOut="this.src='http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_07.png',IEPNGFIX.fix(this)" / ></a></li>
                 <li><a href="./?page_id=1076"><img src="http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_08.png" width="82" height="42" alt="トピックス" class="iepngfix" onMouseOver="this.src='http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_08_over.png',IEPNGFIX.fix(this)" onMouseOut="this.src='http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_08.png',IEPNGFIX.fix(this)" / ></a></li>
                  <li><a href="./?page_id=978"><img src="http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_09.png" width="70" height="42" alt="資料請求" class="iepngfix" onMouseOver="this.src='http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_09_over.png',IEPNGFIX.fix(this)" onMouseOut="this.src='http://www.e-himawari.co.jp/wordpress/wp-content/themes/nopy/images/nav/img_09.png',IEPNGFIX.fix(this)" / ></a></li>
		</ul>
        </div>
        <!--mainmenu -->
		</div>
        <!--#mainmenu_bg -->	
	  
</div><!-- #header -->

	
