<?php
/*
Plugin Name: mdm 指定ID群記事・リスト表示
Description: mdm 指定ID群記事・リスト表示
Version: 1
Plugin URI: http://www.mdm-net.com/
Author: Makoto Hayashi
Author URI: http://www.mdm-net.com/
*/

function show_posts_top($post_ids){

	global $post, $posts;
	add_filter('posts_orderby' , function ($orderby) use ($post_ids) {
		return 'FIELD(ID,'.join(",",$post_ids).')'; 
	});
	$args = array(
		'post__in' => $post_ids,
		'post_type' => array('page','post','staff')
	);
	$post_ids_query = new WP_Query ( $args );
	if ( $post_ids_query->have_posts()):
		$ct = 1;
		while ( $post_ids_query->have_posts()):
			$post_ids_query->the_post();?>
         <dt><img src="http://www.e-himawari.co.jp/wp-content/themes/nopy/images/common/no0<?php echo $ct ?>.png" width="43" height="14"></dt>
			<dd><a href="<?php echo the_permalink(); ?>"><?php echo mb_strimWidth(get_the_title(), 0, 24, "..."); ?></a></dd>
<?php
			++$ct;
		endwhile;
		wp_reset_postdata();
	else:
		//（該当記事がなかった場合）
	endif;
}