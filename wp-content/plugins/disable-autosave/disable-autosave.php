<?php
/*
Plugin Name: Disable Autosave
Plugin URI: http://exper.3drecursions.com/2008/07/25/disable-revisions-and-autosave-plugin/
Description: Disable the Autosave function. By <a href="http://exper.3drecursions.com/">Exper</a>. Original idea and code by <a href="http://www.untwistedvortex.com/2008/06/27/adjust-wordpress-autosave-or-disable-it-completely/" target="_blank">Untwisted Vortex</a>.
*/

function disable_autosave() {
wp_deregister_script('autosave');
}
add_action( 'wp_print_scripts', 'disable_autosave' );
?>
