<?php
/*
Plugin Name: ひまわりほーむ　お勧め編集v2（画像アップロード版）
Description: ひまわりほーむ　お勧め編集v2（画像アップロード版）
Version: 2
Plugin URI: http://www.mdm-net.com/
Author: Makoto Hayashi
Author URI: http:/http://www.mdm-net.com/
*/
function hoge_script_admin($hook_suffix){
	// @param string ユニークなハンドル名
	// @param string 読み込みたいJSファイルのパス
	// @param array 依存するJSライブラリのハンドル
	// @param string バージョン
	// @param boolean falseならヘッダー、trueならフッターに読み込み
	wp_enqueue_script('hoge1', '/wp-content/themes/nopy/js/imgLiquid-min.js', array('jquery'),'0.1.0', false );
	wp_enqueue_script('hoge2', '/wp-content/themes/nopy/js/jquery.upload-1.0.2.min.js', array('jquery'),'0.1.0', false );
}
add_action('admin_enqueue_scripts', 'hoge_script_admin');

function edit_recos() {
	global $wpdb;
	if ($_POST['posted'] == 'Y') :
		$recos = $_POST['recos'];
		// echo var_dump($recos);
		$res = $wpdb->query("DELETE FROM recommends");
		foreach($recos as $reco){
			$v1 = intval($reco['order_no']);  
			$v2 = $reco['title'];
			$v3 = intval($reco['post_id']);
			if (($v1 != 0) && ($v2 !="") && ($v3!= 0)){
				$res = $wpdb->query("INSERT INTO recommends (order_no, title, post_id) VALUES ({$v1},'{$v2}',{$v3})");
			}
		}
		echo "<div class='updated'><p><strong>設定を保存しました</strong></p></div>";
	endif;

	$recos = $wpdb->get_results("SELECT * FROM recommends ORDER BY order_no DESC LIMIT 20");
	// echo var_dump($recos);
	if (count($recos)<12){
		for($i=count($recos); $i<12; $i++){
			$obj= new stdClass;
			$obj->title = '';
			$obj->post_id = '';
			$obj->order_no = '';
			$recos[] = $obj;
		} 
	}
?>
<style>
	img.thumb {
		width:70px;
		height:70px;
		overflow:hodden;
	}
	.file {
		display: inline-block;
		overflow: hidden;
		position: relative;
		padding: .5em;
		border: 1px solid #999;
		background-color: #eee;
	}
	.file input[type="file"] {
		opacity: 0;
		filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0);
		position: absolute;
		right: 0;
		top: 0;
		margin: 0;
		font-size: 100px;
		cursor: pointer;
	}
</style>
<script type="text/javascript">
	jQuery(document).ready(function($){
	
		$('img.thumb').imgLiquid();

		$('input[type=file]').change(function() {
			// alert("fetched");
			$(this).upload(
				'/saveRecoThumbnail.php',
				{ postID : $(this).closest('tr').attr('data-postid'), imgID : $(this).closest('tr').attr('data-id') },
				function(data) {
					if (data.status) {
						$('img#' + data.imgID).attr('src', data.imgPath);
						$('img#' + data.imgID).imgLiquid();
					}
					alert(data.msg);
				},
				'json'
			);
		});
		$('div.delImage').hover(
			function(){
				$(this).css('cursor','pointer');
			},
			function(){
				$(this).css('cursor','arrow');
			}
		);
		$('div.delImage').click(function(){
			if (confirm('削除します。よろしいですか？')){
				$.ajax({
					url:'/deleteRecoThumbnail.php',
					type:'POST',
					dataType:'json',
					data: { postID : $(this).closest('tr').attr('data-postid'), imgID : $(this).closest('tr').attr('data-id') },
					success: function(data){
						if (data.status) {
							$('#' + data.imgID).attr('src', data.imgPath);
							$('#' + data.imgID).imgLiquid();
						}
						alert(data.msg);
					}
				});
			}
		});
	});
</script>
<div class="wrap">
	<h2>ひまわりほーむ　お勧めコンテンツの編集</h2>
	<form method="post" action="<?php echo str_replace('%7E','~',$_SERVER['REQUEST_URI']); ?>">
		<table width="600">
			<tr valign="top">
				<th width=120">表示順（大きい数値ほど上に）</th>
				<th width=150">タイトル</th>
				<th width=80">対象の記事id番号</th>
				<th width=80">サムネール</th>
				<th></th>
			</tr>
			<?php $i=0; ?>
			<?php foreach($recos as $reco) { ?>
			<tr data-postid='<?php echo $reco->post_id; ?>' data-id='img_<?php echo $i ?>'>
				<td><input name="recos[<?php echo $i ?>][order_no]" type="text" value="<?php echo $reco->order_no; ?>" /></td>
				<td><input name="recos[<?php echo $i ?>][title]" type="text" value="<?php echo $reco->title; ?>" /></td>
				<td><input name="recos[<?php echo $i?>][post_id]" type="text" value="<?php echo $reco->post_id; ?>" /></td>
				<td>
					<?php if($reco->post_id != ''){ ?>
					<img id="img_<?php echo $i ?>" class="thumb" src="<?php echo get_thumbnail_path($reco->post_id); ?>" />
					<?php } ?>
				</td>
				<td>
					<?php if($reco->post_id != ''){ ?>
					<div class="file"><input name="upImage" type="file"></input>選択</div> 
					<div class="file delImage">削除</div> 
					<?php } ?>
            </td>
			</tr>
			<?php $i++; } ?>
		</table>
		<p class="submit">
			<input type="hidden" name="posted" value="Y"/>
			<input type="submit" name="Submit" class="button-primary" value="変更を保存" /></p>
	</form>
</div>
<?php
}
function edit_recos_addMenu(){
	add_submenu_page('options-general.php','ひまわりほーむ　お勧め編集','ひまわりほーむ　お勧め編集', 8, '__FILE__', 'edit_recos');
}
add_action('admin_menu', 'edit_recos_addMenu');

function get_thumbnail_path($postid){

	// root/images/postThumbs/thumb_xxx.yyy画像が存在すればそのパスを返す
	$thumb_imgPath = get_stylesheet_directory().'/images/postThumbs/thumb_'.$postid.'.*';
	$files = glob($thumb_imgPath);
	if (count($files)>0){
		return get_stylesheet_directory_uri().'/images/postThumbs/'.subStr($files[0], strRPos($files[0],"/") + 1);
	} else {
		return get_stylesheet_directory_uri().'/images/postThumbs/wordpress.png';
	}

}
