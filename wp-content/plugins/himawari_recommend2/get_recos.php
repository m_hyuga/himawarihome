<?php
/*
Plugin Name: ひまわりほーむ　お勧めID群取得（タイトル対応）
Description: ひまわりほーむ　お勧めID群取得（タイトル対応）
Version: 2
Plugin URI: http://www.mdm-net.com/
Author: Makoto Hayashi
Author URI: http://www.mdm-net.com/
*/

function get_recos(){
	global $wpdb;
	$vals = $wpdb->get_results("SELECT * FROM recommends ORDER BY order_no DESC");

	foreach($vals as $val){
		$ids[] = intval($val->post_id);
		if (isset($val->title) && ($val->title != ''))
			$titles[$val->post_id] = $val->title;
			// $titles[] = array(($val->post_id) => ($val->title));
	}
	$result = array('ids'=>$ids, 'titles'=>$titles);

	return $result;
}