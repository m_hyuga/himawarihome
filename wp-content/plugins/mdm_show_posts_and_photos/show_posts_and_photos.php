<?php
/*
Plugin Name: mdm 指定ID群記事・サムネール表示
Description: mdm 指定ID群記事・サムネール表示
Version: 1
Plugin URI: http://www.mdm-net.com/
Author: Makoto Hayashi
Author URI: http://www.mdm-net.com/
*/

function show_posts_and_photos($param){

	if (isset($param['ids'])){
		$post_ids		= $param['ids'];	// 改良型recosの場合
		$post_titles	= $param['titles'];
	} else {
		$post_ids		= $param;			// 従来型recos/history_recosの場合
		$post_titles	= array();
	}
	
	global $post, $posts;
	add_filter('posts_orderby' , function ($orderby) use ($post_ids) {
		return 'FIELD(ID,'.join(",",$post_ids).')'; 
	});
	$args = array(
		'post__in' => $post_ids,
		'post_type' => array('page','post','staff','sekou','voice','reform')
	);
	$post_ids_query = new WP_Query($args);
	if ( $post_ids_query->have_posts()):
		while ( $post_ids_query->have_posts()):
			$post_ids_query->the_post();?>
<li><a href="<?php echo the_permalink(); ?>"><img src="<?php echo catch_that_image(); ?>" width="75" height="75"/>
<p><?php echo get_that_title(get_the_title(), $post_titles, $post->ID); ?></p>
</a></li>
<?php
		endwhile;
		wp_reset_postdata();
	endif;
}
function catch_that_image() {
	global $post, $posts;
	$first_img = '';
	ob_start();
	ob_end_clean();

	// root/images/postThumbs/thumb_post_id.xxx画像が存在すればそのパスを返す
	$thumb_imgPath = get_stylesheet_directory().'/images/postThumbs/thumb_'.$post->ID.'.*';
	$files = glob($thumb_imgPath);
	if (count($files)>0){
		return get_stylesheet_directory_uri().'/images/postThumbs/'.subStr($files[0], strRPos($files[0],"/") + 1);
	}

	// カスタム投稿タイプの場合は対応する画像フィールドを返す
	if (get_post_type($post)=="staff"){
		$thumb = get_that_thumb('staff-photo');
	} else if (get_post_type($post)=="sekou"){
		$thumb = get_that_thumb('sekou-photo01');
	} else if (get_post_type($post)=="voice"){
		$thumb = get_that_thumb('voice-photo01');
		if (empty($thumb)) {
			$thumb = get_that_thumb('letter-photo');
		}
	} else if (get_post_type($post)=="reform"){
		$thumb = get_that_thumb('After-photo01');
    } else if (get_post_type($post)=="page"){
		$thumb = get_that_thumb('page-photo');
	}
	if (!empty($thumb)) return $thumb;

	// 以外は投稿に含まれる先頭画像のパスを返す
	$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
	$first_img = $matches [1][0];
	if (empty($first_img)) { //Defines a default image
		$first_img = "http://www.e-himawari.co.jp/wp-content/themes/nopy/images/default.jpg";
	}
	return $first_img;
}


function get_that_thumb($fieldname){
	// CustomField GUI Utililtyのフィールド情報を取得
	$imagefield = get_imagefield($fieldname);
	// 上記に従い、thumbnail画像パスを取得
	$thumb = wp_get_attachment_image_src($imagefield['id'], 'thumbnail');
	return $thumb[0];

}

function get_that_title($org, $titles, $id){

	$result = '';
	if (!isset($titles[$id]) || ($titles[$id]=='')) {
		$result = $org;
	} else {
		$result = $titles[$id];
	}
	return mb_strimWidth($result, 0, 24, "...");
}