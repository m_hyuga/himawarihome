<?php
/*
Plugin Name: ひまわりほーむ　遷移記録
Description: ひまわりほーむ　遷移記録
Version: 1
Plugin URI: http://www.mdm-net.com/
Author: Makoto Hayashi
Author URI: http://www.mdm-net.com/
*/

function set_history_reco(){
	// print_r($_SESSION);
	if (empty($_SESSION['from'])) {
		$_SESSION['from'] = get_the_ID();
	} else {
		global $wpdb;
		$id_from = intval($_SESSION["from"]);
		$id_to	= intval(get_the_ID());
		if($id_from==$id_to) return;
		$c_date = date("Y-m-d", time()); 
		// echo $c_date;
		$rec = $wpdb->get_row("SELECT * FROM histories WHERE (c_date='{$c_date}' AND id_from={$id_from} AND id_to={$id_to}) ");
		if (count($rec)==0){
			// echo 'From:'.$id_from.' To:'.$id_to;
			$ans = $wpdb->query("INSERT INTO histories (c_date, id_from, id_to, c_count) VALUES ( '{$c_date}', {$id_from}, {$id_to}, 1 )");
		} else {
			$c_count = $rec->c_count +1;
			$ans = $wpdb->query("UPDATE histories SET c_count={$c_count}, c_date='{$c_date}', id_from={$id_from}, id_to={$id_to} WHERE (c_date='{$c_date}' AND id_from={$id_from} AND id_to={$id_to}) ");
		}
		$_SESSION['from'] = get_the_ID();
	}
}
