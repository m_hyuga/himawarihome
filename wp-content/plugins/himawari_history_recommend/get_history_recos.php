<?php
/*
Plugin Name: ひまわりほーむ　遷移お勧めID群取得
Description: ひまわりほーむ　遷移お勧めID群取得
Version: 1
Plugin URI: http://www.mdm-net.com/
Author: Makoto Hayashi
Author URI: http://www.mdm-net.com/
*/

function get_history_recos(){
	global $wpdb;
	$id_curr = get_the_ID();
	$rows = $wpdb->get_results("SELECT id_to,sum(c_count) as qty FROM histories WHERE id_from={$id_curr} GROUP BY id_to ORDER BY qty DESC LIMIT 6");
	foreach($rows as $row){
		$ids[] = $row->id_to;
	}
	// print_r($ids);
	return $ids;
}